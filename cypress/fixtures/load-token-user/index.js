export const getLoadTokenUserFixture = () => ({
  name: 'loadTokenUser',
  files: [
    'load-token-user/valid',
    'load-token-user/invalid',
    'load-token-user/admin',
    'load-token-user/validNoSpotify',
  ],
  resolve: (data, _, context) => {
    if (context.loggedInWithRoles) {
      const user = Object.assign({}, data.valid.data.user);
      user.roles = context.loggedInWithRoles;
      return { data: { user } };
    }
    if (context.loggedIn) {
      return data.valid;
    }
    if (context.loggedInAsAdmin) {
      return data.admin;
    }
    if (context.loggedInNoSpotify) {
      return data.validNoSpotify;
    }
    return data.invalid;
  },
});
