const PIECE_OF_MIND = '59fd2859afb450411dfc5294';
const SYMPHONY_OF_ENCHANTED_LANDS = '5a57fa6d500d500b58f3419c';

export const getAlbumDetailsFixture = () => ({
  name: 'loadAlbumDetails',
  files: [
    'load-album-details/pieceOfMind',
    'load-album-details/symphonyOfEchantedLands',
    'errors/genericError',
  ],
  resolve: (data, { albumId }, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    if (albumId === PIECE_OF_MIND) {
      return data.pieceOfMind;
    }
    if (albumId === SYMPHONY_OF_ENCHANTED_LANDS) {
      return data.symphonyOfEchantedLands;
    }
    return {};
  },
});
