export const getResetPasswordFixture = () => ({
  name: 'resetPassword',
  files: [
    'reset-password/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
