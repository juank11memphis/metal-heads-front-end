export const getNewsBySourceFixture = () => ({
  name: 'loadNewsBySource',
  files: [
    'news/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
