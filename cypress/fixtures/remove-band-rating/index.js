export const getRemoveBandRatingFixture = () => ({
  name: 'removeBandRating',
  files: ['remove-band-rating/valid', 'errors/genericError'],
  resolve: (data, _, { withUserDataErrors }) => {
    if (withUserDataErrors) {
      return data.genericError
    }
    return data.valid
  },
})
