export const getSignInFixture = () => ({
  name: 'signin',
  files: [
    'signin/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
