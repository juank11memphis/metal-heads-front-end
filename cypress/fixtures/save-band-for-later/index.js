export const getSaveBandForLaterFixture = () => ({
  name: 'saveBandForLater',
  files: [
    'save-band-for-later/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withUserDataErrors }) => {
    if (withUserDataErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
