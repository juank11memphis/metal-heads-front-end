export const getLoadRatedBandsFixture = () => ({
  name: 'loadRatedBands',
  files: [
    'load-rated-bands/valid',
    'load-rated-bands/noData',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors, noData }) => {
    if (withErrors) {
      return data.genericError
    }
    if (noData) {
      return data.noData
    }
    return data.valid
  },
})
