export const getLoadUserFixture = () => ({
  name: 'loadUser',
  files: ['load-user/valid'],
  resolve: data => data.valid,
})
