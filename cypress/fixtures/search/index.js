export const getSearchBandsFixture = () => ({
  name: 'search',
  files: ['search/withResults', 'search/withNoResults', 'errors/genericError'],
  resolve: (data, { searchText }, { withErrors }) => {
    if (withErrors) {
      return data.genericError
    }
    if (searchText === 'none') {
      return data.withNoResults
    }
    return data.withResults
  },
})
