export const getRateBandDataFixture = () => ({
  name: 'rateBand',
  files: [
    'rate-band/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withUserDataErrors }) => {
    if (withUserDataErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
