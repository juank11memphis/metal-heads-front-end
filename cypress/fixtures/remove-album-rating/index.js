export const getRemoveAlbumRatingFixture = () => ({
  name: 'removeAlbumRating',
  files: ['remove-album-rating/valid', 'errors/genericError'],
  resolve: (data, _, { withUserDataErrors }) => {
    if (withUserDataErrors) {
      return data.genericError
    }
    return data.valid
  },
})
