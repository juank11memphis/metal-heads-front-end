const MAIDEN_ID = '592f7fa02615c92686b9c5aa'
const JUDAS_ID = '592f7fcdb7bb93268a46b597'
const SABBATH_ID = '592f7c0b920666266c1cef34'
const ENFORCER_ID = '592f7e5f7f218d267bef2ba6'

export const getLoadUserBandDataFixture = () => ({
  name: 'loadUserBandData',
  files: [
    'load-user-band-data/maiden',
    'load-user-band-data/judas',
    'load-user-band-data/blackSabbath',
    'load-user-band-data/enforcer',
    'errors/genericError',
  ],
  resolve: (data, { bandId }, { withErrors }) => {
    if (withErrors) {
      return data.genericError
    }
    if (bandId === MAIDEN_ID) {
      return data.maiden
    }
    if (bandId === JUDAS_ID) {
      return data.judas
    }
    if (bandId === SABBATH_ID) {
      return data.blackSabbath
    }
    if (bandId === ENFORCER_ID) {
      return data.enforcer
    }
    return {}
  },
})

export const getBandSimilarArtists = () => ({
  name: 'loadSimilarBands',
  files: ['load-band-details/maidenSimilarArtists', 'errors/genericError'],
  resolve: (data, { bandId }, { withErrors }) => {
    if (withErrors) {
      return data.genericError
    }
    if (bandId === MAIDEN_ID) {
      return data.maidenSimilarArtists
    }
    return {}
  },
})
