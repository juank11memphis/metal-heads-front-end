export const getLoadSpotifyDevicesFixture = () => ({
  name: 'loadSpotifyDevices',
  files: [
    'load-spotify-devices/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
