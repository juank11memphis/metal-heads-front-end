export const getRemoveFromSavedBandsFixture = () => ({
  name: 'removeFromSavedBands',
  files: [
    'remove-from-saved-bands/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withUserDataErrors }) => {
    if (withUserDataErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
