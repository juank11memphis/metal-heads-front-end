export const getLoadSavedBandsFixture = () => ({
  name: 'loadSavedBands',
  files: [
    'load-saved-bands/valid',
    'load-rated-bands/noData',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors, noData }) => {
    if (withErrors) {
      return data.genericError
    }
    if (noData) {
      return data.noData
    }
    return data.valid
  },
})
