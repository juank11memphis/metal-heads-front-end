const STEVE_HARRIS_ID = '5930c25979e4ad1e2139438e';

export const getArtistDetailsFixture = () => ({
  name: 'loadArtistDetails',
  files: [
    'artist-details/harris',
    'errors/genericError',
  ],
  resolve: (data, { artistId }, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    if (artistId === STEVE_HARRIS_ID) {
      return data.harris;
    }
    return {};
  },
});
