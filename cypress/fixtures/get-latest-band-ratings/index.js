export const getLatestBandRatingsFixture = () => ({
  name: 'getLatestBandRatings',
  files: ['get-latest-band-ratings/valid', 'errors/genericError'],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError
    }
    return data.valid
  },
})
