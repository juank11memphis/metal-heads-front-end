export const getMostRatedBandsFixture = () => ({
  name: 'loadMostRatedBands',
  files: ['load-most-rated-bands/valid', 'errors/genericError'],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError
    }
    return data.valid
  },
})
