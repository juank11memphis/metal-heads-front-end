export const getSignUpFixture = () => ({
  name: 'signup',
  files: [
    'signup/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
