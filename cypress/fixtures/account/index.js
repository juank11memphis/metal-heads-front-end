export const getAccountDetailsFixture = () => ({
  name: 'updateTokenUser',
  files: [
    'account/updateTokenUser',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.updateTokenUser;
  },
});

export const getChangePasswordFixture = () => ({
  name: 'changePassword',
  files: [
    'account/changePassword',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.changePassword;
  },
});

export const getSpotifyDisconnectFixture = () => ({
  name: 'spotifyDisconnect',
  files: [
    'account/spotifyDisconnect',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.spotifyDisconnect;
  },
});
