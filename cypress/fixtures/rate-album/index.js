export const getRateAlbumDataFixture = () => ({
  name: 'rateAlbum',
  files: [
    'rate-album/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withUserDataErrors }) => {
    if (withUserDataErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
