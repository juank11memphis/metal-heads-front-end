export const getPlayAlbumFixture = () => ({
  name: 'playAlbum',
  files: [
    'play-album/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withSpotifyErrors }) => {
    if (withSpotifyErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
