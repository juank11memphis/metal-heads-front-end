export const getForgotPasswordFixture = () => ({
  name: 'requestPasswordReset',
  files: [
    'forgot-password/valid',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.valid;
  },
});
