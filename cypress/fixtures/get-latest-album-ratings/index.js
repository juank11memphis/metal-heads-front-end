export const getLatestAlbumsRatingsFixture = () => ({
  name: 'getLatestAlbumRatings',
  files: ['get-latest-album-ratings/valid', 'errors/genericError'],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError
    }
    return data.valid
  },
})
