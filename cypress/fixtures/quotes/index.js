export const getActiveQuoteFixture = () => ({
  name: 'loadActiveQuote',
  files: [
    'quotes/activeQuote',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.activeQuote;
  },
});

export const getCreateQuoteFixture = () => ({
  name: 'createQuote',
  files: [
    'quotes/createQuote',
    'errors/genericError',
  ],
  resolve: (data, _, { withErrors }) => {
    if (withErrors) {
      return data.genericError;
    }
    return data.createQuote;
  },
});
