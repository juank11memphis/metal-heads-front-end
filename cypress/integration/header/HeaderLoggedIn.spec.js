import { setupTests } from '../../util'

describe('Header LoggedIn', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/', data, { loggedIn: true })
    })
  })

  it('should render properly when a user is logged in', () => {
    cy.get('.header-logo .header-logo__image')

    cy.get('.header-menu__user').contains('Juanca Morales')

    cy.get('.header-menu__user')
      .contains('Juanca Morales')
      .click()

    cy.get('.menu').contains('Profile')

    cy.get('.menu').contains('Account')

    cy.get('.menu').contains('Sign Out')
  })

  it('should navigate to the Profile Page when clicking on Profile', () => {
    cy.get('.header-menu__user')
      .contains('Juanca Morales')
      .click()

    cy.get('.menu')
      .contains('Profile')
      .click()

    cy.url().should('include', '/user/profile')
  })

  it('should navigate to the Account Page when clicking on account', () => {
    cy.get('.header-menu__user')
      .contains('Juanca Morales')
      .click()

    cy.get('.menu')
      .contains('Account')
      .click()

    cy.url().should('include', '/user/account/details')
  })

  it('should sign out current user when clicking on sign out', () => {
    cy.get('.header-menu__user')
      .contains('Juanca Morales')
      .click()

    cy.get('.menu')
      .contains('Sign Out')
      .click()

    cy.url().should('include', '/')

    cy.get('.header-menu')
      .find('header-menu__user')
      .should('have.length', 0)

    cy.get('.header-menu').find('.header-menu__menu')
  })
})
