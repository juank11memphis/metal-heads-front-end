import { setupTests } from '../../util'

describe('Header Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/', data, { withErrors: true })
    })
  })

  it('should handle errors on search bands', () => {
    cy.get('.header-menu .search').click()

    cy
      .get('.header-menu')
      .find('.global-search input')
      .type('Maiden')

    cy
      .get('.results')
      .find('.empty')
      .contains('No results found.')
  })
})
