import { setupTests } from '../../util'

describe('Header Logged Out', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/', data)
    })
  })

  it('should render properly when no user is logged in', () => {
    cy.get('.header-menu__menu .item').contains('Sign Up')

    cy.get('.header-menu__menu .item').contains('Sign In')
  })

  it('should navigate to home when clicking logo', () => {
    cy.get('.header-logo').click()

    cy.url().should('include', '/')

    cy.get('.home-page')
  })

  it('should navigate to signin page when clicking on signin', () => {
    cy.get('.header-menu__menu .item')
      .contains('Sign In')
      .click()

    cy.url().should('include', '/signin')

    cy.get('.signin-page')
  })

  it('should navigate to signup page when clicking on signup', () => {
    cy.get('.header-menu__menu .item')
      .contains('Sign Up')
      .click()

    cy.url().should('include', '/signup')

    cy.get('.signup-page')
  })

  it('should handle search when we have results', () => {
    cy.get('.header-menu .search').click()

    cy.get('.header-menu')
      .find('.global-search input')
      .type('Maiden', { delay: 280 })

    cy.get('.results')
      .find('.result')
      .should('have.length', 8)

    cy.get('.results')
      .find('.category')
      .should('have.length', 3)

    cy.get('.results')
      .find('.category .name')
      .contains('bands')

    cy.get('.results')
      .find('.category .name')
      .contains('artists')

    cy.get('.results')
      .find('.category .name')
      .contains('albums')

    // Validates bands items
    cy.get('.results')
      .find('.result:first')
      .contains('JT Bruce')

    cy.get('.results')
      .find('.result:first')
      .contains('United States')

    cy.get('.results')
      .find('.result:first')
      .contains('Progressive Rock/Metal')

    // Validates albums items
    cy.get('.results').contains('Bruce Dickinson Alive')

    cy.get('.results').contains('2005')

    cy.get('.results').contains('Boxed set')

    // Validates bands items
    cy.get('.results')
      .find('.result:last')
      .contains('Bruce Dickinson')

    cy.get('.results')
      .find('.result:last')
      .contains('Male')
  })

  it('should go to the band detail page when clicking on a band search result', () => {
    cy.get('.header-menu .search').click()

    cy.get('.header-menu')
      .find('.global-search input')
      .type('Maiden')

    cy.get('.results')
      .find('.result:first')
      .click()

    cy.url().should('include', '/bands/592f7fcdb7bb93268a46b589/info')
  })

  it('should go to the album detail page when clicking on an album search result', () => {
    cy.get('.header-menu .search').click()

    cy.get('.header-menu')
      .find('.global-search input')
      .type('Maiden')

    cy.get('.results')
      .contains('Bruce Dickinson Alive')
      .click()

    cy.url().should('include', '/albums/5a8bac66cb21d3d834ef43d9')
  })

  it('should go to the artist detail page when clicking on an artist search result', () => {
    cy.get('.header-menu .search').click()

    cy.get('.header-menu')
      .find('.global-search input')
      .type('Maiden')

    cy.get('.results')
      .find('.result:last')
      .click()

    cy.url().should('include', '/artists/5930c25979e4ad1e21394391')
  })

  it('should handle search when we have no results', () => {
    cy.get('.header-menu .search').click()

    cy.get('.header-menu')
      .find('.global-search input')
      .type('none', { delay: 280 })

    cy.get('.results')
      .find('.empty')
      .contains('No results found.')
  })

  it('should close search when clicking on the close icon', () => {
    cy.get('.header-menu .search').click()

    cy.get('.header-menu').find('.global-search input')

    cy.get('.header-menu .close').click()

    cy.get('.header-logo')
  })
})
