import { setupTests } from '../../util'

describe('Header LoggedInAsAdmin', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/', data, { loggedInAsAdmin: true })
    })
  })

  it('should render properly when a user is logged in as admin', () => {
    cy.get('.header-menu__user').contains('Juanca Morales')

    cy.get('.header-menu__user')
      .contains('Juanca Morales')
      .click()

    cy.get('.menu').contains('Account')

    cy.get('.menu').contains('Admin')

    cy.get('.menu').contains('Sign Out')
  })

  it('should navigate to the Admin Page when clicking on admin', () => {
    cy.get('.header-menu__user')
      .contains('Juanca Morales')
      .click()

    cy.get('.menu')
      .contains('Admin')
      .click()

    cy.url().should('include', '/admin')
  })
})
