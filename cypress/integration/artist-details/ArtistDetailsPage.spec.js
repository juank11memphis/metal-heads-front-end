import { setupTests } from '../../util';

describe('ArtistDetailPage', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.clearLocalStorage().should((ls) => {
          ls.setItem('metalHeadsBandSelectedId', '592f7fa02615c92686b9c5aa')
          ls.setItem('metalHeadsBandSelectedName', 'Iron Maiden')
          cy.visitStubbed('/artists/5930c25979e4ad1e2139438e', data);
        });
      });
  });

  it('should render properly', () => {
    // artist header - parent band link
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__header a h2')
      .contains('Iron Maiden');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__header a:first')
      .should('have.attr', 'href', '/bands/592f7fa02615c92686b9c5aa/info');

    // artist header - artist link
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__header a h2')
      .contains('Steve Harris');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__header a:last')
      .should('have.attr', 'href', '/artists/5930c25979e4ad1e2139438e');

    // artist image
    const expectedImageUrl
      = 'url("https://metalheads.imgix.net/artists/5930c25979e4ad1e2139438e_picture.jpg?auto=compress,enhance,redeye")'
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__image')
      .should('have.attr', 'style', `background-image: ${expectedImageUrl};`);

    // general info

    // Real name
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__general-info')
      .contains('Real Name: ');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__general-info')
      .contains('Stephen Percy Harris');

    // Place of Origin
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__general-info')
      .contains('Place of Origin: ');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__general-info')
      .contains('United Kingdom (Leytonstone, London)');

    // Age
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__general-info')
      .contains('Age: ');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__general-info')
      .contains('61 (born Mar 12th, 1956)');

    // Gender
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__general-info')
      .contains('Gender: ');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__general-info')
      .contains('Male');
  });

  it('should render active and past bands properly', () => {
    // Active Bands
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands-menu .active')
      .contains('Active Bands');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands .artist-detail-page__bands__item')
      .should('have.length', 2);

    // Band item
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands .artist-detail-page__bands__item:first a h4')
      .contains('Iron Maiden');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands .artist-detail-page__bands__item:first a')
      .should('have.attr', 'href', '/bands/592f7fa02615c92686b9c5aa/info');

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands .artist-detail-page__bands__item:first')
      .contains('Bass, Keyboards');

    // Past Bands
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands-menu')
      .contains('Past Bands')
      .click();

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands-menu .active')
      .contains('Past Bands')

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands .artist-detail-page__bands__item')
      .should('have.length', 2);

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands .artist-detail-page__bands__item:first a')
      .should('have.length', 0);

    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands .artist-detail-page__bands__item:first h4')
      .contains('Gypsy\'s Kiss');
  });

  it('should navigate to the band detail page after clicking on the band link on the header', () => {
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__header a h2')
      .contains('Iron Maiden')
      .click();

    cy.url()
      .should('include', '/bands/592f7fa02615c92686b9c5aa/info');

    cy.get('.band-detail-page');
  });

  it('should navigate to the band detail page after clicking on a band link on the active/past band tabs', () => {
    cy.get('.artist-detail-page')
      .find('.artist-detail-page__bands .artist-detail-page__bands__item:first a h4')
      .contains('Iron Maiden')
      .click();

    cy.url()
      .should('include', '/bands/592f7fa02615c92686b9c5aa/info');

    cy.get('.band-detail-page');
  });

});
