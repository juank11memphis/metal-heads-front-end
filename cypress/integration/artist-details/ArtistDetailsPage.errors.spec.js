import { setupTests } from '../../util';

describe('ArtistDetailPage Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/artists/5930c25979e4ad1e2139438e', data, { withErrors: true });
      });
  });

  it('should handle artist detail page errors', () => {
    cy.get('.artist-detail-page')
      .find('.error-message-container');
  });

});
