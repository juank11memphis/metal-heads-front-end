import { setupTests } from '../../../util'

describe('ProfilePage -> SavedBands -> No data', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/user/profile/11/saved', data, {
        loggedIn: true,
        noData: true,
      })
    })
  })

  it('should render properly', () => {
    cy.get('.profile-page')
      .find('.menu .active')
      .contains('Watchlist')

    cy.get('.profile-page').contains(
      'You have not added any bands to your watchlist',
    )
  })
})
