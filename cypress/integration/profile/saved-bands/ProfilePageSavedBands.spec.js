import { setupTests } from '../../../util'

describe('Profile Page -> Saved Bands -> Logged In', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/user/profile/11/saved', data, { loggedIn: true })
    })
  })

  it('should render properly', () => {
    cy.get('.profile-page')
      .find('.menu .active')
      .contains('Watchlist')

    cy.get('.profile-page')
      .find('.image-card-group .image-card')
      .should('have.length', 2)

    // Band card
    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .image-card__image')
      .should('have.attr', 'href', '/bands/592f7e006551c52677d3685b/info')

    const noImage =
      'url("https://corriente.us/wp-content/uploads/2014/09/no-image-available.jpg")'
    const expectedImageUrl =
      'url("https://metalheads.imgix.net/bands/592f7e006551c52677d3685b_picture.jpg?auto=compress,enhance,redeye")'

    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .image-card__image')
      .should(
        'have.attr',
        'style',
        `background-image: ${expectedImageUrl}, ${noImage};`,
      )

    cy.get('.profile-page')
      .find('.image-card-group .image-card:first h3')
      .contains('Death')

    cy.get('.profile-page')
      .find('.image-card-group')
      .contains('Megadeth')

    cy.get('.profile-page')
      .find('.image-card-group')
      .contains('Not Rated')

    cy.get('.profile-page')
      .find('.image-card-group .image-card:last')
      .contains('100%')

    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .band-actions i')
      .should('have.attr', 'class', 'star icon')
  })

  it('should call removeFromSavedBands when clicking on the star icon', () => {
    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .band-actions i')
      .click()
  })

  it('should navigate to the band details page after clicking on a band card', () => {
    cy.get('.profile-page')
      .find('.image-card-group .image-card:first')
      .click()

    cy.get('.band-detail-page')
  })

  it('should sort bands by global rating', () => {
    cy.get('.profile-page')
      .find('.sort-by-options .select__indicators')
      .click()

    cy.get('.profile-page')
      .find('.sort-by-options ')
      .find('.select__menu')
      .contains('Global Rating')
      .click()

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:first')
      .contains('Megadeth')

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:last')
      .contains('Death')
  })

  it('should sort bands by name', () => {
    cy.get('.profile-page')
      .find('.sort-by-options .select__indicators')
      .click()

    cy.get('.profile-page')
      .find('.sort-by-options')
      .find('.select__menu')
      .contains('Name')
      .click()

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:first')
      .contains('Death')

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:last')
      .contains('Megadeth')
  })
})
