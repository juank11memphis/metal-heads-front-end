import { setupTests } from '../../../util'

describe('ProfilePage -> MyBands -> No data', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/user/profile/11/rated', data, {
        loggedIn: true,
        noData: true,
      })
    })
  })

  it('should render properly', () => {
    cy.get('.profile-page')
      .find('.menu .active')
      .contains('Rated Bands')

    cy.get('.profile-page').contains('You have not rate any bands yet')
  })
})
