import { setupTests } from '../../../util'

describe('Profile Page -> My Bands -> Logged In', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/user/profile/11/rated', data, { loggedIn: true })
    })
  })

  it('should render properly', () => {
    cy.get('.profile-page')
      .find('.menu .active')
      .contains('Rated Bands')

    cy.get('.profile-page')
      .find('.image-card-group .image-card')
      .should('have.length', 39)

    // Band card
    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .image-card__image')
      .should('have.attr', 'href', '/bands/592f83d732de9226b40e0e7c/info')

    const noImage =
      'url("https://corriente.us/wp-content/uploads/2014/09/no-image-available.jpg")'
    const expectedImageUrl =
      'url("https://metalheads.imgix.net/bands/592f83d732de9226b40e0e7c_picture.jpg?auto=compress,enhance,redeye")'
    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .image-card__image')
      .should(
        'have.attr',
        'style',
        `background-image: ${expectedImageUrl}, ${noImage};`,
      )

    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .hbox')
      .contains('Saxon')

    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .hbox')
      .contains('85%')

    cy.get('.profile-page').find(
      '.image-card-group .image-card:first .slider-rating',
    )

    cy.get('.profile-page')
      .find('.image-card-group .image-card:first')
      .contains('Your Rating 10')
  })

  it('should navigate to the band details page after clicking on a band card', () => {
    cy.get('.profile-page')
      .find('.image-card-group .image-card:first')
      .click()

    cy.get('.band-detail-page')
  })

  it('should sort bands by global rating', () => {
    cy.get('.profile-page')
      .find('.sort-by-options .select__indicators')
      .click()

    cy.get('.profile-page')
      .find('.sort-by-options')
      .find('.select__menu')
      .contains('Global Rating')
      .click()

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:first')
      .contains('Iron Maiden')

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:last')
      .contains('Anvil')
  })

  it('should sort bands by user rating', () => {
    cy.get('.profile-page')
      .find('.sort-by-options .select__indicators')
      .click()

    cy.get('.profile-page')
      .find('.sort-by-options ')
      .find('.select__menu')
      .contains('User Rating')
      .click()

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:first')
      .contains('Saxon')

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:last')
      .contains('Anvil')
  })

  it('should sort bands by name', () => {
    cy.get('.profile-page')
      .find('.sort-by-options .select__indicators')
      .click()

    cy.get('.profile-page')
      .find('.sort-by-options')
      .find('.select__menu')
      .contains('Name')
      .click()

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:first')
      .contains('Accept')

    cy.get('.profile-page')
      .find('.image-card-group')
      .find('.image-card:last')
      .contains('Xentrix')
  })
})
