/* eslint-disable */
/// <reference types="cypress" />

import { setupTests } from '../../util'

describe('Profile Page -> Logged In', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/user/profile/11/rated', data, { loggedIn: true })
    })
  })

  it('should render header properly', () => {
    cy.get('.profile-page')
      .find('h1')
      .contains('Sandra Aguilar')

    cy.get('.profile-page')
      .find('.menu .active')
      .contains('Rated Bands')

    cy.get('.profile-page')
      .find('.menu')
      .contains('Watchlist')

    cy.get('.profile-page').find('.profile-page__header .sort-by-options')
  })
})
