import { setupTests } from '../../../util'

describe('Profile Page Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/user/profile/mybands', data, {
        withErrors: true,
        loggedIn: true,
      })
    })
  })

  it('should handle profile page -> myBands errors', () => {
    cy.get('.profile-page')
      .find('.menu')
      .contains('Rated Bands')
      .click()

    cy.get('.profile-page').find('.error-message-container')
  })

  it('should handle profile page -> savedBands errors', () => {
    cy.get('.profile-page')
      .find('.menu')
      .contains('Watchlist')
      .click()

    cy.get('.profile-page').find('.error-message-container')
  })
})
