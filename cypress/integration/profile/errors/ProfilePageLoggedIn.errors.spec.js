import { setupTests } from '../../../util'

describe('Profile Page Logged In Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/user/profile/11/saved', data, {
        loggedIn: true,
        withUserDataErrors: true,
      })
    })
  })

  it('should handle removeFromSavedBands errors', () => {
    cy.get('.profile-page')
      .find('.image-card-group .image-card:first .band-actions i')
      .click()
  })
})
