import { setupTests } from '../../util';

describe('AdminPage LoggedIn', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/admin', data, { loggedInAsAdmin: true });
      });
  });

  it('should render properly when a user is logged in', () => {
    cy.get('.admin-page')
      .find('.menu');
  });

});
