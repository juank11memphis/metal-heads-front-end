import { setupTests } from '../../../util';

describe('AdminPage Quotes Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/admin', data, { loggedInAsAdmin: true, withErrors: true });
      });
  });

  it('should handle create quote errors', () => {
    cy.get('.admin-page')
      .find('.form .field input[placeholder="Quote text"]')
      .type('Heavy metal is my life');

    cy.get('.admin-page')
      .find('button[type="submit"]')
      .contains('Add quote')
      .click();

    cy.get('.admin-page')
      .find('.error-message-container');
  });

});
