import { setupTests } from '../../../util';

describe('AdminPage Quotes no permissions', () => {

  const testRoles = [
    {
      name: 'admins',
      permissions: [
        'update:band',
      ],
      __typename: 'UserRole',
    },
  ];

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/admin', data, { loggedInWithRoles: testRoles });
      });
  });

  it('should not render the quotes tab', () => {
    cy.get('.admin-page')
      .find('.menu .item')
      .contains('Quotes')
      .should('not.be.visible');
  });

});
