import { setupTests } from '../../../util';

describe('AdminPage Quotes', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/admin', data, { loggedInAsAdmin: true });
      });
  });

  it('should render properly', () => {
    cy.get('.admin-page')
      .find('.menu .item')
      .contains('Quotes');

    cy.get('.admin-page')
      .find('.form .field input[placeholder="Quote text"]');

    cy.get('.admin-page')
      .find('.form .field input[placeholder="Quote author"]');

    cy.get('.admin-page')
      .find('button[type="submit"]')
      .contains('Add quote');
  });

  it('should show proper errors if clicking Add quote without required fields filled in', () => {
    cy.get('.admin-page')
      .find('button[type="submit"]')
      .contains('Add quote')
      .click();

    cy.get('.admin-page')
      .find('.error')
      .contains('Quote text is required');
  });

  it('should show proper errors if invalid data has been set on inputs', () => {
    cy.get('.admin-page')
      .find('.form .field input[placeholder="Quote text"]')
      .type('short');

    cy.get('.admin-page')
      .find('button[type="submit"]')
      .contains('Add quote')
      .click();

    cy.get('.admin-page')
      .find('.error')
      .contains('Quote text must have at least 6 characters');
  });

  it('should show success message after a valid user update', () => {
    cy.get('.admin-page')
      .find('.form .field input[placeholder="Quote text"]')
      .type('Heavy metal is my life');

    cy.get('.admin-page')
      .find('.form .field input[placeholder="Quote author"]')
      .type('Juanca');

    cy.get('.admin-page')
      .find('button[type="submit"]')
      .contains('Add quote')
      .click();

    cy.get('.admin-page')
      .find('.success-message-container')
      .contains('Quote added successfully');
  });

});
