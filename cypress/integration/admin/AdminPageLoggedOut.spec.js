import { setupTests } from '../../util';

describe('AdminPage LoggedOut', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/admin', data);
      });
  });

  it('should redirect to login when not logged in', () => {
    cy.url()
      .should('include', '/signin');

    cy.get('.signin-page');
  });

});
