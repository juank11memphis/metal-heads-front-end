import { setupTests } from '../../util'

describe('AdminPage LoggedIn with no permissions', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/admin', data, { loggedIn: true })
    })
  })

  it('should redirect to the login page when logged in with no admin permissions', () => {
    cy.url().should('include', '/')

    cy.get('.home-page')
  })
})
