import { setupTests } from '../../../util';

describe('AccountPageSocialAccounts', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/user/account/social-accounts', data, { loggedIn: true });
      });
  });

  it('should handle spotify disconnect', () => {
    cy.get('.account-page')
      .find('.account-page__social-accounts .checkbox')
      .click();

    cy.get('.account-page')
      .find('.account-page__social-accounts .checkbox')
      .contains('Connect to Spotify');

    cy.get('.account-page')
      .find('.account-page__social-accounts')
      .contains('Play albums on Spotify with a Spotify Premium Account');
  });

});
