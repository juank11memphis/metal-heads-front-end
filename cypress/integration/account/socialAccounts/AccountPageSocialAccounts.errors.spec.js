import { setupTests } from '../../../util';

describe('AccountPageSocialAccounts Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/user/account/social-accounts', data, { withErrors: true, loggedIn: true });
      });
  });

  it('should handle account page social accounts errors', () => {
    cy.get('.account-page')
      .find('.account-page__social-accounts .checkbox')
      .click();

    cy.get('.account-page')
      .find('.account-page__social-accounts .error-message-container');
  });

});
