import { setupTests } from '../../../util';

describe('AccountPageDetails Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/user/account/details', data, { withErrors: true, loggedIn: true });
      });
  });

  it('should handle account page details errors', () => {
    cy.get('.account-page')
      .find('button')
      .click();

    cy.get('.account-page')
      .find('.account-page__details .error-message-container');
  });

});
