import { setupTests } from '../../../util'

describe('Account Page Details', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/user/account/details', data, { loggedIn: true })
    })
  })

  it('should render properly', () => {
    cy.get('.account-page')
      .find('h1')
      .contains('Account Settings')

    // Account menu
    cy.get('.account-page')
      .find('.account-page__menu .active')
      .contains('Details')

    cy.get('.account-page')
      .find('.account-page__menu')
      .contains('Password')

    cy.get('.account-page')
      .find('.account-page__menu')
      .contains('Social Accounts')

    // Form
    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=firstname]')
      .should('have.attr', 'value', 'Juanca')

    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=lastname]')
      .should('have.attr', 'value', 'Morales')

    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=email]')
      .should('have.attr', 'value', 'juank.memphis@gmail.com')
      .should('have.attr', 'disabled')

    cy.get('.account-page')
      .find('.account-page__details .form .select-input')
      .contains('Costa Rica')

    cy.get('.account-page')
      .find('button')
      .contains('Save')
  })

  it('should show proper errors if clicking save without required fields filled in', () => {
    // Clears fields
    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=firstname]')
      .clear()

    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=lastname]')
      .clear()

    cy.get('.account-page')
      .find('button')
      .click()

    cy.get('.account-page')
      .find('.error')
      .contains('Your first name is required')

    cy.get('.account-page')
      .find('.error')
      .contains('Your last name is required')
  })

  it('should show proper errors if focusing on inputs without typing any data', () => {
    // Clears fields
    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=firstname]')
      .clear()

    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=lastname]')
      .clear()

    // Focus in focus out fields
    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=firstname]')
      .click()

    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=lastname]')
      .click()

    cy.get('.account-page')
      .find('.error')
      .contains('Your first name is required')

    cy.get('.account-page')
      .find('.error')
      .contains('Your last name is required')
  })

  it('should show proper errors if invalid data has been set on inputs', () => {
    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=firstname]')
      .clear()
      .type('a')

    cy.get('.account-page')
      .find('.account-page__details .form .field input[name=lastname]')
      .clear()
      .type('a')

    cy.get('.account-page')
      .find('button')
      .click()

    cy.get('.account-page')
      .find('.error')
      .contains('Your first name must have at least 2 characters')

    cy.get('.account-page')
      .find('.error')
      .contains('Your last name must have at least 2 characters')
  })

  it('should show success message after a valid user update', () => {
    cy.get('.account-page')
      .find('button')
      .click()

    cy.get('.account-page')
      .find('.success-message-container')
      .contains('User updated successfully')
  })
})
