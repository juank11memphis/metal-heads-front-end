import { setupTests } from '../../../util';

describe('AccountPage Passwords', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/user/account/password', data, { loggedIn: true });
      });
  });

  it('should render properly', () => {
    cy.get('.account-page')
      .find('.account-page__menu .active')
      .contains('Password');

    // Form
    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=password]');

    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=newPassword]');

    cy.get('.account-page')
      .find('button')
      .contains('Change Password');
  });

  it('should show proper errors if clicking save without required fields filled in', () => {
    cy.get('.account-page')
      .find('button')
      .contains('Change Password')
      .click();

    cy.get('.account-page')
      .find('.error')
      .contains('Your password is required');

    cy.get('.account-page')
      .find('.error')
      .contains('Your new password is required');
  });

  it('should show proper errors if focusing on inputs without typing any data', () => {
    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=password]')
      .click();

    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=newPassword]')
      .click();

    cy.get('.account-page')
      .click();

    cy.get('.account-page')
      .find('.error')
      .contains('Your password is required');

    cy.get('.account-page')
      .find('.error')
      .contains('Your new password is required');
  });

  it('should show proper errors if invalid data has been set on inputs', () => {
    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=password]')
      .type('short');

    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=newPassword]')
      .type('short');

    cy.get('.account-page')
      .find('button')
      .contains('Change Password')
      .click();

    cy.get('.account-page')
      .find('.error')
      .contains('Your password must have at least 6 characters');

    cy.get('.account-page')
      .find('.error')
      .contains('Your new password must have at least 6 characters');
  });

  it('should show success message after a valid user update', () => {
    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=password]')
      .type('good new password');

    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=newPassword]')
      .type('good new password');

    cy.get('.account-page')
      .find('button')
      .contains('Change Password')
      .click();

    cy.get('.account-page')
      .find('.success-message-container')
      .contains('Password changed successfully');
  });

});
