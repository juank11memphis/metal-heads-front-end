import { setupTests } from '../../../util';

describe('AccountPagePasswords Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/user/account/password', data, { withErrors: true, loggedIn: true });
      });
  });

  it('should handle account page password errors', () => {
    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=password]')
      .type('good new password');

    cy.get('.account-page')
      .find('.account-page__passwords .form .field input[name=newPassword]')
      .type('good new password');

    cy.get('.account-page')
      .find('button')
      .contains('Change Password')
      .click();

    cy.get('.account-page')
      .find('.account-page__passwords .error-message-container');
  });

});
