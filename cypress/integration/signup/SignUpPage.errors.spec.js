import { setupTests } from '../../util'

describe('Sign Up Page Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/signup', data, { withErrors: true })
    })
  })

  it('should handle sign up errors', () => {
    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your first name"]')
      .type('Juan')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your last name"]')
      .type('Morales')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('valid@email.com')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your password"]')
      .type('good-password')

    cy.get('.signup-page')
      .find('.form .field .select-input .select__indicators')
      .click()

    cy.get('.select__menu')
      .find('.select__option:first')
      .click()

    cy.get('.signup-page')
      .find('button[type="submit"]')
      .contains('Sign Up')
      .click()

    cy.get('.signup-page').find('.error-message-container')
  })
})
