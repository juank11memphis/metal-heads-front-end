import { setupTests } from '../../util'

describe('SignUp Page', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/signup', data)
    })
  })

  it('should render properly', () => {
    cy.get('.signup-page')
      .find('h2')
      .contains('Become a MetalHead!')

    cy.get('.signup-page')
      .find('.social-accounts-buttons .social-accounts-buttons__google-button')
      .contains('Sign up with Google')

    cy.get('.signup-page')
      .find('.social-accounts-buttons__spotify-button')
      .contains('Sign up with Spotify')

    cy.get('.signup-page')
      .find('.signup-page__divider')
      .contains('Or use your email')

    cy.get('.signup-page').find(
      '.form .field input[placeholder="Your first name"]',
    )

    cy.get('.signup-page').find(
      '.form .field input[placeholder="Your last name"]',
    )

    cy.get('.signup-page').find(
      '.form .field input[placeholder="Your email address"]',
    )

    cy.get('.signup-page').find(
      '.form .field input[placeholder="Your password"]',
    )

    cy.get('.signup-page')
      .find('.form .field .select-input')
      .contains('Your country')

    cy.get('.signup-page')
      .find('button[type="submit"]')
      .contains('Sign Up')

    cy.get('.signup-page')
      .find('.signup-page__to-signin')
      .contains('Already a MetalHead?')

    cy.get('.signup-page')
      .find('.signup-page__to-signin a')
      .should('have.attr', 'href', '/signin')
      .contains('Sign In')
  })

  it('should go to the SignInPage after clicking the Sign In link', () => {
    cy.get('.signup-page')
      .find('.signup-page__to-signin a')
      .contains('Sign In')
      .click()

    cy.url().should('include', 'signin')
  })

  it('should show proper errors if clicking sign in without required fields filled in', () => {
    cy.get('.signup-page')
      .find('button[type="submit"]')
      .contains('Sign Up')
      .click()

    cy.get('.signup-page')
      .find('.error')
      .contains('Your first name is required')

    cy.get('.signup-page')
      .find('.error')
      .contains('Your last name is required')

    cy.get('.signup-page')
      .find('.error')
      .contains('Your email is required')

    cy.get('.signup-page')
      .find('.error')
      .contains('Your password is required')

    cy.get('.signup-page')
      .find('.error')
      .contains('Your country is required')
  })

  it('should show proper errors if focusing on inputs without typing any data', () => {
    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your first name"]')
      .click()

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your last name"]')
      .click()

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your email address"]')
      .click()

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your password"]')
      .click()

    cy.get('.signup-page')
      .find('.form .field .select-input')
      .click()

    cy.get('.signup-page').click()

    cy.get('.signup-page')
      .find('.error')
      .contains('Your first name is required')

    cy.get('.signup-page')
      .find('.error')
      .contains('Your last name is required')

    cy.get('.signup-page')
      .find('.error')
      .contains('Your email is required')

    cy.get('.signup-page')
      .find('.error')
      .contains('Your password is required')
  })

  it('should show proper errors if invalid data has been set on inputs', () => {
    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your first name"]')
      .type('a')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your last name"]')
      .type('a')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('invalid email')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your password"]')
      .type('short')

    cy.get('.signup-page').click()

    cy.get('.signup-page')
      .find('.error')
      .contains('Your first name must have at least 2 characters')

    cy.get('.signup-page')
      .find('.error')
      .contains('Your last name must have at least 2 characters')

    cy.get('.signup-page')
      .find('.error')
      .contains('Invalid email')

    cy.get('.signup-page')
      .find('.error')
      .contains('Password must have at least 6 characters')
  })

  it('should go to HomePage after a valid sign up', () => {
    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your first name"]')
      .type('Juan')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your last name"]')
      .type('Morales')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('valid@email.com')

    cy.get('.signup-page')
      .find('.form .field input[placeholder="Your password"]')
      .type('good-password')

    cy.get('.signup-page')
      .find('.form .field .select-input .select__indicators')
      .click()

    cy.get('.select__menu')
      .find('.select__option:first')
      .click()

    cy.get('.signup-page')
      .find('button[type="submit"]')
      .contains('Sign Up')
      .click()

    cy.url().should('include', '/')

    cy.get('.home-page')

    cy.get('.header-menu__user').contains('Juanca Morales')
  })
})
