import { setupTests } from '../../../util';

describe('AlbumDetailsPage -> logged in -> without user data', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/albums/5a57fa6d500d500b58f3419c', data, { loggedIn: true, withUserData: false });
      });
  });

  it('should render album detail user data related stuff', () => {
    cy.get('.album-detail-page')
      .find('.slider-rating')
      .contains('Add Your Rating');

    cy.get('.album-detail-page')
      .find('.global-rating')
      .contains('Not rated');
  });

  it('should handle rate an album', () => {
    cy.get('.album-detail-page')
      .find('.slider-rating')
      .find('.rc-slider')
      .click();

    cy.get('.album-detail-page')
      .find('.slider-rating')
      .contains('5');
  });

});
