import { setupTests } from '../../../util'

describe('AlbumDetailsPage -> logged in -> with user data', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/albums/59fd2859afb450411dfc5294', data, {
        loggedIn: true,
        withUserData: true,
      })
    })
  })

  it('should render album detail user data related stuff', () => {
    cy
      .get('.album-detail-page')
      .find('.slider-rating')
      .contains('Your Rating 9.5')

    cy.get('.album-detail-page').find('.slider-rating .slider-rating__remove')

    cy
      .get('.album-detail-page')
      .find('.global-rating')
      .contains('83%')

    cy
      .get('.album-detail-page')
      .find('.global-rating .user-ratings')
      .contains('User Ratings: 2')
  })

  it('should render play on youtube and spotify', () => {
    cy
      .get('.album-detail-page .album-detail-page__header .play-buttons')
      .contains('Spotify')

    cy.get(
      '.album-detail-page .album-detail-page__header .play-buttons .spotify',
    )

    cy
      .get('.album-detail-page .album-detail-page__header .play-buttons')
      .contains('Youtube')

    cy.get(
      '.album-detail-page .album-detail-page__header .play-buttons .youtube',
    )
  })
})
