import { setupTests } from '../../../util'

describe('AlbumDetailPage logged out', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/albums/59fd2859afb450411dfc5294', data)
    })
  })

  it('should render properly', () => {
    // album header - parent band link
    cy.get('.album-detail-page')
      .find('.album-detail-page__header a h2')
      .contains('Iron Maiden')

    cy.get('.album-detail-page')
      .find('.album-detail-page__header a:first')
      .should('have.attr', 'href', '/bands/592f7fa02615c92686b9c5aa/albums')

    // artist header - artist link
    cy.get('.album-detail-page')
      .find('.album-detail-page__header a h2')
      .contains('Piece of Mind')

    cy.get('.album-detail-page')
      .find('.album-detail-page__header a:last')
      .should('have.attr', 'href', '/albums/59fd2859afb450411dfc5294')

    // artist image
    const expectedImageUrl =
      'url("https://metalheads.imgix.net/albums/59fd2859afb450411dfc5294_picture.jpg?auto=compress,enhance,redeye")'
    cy.get('.album-detail-page')
      .find('.album-detail-page__image')
      .should('have.attr', 'style', `background-image: ${expectedImageUrl};`)

    // general info

    // Type
    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('Type: ')

    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('Full-length')

    // Release Date
    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('Release Date: ')

    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('May 16th, 1983')

    // Catalog ID
    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('Catalog ID: ')

    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('EMA 800')

    // Label
    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('Label: ')

    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('EMI')

    // Format
    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('Format: ')

    cy.get('.album-detail-page')
      .find('.album-detail-page__general-info')
      .contains('12" vinyl (33⅓ RPM)')
  })

  it('should render play on youtube', () => {
    cy.get(
      '.album-detail-page .album-detail-page__header .play-buttons',
    ).contains('Youtube')

    cy.get(
      '.album-detail-page .album-detail-page__header .play-buttons .youtube',
    )
  })

  it('should render songs properly', () => {
    cy.get('.album-detail-page')
      .find('.menu .active')
      .contains('Songs')

    cy.get('.album-detail-page')
      .find('.album-detail-page__songs-grid .row')
      .should('have.length', 9)

    // Song item
    cy.get('.album-detail-page')
      .find('.album-detail-page__songs-grid .row')
      .contains('1. Where Eagles Dare')

    cy.get('.album-detail-page')
      .find('.album-detail-page__songs-grid .row')
      .contains('06:13')

    cy.get('.album-detail-page')
      .find('.album-detail-page__songs-grid .row')
      .find('.icon[data-id=musixmatch-icon]')
      .should('have.length', 9)
  })

  it('should render lineup properly', () => {
    cy.get('.album-detail-page')
      .find('.menu')
      .contains('Line Up')
      .click()

    cy.get('.album-detail-page')
      .find('.menu .active')
      .contains('Line Up')

    cy.get('.album-detail-page')
      .find('.album-detail-page__line-up .album-detail-page__line-up__item')
      .should('have.length', 5)

    // Line Up Item
    cy.get('.album-detail-page')
      .find(
        '.album-detail-page__line-up .album-detail-page__line-up__item:first a h4',
      )
      .contains('Bruce Dickinson')

    cy.get('.album-detail-page')
      .find(
        '.album-detail-page__line-up .album-detail-page__line-up__item:first a',
      )
      .should('have.attr', 'href', '/artists/5930c25979e4ad1e21394391')

    cy.get('.album-detail-page')
      .find(
        '.album-detail-page__line-up .album-detail-page__line-up__item:first',
      )
      .contains('Vocals')
  })

  it('should navigate to the band detail page after clicking on the band link on the header', () => {
    cy.get('.album-detail-page')
      .find('.album-detail-page__header a h2')
      .contains('Iron Maiden')
      .click()

    cy.url().should('include', '/bands/592f7fa02615c92686b9c5aa/albums')

    cy.get('.band-detail-page')
  })

  it('should navigate to the artist detail page after clicking on an artist link on the line up tab', () => {
    cy.get('.album-detail-page')
      .find('.menu')
      .contains('Line Up')
      .click()

    cy.get('.album-detail-page')
      .find(
        '.album-detail-page__line-up .album-detail-page__line-up__item a h4',
      )
      .contains('Steve Harris')
      .click()

    cy.url().should('include', '/artists/5930c25979e4ad1e2139438e')

    cy.get('.artist-detail-page')
  })
})
