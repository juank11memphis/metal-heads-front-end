import { setupTests } from '../../../util';

describe('AlbumDetailPage -> logged out -> no ratings data', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/albums/5a57fa6d500d500b58f3419c', data);
      });
  });

  it('should render globalRating when album has not been rated yet', () => {
    cy.get('.album-detail-page')
      .find('.global-rating')
      .contains('Not rated');
  });

});
