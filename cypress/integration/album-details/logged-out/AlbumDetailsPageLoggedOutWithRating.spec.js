import { setupTests } from '../../../util'

describe('AlbumDetailPage -> logged out -> ratings data', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/albums/59fd2859afb450411dfc5294', data)
    })
  })

  it('should render globalRating when album has been rated already', () => {
    cy
      .get('.album-detail-page')
      .find('.global-rating')
      .contains('83%')
  })
})
