import { setupTests } from '../../../util';

describe('AlbumDetailPage Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/albums/59fd2859afb450411dfc5294', data, { withErrors: true });
      });
  });

  it('should handle album detail page errors', () => {
    cy.get('.album-detail-page')
      .find('.error-message-container');
  });

});
