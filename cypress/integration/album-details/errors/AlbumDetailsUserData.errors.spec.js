/* eslint-disable */
/// <reference types="cypress" />

import { setupTests } from '../../../util'

describe('AlbumDetail UserData Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/albums/59fd2859afb450411dfc5294', data, {
        loggedIn: true,
        withUserDataErrors: true,
      })
    })
  })

  it('should handle errors on rate album mutation', () => {
    cy.get('.album-detail-page')
      .find('.slider-rating')
      .find('.rc-slider')
      .click()

    cy.get('.album-detail-page')
      .find('.slider-rating')
      .contains('5')
  })
})
