import { setupTests } from '../../../util'

describe('BandDetailPage Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fa02615c92686b9c5aa/info', data, {
        withErrors: true,
      })
    })
  })

  it('should handle band detail page errors', () => {
    cy.get('.band-detail-page').find('.error-message-container')
  })
})
