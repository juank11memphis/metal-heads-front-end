import { setupTests } from '../../../util'

describe('BandDetail UserData Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fcdb7bb93268a46b597/info', data, {
        loggedIn: true,
        withUserDataErrors: true,
      })
    })
  })

  it('should handle band detail user data errors', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('have.attr', 'data-tooltip', 'Add to watchlist')
      .click()

    cy.get('.band-detail-page').find(
      '.band-detail-page__header__action-buttons .slider-rating',
    )
  })

  it('should handle errors on removeFromSaveBands and saveForLater mutations', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('have.attr', 'data-tooltip', 'Add to watchlist')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('have.attr', 'data-tooltip', 'Remove from watchlist')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('have.attr', 'data-tooltip', 'Add to watchlist')
      .click()
  })

  it('should handle errors on rate band mutation', () => {
    cy.get('.band-detail-page')
      .find(
        '.band-detail-page__header__action-buttons .slider-rating .rc-slider',
      )
      .click()
  })

  it('should handle errors on rate album mutation', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .find('.rc-slider')
      .click()
  })

  it('should handle errors on remove band rating mutation', () => {
    cy.get('.band-detail-page')
      .find(
        '.band-detail-page__header__action-buttons .slider-rating .rc-slider',
      )
      .click()

    cy.get('.band-detail-page')
      .find(
        '.band-detail-page__header__action-buttons .slider-rating .slider-rating__remove',
      )
      .click()
  })

  it('should handle errors on remove album rating mutation', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.image-card:first .album-card .slider-rating .rc-slider')
      .click()

    cy.get('.band-detail-page__discography')
      .find(
        '.image-card:first .album-card .slider-rating .slider-rating__remove',
      )
      .click()
  })
})
