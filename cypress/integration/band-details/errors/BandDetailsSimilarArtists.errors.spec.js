import { setupTests } from '../../../util'

describe('BandDetail SimilarArtists Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fa02615c92686b9c5aa/similar', data, {
        withErrors: true,
      })
    })
  })

  it('should handle band detail similar artists errors', () => {
    cy.get('.band-detail-page').find('.error-message-container')
  })
})
