import { setupTests } from '../../../util'

describe('BandDetailsPage -> logged in -> without user data', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fcdb7bb93268a46b597/info', data, {
        loggedIn: true,
        withUserData: false,
      })
    })
  })

  it('should not render the remove rating icon', () => {
    cy
      .get('.band-detail-page')
      .find(
        '.band-detail-page__header__action-buttons .slider-rating .slider-rating__remove',
      )
      .should('not.be.visible')
  })

  it('should render band detail user data related stuff', () => {
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('have.attr', 'data-tooltip', 'Add to watchlist')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .slider-rating')
      .should('be.visible')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .global-rating')
      .should('be.visible')
  })

  it('should remove band from saved bands and add it again', () => {
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('have.attr', 'data-tooltip', 'Add to watchlist')
      .click()

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('have.attr', 'data-tooltip', 'Remove from watchlist')
      .click()

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('have.attr', 'data-tooltip', 'Add to watchlist')
  })

  it('should render albums cards user data', () => {
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .contains('Add Your Rating')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .find('.rc-slider')
  })

  it('should render albums list items user data', () => {
    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.menu')
      .contains('Live')
      .click()

    cy
      .get('.band-detail-page__discography')
      .find('.albums-list__item:first')
      .contains('Add Your Rating')

    cy
      .get('.band-detail-page__discography')
      .find('.albums-list__item:first')
      .find('.rc-slider')
  })

  it('should handle rate an album', () => {
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .find('.rc-slider')
      .click()
  })
})
