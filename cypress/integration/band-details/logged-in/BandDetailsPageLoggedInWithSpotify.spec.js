import { setupTests } from '../../../util'

describe('BandDetailsPage -> logged in -> connected to spotify', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fcdb7bb93268a46b597/info', data, {
        loggedIn: true,
      })
    })
  })

  it('should handle play on spotify', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.image-card:first .image-card__image')
      .trigger('mouseover')

    cy.get('.band-detail-page')
      .find('.image-card:first .play-buttons .spotify')
      .click()

    cy.get('.band-detail-page .spotify-devices-modal .header').contains(
      'Spotify Active Devices',
    )

    cy.get('.band-detail-page .image-card:first')
      .find('.spotify-devices-modal')
      .find('.select-input .select__indicators')
      .click()

    cy.get('.band-detail-page .image-card:first')
      .find('.spotify-devices-modal')
      .find('.select__menu')
      .contains('Web Player (Chrome)')
      .click()

    cy.get('.band-detail-page .image-card:first')
      .find('.spotify-devices-modal .rodal-confirm-btn')
      .click()

    cy.get('.s-alert-wrapper .s-alert-success .s-alert-box-inner')
  })
})
