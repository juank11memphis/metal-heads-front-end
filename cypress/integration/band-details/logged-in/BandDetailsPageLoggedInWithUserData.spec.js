import { setupTests } from '../../../util'

describe('Band Details Page -> logged in -> with user data', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7c0b920666266c1cef34/info', data, {
        loggedIn: true,
        withUserData: true,
      })
    })
  })

  it('should render band detail user data related stuff', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .save-for-later-button')
      .should('not.be.visible')

    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .slider-rating')
      .should('be.visible')

    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons .global-rating')
      .should('be.visible')
  })

  it('should render globalRating when band has been already rated', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons')
      .find('.global-rating')
      .contains('80%')

    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons')
      .find('.global-rating .user-ratings')
      .contains('User Ratings: 1')
  })

  it('should render albums cards user data', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .contains('Your Rating')

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .contains('9.5')
  })

  it('should render albums list items user data', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.menu')
      .contains('Live')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.albums-list__item:first')
      .contains('Your Rating')

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.albums-list__item:first')
      .contains('8')
  })

  it('should sort albums by year', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.hide-on-mobile .sort-by-options .select__indicators')
      .click()

    cy.get('.band-detail-page')
      .find('.sort-by-options ')
      .find('.select__menu')
      .contains('Year')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .contains('Black Sabbath')

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:last')
      .contains('13')
  })

  it('should sort albums by name', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.hide-on-mobile .sort-by-options .select__indicators')
      .click()

    cy.get('.band-detail-page')
      .find('.sort-by-options ')
      .find('.select__menu')
      .contains('Name')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .contains('13')

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:last')
      .contains('Tyr')
  })

  it('should sort albums by globalRating', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.hide-on-mobile .sort-by-options .select__indicators')
      .click()

    cy.get('.band-detail-page')
      .find('.sort-by-options ')
      .find('.select__menu')
      .contains('Global Rating')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .contains('Paranoid')

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:last')
      .contains('Mob Rules')
  })

  it('should sort albums by user rating', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header .menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.hide-on-mobile .sort-by-options .select__indicators')
      .click()

    cy.get('.band-detail-page')
      .find('.sort-by-options')
      .find('.select__menu')
      .contains('User Rating')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:first')
      .contains('Paranoid')

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.image-card:last')
      .contains('Mob Rules')
  })
})
