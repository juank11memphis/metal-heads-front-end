import { setupTests } from '../../../util'

describe('BandDetailsPage -> logged in -> not connected to spotify', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fcdb7bb93268a46b597/info', data, {
        loggedInNoSpotify: true,
      })
    })
  })

  it('should render play on youtube and spotify on album cards', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.image-card:first .image-card__image')
      .trigger('mouseover')

    cy.get('.band-detail-page .play-buttons').should(
      'have.attr',
      'style',
      'opacity: 1;',
    )

    cy.get('.band-detail-page .play-buttons').contains('Youtube')

    cy.get('.band-detail-page .play-buttons .youtube')

    cy.get('.band-detail-page .play-buttons').contains('Spotify')

    cy.get('.band-detail-page .play-buttons .spotify')
  })

  it('should render play on youtube and spotify on album list items', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Live')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.albums-list__item:first .play-buttons')
      .contains('Youtube')

    cy.get('.band-detail-page__discography').find(
      '.albums-list__item:first .play-buttons .youtube',
    )

    cy.get('.band-detail-page__discography')
      .find('.albums-list__item:first .play-buttons')
      .contains('Spotify')

    cy.get('.band-detail-page__discography').find(
      '.albums-list__item:first .play-buttons .spotify',
    )
  })

  it('should show alert when clicking on spotify but user is not connected to spotify', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.image-card:first .image-card__image')
      .trigger('mouseover')

    cy.get('.band-detail-page')
      .find('.image-card:first .play-buttons .spotify')
      .click()

    cy.get('.s-alert-wrapper .s-alert-info .s-alert-box-inner')
  })
})
