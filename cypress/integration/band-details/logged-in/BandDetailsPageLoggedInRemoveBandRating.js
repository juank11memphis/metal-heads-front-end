import { setupTests } from '../../../util'

describe('BandDetailsPage -> logged in -> remove band rating', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7e5f7f218d267bef2ba6/info', data, {
        loggedIn: true,
        withUserData: true,
      })
    })
  })

  it('should render the remove rating icon', () => {
    cy.get('.band-detail-page')
      .find(
        '.band-detail-page__header__action-buttons .slider-rating .slider-rating__remove',
      )
      .should('be.visible')
      .should('have.attr', 'data-tooltip', 'Remove rating')
  })

  it('should handle remove band rating', () => {
    cy.get('.band-detail-page')
      .find(
        '.band-detail-page__header__action-buttons .slider-rating .slider-rating__remove',
      )
      .click()

    cy.get('.band-detail-page')
      .find(
        '.band-detail-page__header__action-buttons .slider-rating .slider-rating__remove',
      )
      .should('not.be.visible')
  })

  it('should handle remove album rating on album cards', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page__discography')
      .find(
        '.image-card:first .album-card .slider-rating .slider-rating__remove',
      )
      .click()
  })

  it('should handle remove album rating on album list items', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Live')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.albums-list__item:first .slider-rating .slider-rating__remove')
      .click()
  })
})
