import { setupTests } from '../../../util'

describe('Band Detail Page -> logged out -> with ratings data', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fcdb7bb93268a46b597/info', data)
    })
  })

  it('should render globalRating when band been rated already', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons')
      .find('.global-rating')
      .contains('90%')
  })

  it('should render albums globalRating for albums that have been rated already', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.image-card:last')
      .contains('10%')
  })

  it('should render albums list items globalRating for albums that have been rated already', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.menu')
      .contains('Live')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.albums-list__item:last')
      .contains('10%')
  })
})
