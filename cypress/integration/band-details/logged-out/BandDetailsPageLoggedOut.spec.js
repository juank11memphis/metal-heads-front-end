import { setupTests } from '../../../util'

describe('BandDetailPage -> logged out', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fa02615c92686b9c5aa/info', data)
    })
  })

  it('should render general info and current line up properly', () => {
    const expectedImageUrl =
      'https://metalheads.imgix.net/bands/592f7fa02615c92686b9c5aa_picture.jpg?auto=compress,enhance,redeye'
    // band image
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__band-image')
      .should(
        'have.attr',
        'style',
        `background-image: url("${expectedImageUrl}");`,
      )

    // band header -> title and menu
    cy
      .get('.band-detail-page')
      .find('h1')
      .contains('Iron Maiden')

    cy
      .get('.band-detail-page')
      .find('.menu .active')
      .contains('General Info')

    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Discography')

    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Similar Artists')

    // Band general info

    // Genre
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Genre: ')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Heavy Metal, NWOBHM')

    // Country
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Country: ')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('United Kingdom')

    // Location
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Location: ')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('London, England')

    // Formed In
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Formed In: ')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('1975')

    // Years Active
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Years Active: ')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('1975-present')

    // Status
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Status: ')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Active')

    // Lyrical Themes
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('Lyrical Themes: ')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__general-info')
      .contains('History, Literature, War, Mythology, Society, Religion')

    // Current LineUp
    cy
      .get('.band-detail-page')
      .find('h2')
      .contains('Current Lineup')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__band-members .image-card')
      .should('have.length', 6)

    // Current LineUp card
    const noImage =
      'url("https://corriente.us/wp-content/uploads/2014/09/no-image-available.jpg")'
    const cardImage =
      'url("https://metalheads.imgix.net/artists/5930c25979e4ad1e2139438e_picture.jpg?auto=compress,enhance,redeye")'
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__band-members .image-card:first')
      .find('.image-card__image')
      .should(
        'have.attr',
        'style',
        `background-image: ${cardImage}, ${noImage};`,
      )
      .should('have.attr', 'href', '/artists/5930c25979e4ad1e2139438e')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__band-members .image-card:first')
      .find('.image-card__content')
      .contains('Steve Harris')

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__band-members .image-card:first')
      .find('.image-card__content h4')
  })

  it('should navigate to the artist detail page when clicking on a member card', () => {
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__band-members .image-card:first')
      .click()

    cy.url().should('include', '/artists/5930c25979e4ad1e2139438e')

    cy.get('.artist-detail-page')
  })

  it('should render play on youtube on album cards', () => {
    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy
      .get('.band-detail-page__discography')
      .find('.image-card:first .image-card__image')
      .trigger('mouseover')

    cy
      .get('.band-detail-page .play-buttons')
      .should('have.attr', 'style', 'opacity: 1;')

    cy.get('.band-detail-page .play-buttons').contains('Youtube')

    cy.get('.band-detail-page .play-buttons .youtube')
  })

  it('should render play on youtube on album list items', () => {
    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Live')
      .click()

    cy
      .get('.band-detail-page__discography')
      .find('.albums-list__item:first .play-buttons')
      .contains('Youtube')

    cy
      .get('.band-detail-page__discography')
      .find('.albums-list__item:first .play-buttons .youtube')
  })

  it('should render the discography properly', () => {
    // selecting the discography menu option
    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy
      .get('.band-detail-page')
      .find('.menu .active')
      .contains('Discography')

    cy.url().should('include', '/bands/592f7fa02615c92686b9c5aa/albums')

    // Album card
    const noImage =
      'url("https://corriente.us/wp-content/uploads/2014/09/no-image-available.jpg")'
    const cardImage =
      'url("https://metalheads.imgix.net/albums/59fd2859afb450411dfc5285_picture.jpg?auto=compress,enhance,redeye")'
    cy
      .get('.band-detail-page__discography')
      .find('.image-card:first .image-card__image')
      .should('have.attr', 'href', '/albums/59fd2859afb450411dfc5285')
      .should(
        'have.attr',
        'style',
        `background-image: ${cardImage}, ${noImage};`,
      )

    cy
      .get('.band-detail-page__discography')
      .find('.image-card:first .image-card__content')
      .contains('Iron Maiden')

    cy
      .get('.band-detail-page__discography')
      .find('.image-card:first .image-card__content')
      .contains('1980')

    // full length albums
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu .active')
      .contains('Full-length')

    cy
      .get('.band-detail-page__discography')
      .find('.image-card .image-card__image')
      .should('have.length', 16)

    // Live albums
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Live')
      .click()

    cy
      .get('.albums-list')
      .find('.albums-list__item')
      .should('have.length', 10)

    // Video albums
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Video')
      .click()

    cy
      .get('.albums-list')
      .find('.albums-list__item')
      .should('have.length', 17)

    // Compilations albums
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Compilations')
      .click()

    cy
      .get('.albums-list')
      .find('.albums-list__item')
      .should('have.length', 16)

    // Albums list item
    cy
      .get('.albums-list')
      .find('.albums-list__item:first')
      .contains('Best of the Beast')

    cy
      .get('.albums-list')
      .find('.albums-list__item:first')
      .contains('1996')

    cy
      .get('.albums-list')
      .find('.albums-list__item:first')
      .contains('Compilation')

    // Singles albums
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Singles')
      .click()

    cy
      .get('.albums-list')
      .find('.albums-list__item')
      .should('have.length', 45)

    // Other albums
    cy
      .get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Other')
      .click()

    cy
      .get('.albums-list')
      .find('.albums-list__item')
      .should('have.length', 14)
  })

  it('should navigate to the album detail page when clicking on an album card', () => {
    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy
      .get('.band-detail-page__discography')
      .find('.image-card:first')
      .click()

    cy.url().should('include', '/albums/59fd2859afb450411dfc5285')

    cy.get('.album-detail-page')
  })

  it('should navigate to the album detail page when clicking on an album list item', () => {
    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy
      .get('.band-detail-page')
      .find('.band-detail-page__content')
      .find('.menu')
      .contains('Live')
      .click()

    cy
      .get('.albums-list')
      .find('.albums-list__item:first h4')
      .click()

    cy.url().should('include', '/albums/59fd2859afb450411dfc529c')

    cy.get('.album-detail-page')
  })

  it('should render similar artists properly', () => {
    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Similar Artists')
      .click()

    cy
      .get('.band-detail-page__similar-bands')
      .find('.image-card')
      .should('have.length', 10)

    // Similar Artist Card
    const noImage =
      'url("https://corriente.us/wp-content/uploads/2014/09/no-image-available.jpg")'
    const cardImage =
      'url("https://metalheads.imgix.net/bands/592f7b964c27e526667533e5_picture.jpg?auto=compress,enhance,redeye")'
    cy
      .get('.band-detail-page__similar-bands')
      .find('.image-card:first .image-card__image')
      .should(
        'have.attr',
        'style',
        `background-image: ${cardImage}, ${noImage};`,
      )
      .should('have.attr', 'href', '/bands/592f7b964c27e526667533e5/info')

    cy
      .get('.band-detail-page__similar-bands')
      .find('.image-card:first .image-card__content h4')
      .contains('Angel Witch')

    cy
      .get('.band-detail-page__similar-bands')
      .find('.image-card:first .image-card__content .global-rating')
      .contains('76%')
  })

  it('should navigate to the band detail page when clicking on a similar artist card', () => {
    cy
      .get('.band-detail-page')
      .find('.menu')
      .contains('Similar Artists')
      .click()

    cy
      .get('.band-detail-page__similar-bands')
      .find('.image-card:first')
      .click()

    cy.url().should('include', '/bands/592f7b964c27e526667533e5/info')

    cy.get('.band-detail-page')
  })
})
