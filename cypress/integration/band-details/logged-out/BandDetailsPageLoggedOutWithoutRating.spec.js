import { setupTests } from '../../../util'

describe('Band Detail Page -> logged out -> no ratings data', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/bands/592f7fa02615c92686b9c5aa/info', data)
    })
  })

  it('should render globalRating when band has not been rated yet', () => {
    cy.get('.band-detail-page')
      .find('.band-detail-page__header__action-buttons')
      .find('.global-rating')
      .contains('Not rated')
  })

  it('should render albums cards globalRating for albums that have not been rated yet', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.image-card:first')
      .contains('Not rated')
  })

  it('should render albums list items globalRating for albums that have not been rated yet', () => {
    cy.get('.band-detail-page')
      .find('.menu')
      .contains('Discography')
      .click()

    cy.get('.band-detail-page')
      .find('.band-detail-page__discography')
      .find('.menu')
      .contains('Live')
      .click()

    cy.get('.band-detail-page__discography')
      .find('.albums-list__item:first')
      .contains('Not rated')
  })
})
