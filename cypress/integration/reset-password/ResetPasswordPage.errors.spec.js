import { setupTests } from '../../util';

describe('Reset Password Page Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/reset-password', data, { withErrors: true });
      });
  });

  it('should handle forgot password errors', () => {
    cy.get('.reset-password-page')
      .find('.form .field input[placeholder="Your new password"]')
      .type('valid-password');

    cy.get('.reset-password-page')
      .find('button[type="submit"]')
      .contains('Reset Password')
      .click();

    cy.get('.reset-password-page')
      .find('.error-message-container');
  });

});
