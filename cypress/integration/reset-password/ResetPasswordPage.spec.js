import { setupTests } from '../../util';

describe('Reset Password Page', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/reset-password', data);
      });
  });

  it('should render properly', () => {
    cy.get('.reset-password-page')
      .find('h2')
      .contains('Enter your new password');

    cy.get('.reset-password-page')
      .find('.form .field input[placeholder="Your new password"]');

    cy.get('.reset-password-page')
      .find('button[type="submit"]')
      .contains('Reset Password');
  });

  it('should show proper errors if submitting without required fields filled in', () => {
    cy.get('.reset-password-page')
      .find('button[type="submit"]')
      .contains('Reset Password')
      .click();

    cy.get('.reset-password-page')
      .find('.error')
      .contains('Your password is required');
  });

  it('should show proper errors if focusing on inputs without typing any data', () => {
    cy.get('.reset-password-page')
      .find('.form .field input[placeholder="Your new password"]')
      .click();

    cy.get('.reset-password-page')
      .find('h2')
      .click();

    cy.get('.reset-password-page')
      .find('.error')
      .contains('Your password is required');
  });

  it('should show proper errors if invalid data has been set on inputs', () => {
    cy.get('.reset-password-page')
      .find('.form .field input[placeholder="Your new password"]')
      .type('short');

    cy.get('.reset-password-page')
      .find('h2')
      .click();

    cy.get('.reset-password-page')
      .find('.error')
      .contains('Password must have at least 6 characters');
  });

  it('should go to HomePage after clicking on Reset Password with valid data', () => {
    cy.get('.reset-password-page')
      .find('.form .field input[placeholder="Your new password"]')
      .type('valid-password');

    cy.get('.reset-password-page')
      .find('button[type="submit"]')
      .contains('Reset Password')
      .click();

    cy.url()
      .should('include', '/');

    cy.get('.home-page');

    cy.get('.header-menu__user')
      .contains('Juanca Morales');
  });

});
