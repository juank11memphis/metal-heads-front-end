import { setupTests } from '../../util';

describe('SignIn Page Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/signin', data, { withErrors: true });
      });
  });

  it('should handle sign in errors', () => {
    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('valid@email.com');

    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your password"]')
      .type('valid password');

    cy.get('.signin-page')
      .find('button[type="submit"]')
      .contains('Sign In')
      .click();

    cy.get('.signin-page')
      .find('.error-message-container');
  });

});
