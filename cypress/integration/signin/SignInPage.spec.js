import { setupTests } from '../../util'

describe('Sign In Page', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/signin', data)
    })
  })

  it('should render properly', () => {
    cy.get('.signin-page')
      .find('h2')
      .contains('Sign In')

    cy.get('.signin-page')
      .find('.social-accounts-buttons .social-accounts-buttons__google-button')
      .contains('Sign in with Google')

    cy.get('.signin-page')
      .find('.social-accounts-buttons__spotify-button')
      .contains('Sign in with Spotify')

    cy.get('.signin-page')
      .find('.signin-page__divider')
      .contains('Or use your email')

    cy.get('.signin-page').find(
      '.form .field input[placeholder="Your email address"]',
    )

    cy.get('.signin-page').find(
      '.form .field input[placeholder="Your password"]',
    )

    cy.get('.signin-page')
      .find('button[type="submit"]')
      .contains('Sign In')

    cy.get('.signin-page')
      .find('.signin-page__to-forgot-password')
      .contains('I have forgotten my password')

    cy.get('.signin-page')
      .find('.signin-page__to-signup')
      .contains('New to MetalHeads?')

    cy.get('.signin-page')
      .find('.signin-page__to-signup a')
      .should('have.attr', 'href', '/signup')
      .contains('Sign up')
  })

  it('should go to the ForgotPasswordPage after clicking the I have forgotten my password link', () => {
    cy.get('.signin-page')
      .find('.signin-page__to-forgot-password')
      .contains('I have forgotten my password')
      .click()

    cy.url().should('include', 'forgot-password')
  })

  it('should go to the SignUpPage after clicking the Sign Up link', () => {
    cy.get('.signin-page')
      .find('.signin-page__to-signup a')
      .contains('Sign up')
      .click()

    cy.url().should('include', 'signup')
  })

  it('should show proper errors if clicking sign in without required fields filled in', () => {
    cy.get('.signin-page')
      .find('button[type="submit"]')
      .contains('Sign In')
      .click()

    cy.get('.signin-page')
      .find('.error')
      .contains('Your email is required')

    cy.get('.signin-page')
      .find('.error')
      .contains('Your password is required')
  })

  it('should show proper errors if focusing on inputs without typing any data', () => {
    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your email address"]')
      .click()

    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your password"]')
      .click()

    cy.get('.signin-page').click()

    cy.get('.signin-page')
      .find('.error')
      .contains('Your email is required')

    cy.get('.signin-page')
      .find('.error')
      .contains('Your password is required')
  })

  it('should show proper errors if invalid data has been set on inputs', () => {
    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('invalid email')

    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your password"]')
      .type('short')

    cy.get('.signin-page').click()

    cy.get('.signin-page')
      .find('.error')
      .contains('Invalid email')

    cy.get('.signin-page')
      .find('.error')
      .contains('Password must have at least 6 characters')
  })

  it('should reveal password', () => {
    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your password"]')
      .should('have.attr', 'type', 'password')
      .type('a valid password')

    cy.get('.signin-page')
      .find('.password button')
      .click()

    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your password"]')
      .should('have.attr', 'type', 'text')
      .should('have.attr', 'value', 'a valid password')
  })

  it('should go to HomePage after a valid sign-in', () => {
    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('valid@email.com')

    cy.get('.signin-page')
      .find('.form .field input[placeholder="Your password"]')
      .type('valid password')

    cy.get('.signin-page')
      .find('button[type="submit"]')
      .contains('Sign In')
      .click()

    cy.url().should('include', '/')

    cy.get('.home-page')

    cy.get('.header-menu__user').contains('Juanca Morales')
  })
})
