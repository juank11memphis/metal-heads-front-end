import { setupTests } from '../../util'

describe('Home Page Errors', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/', data, { withErrors: true })
    })
  })

  it('should handle errors on quotes', () => {
    cy
      .get('.home-page')
      .find('.home-page__header__quotes')
      .should('not.be.visible')
  })

  it('should handle errors on most rated bands', () => {
    cy.get('.home-page__most-rated-bands').find('.error-message-container')
  })

  it('should handle errors on get latest band ratings', () => {
    cy.get('.latest-band-ratings').find('.error-message-container')
  })

  it('should handle errors on get latest albums ratings', () => {
    cy.get('.latest-album-ratings').find('.error-message-container')
  })

  it('should handle errors on news', () => {
    cy
      .get('.home-page__news-section')
      .find('[data-source=Loudwire]')
      .find('.error-message-container')

    cy
      .get('.home-page__news-section')
      .find('[data-source=Blabbermouth]')
      .find('.error-message-container')
  })
})
