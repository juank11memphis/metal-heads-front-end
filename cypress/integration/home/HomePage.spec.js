/* eslint-disable */
/// <reference types="cypress" />

import { setupTests } from '../../util'

describe('Home Page', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/', data)
    })
  })

  it('should render the banner', () => {
    cy.get('.home-page__header')

    cy.get('.home-page__header__slogan')
      .find('h2')
      .contains('Share your love for metal with the world')
  })

  it('should render the most rated bands', () => {
    cy.get('.home-page__most-rated-bands')
      .find('.slick-list:visible')
      .find('.home-page__most-rated-bands__slider__slide')
      .should('have.length', 22)

    cy.get('.home-page__most-rated-bands')
      .find('[data-index=0] a')
      .should('have.attr', 'href', '/bands/592f7fa02615c92686b9c5aa/info')
      .contains('1. Iron Maiden')

    cy.get('.home-page__most-rated-bands')
      .find('[data-index=0]')
      .contains('100%')

    cy.get('.home-page__most-rated-bands')
      .find('[data-index=0]')
      .contains('User Ratings: 4')
  })

  it('should go to the band detail page when clicking on a slider item', () => {
    cy.get('.home-page__most-rated-bands')
      .find('[data-index=0]')
      .contains('Iron Maiden')
      .click()

    cy.url().should('include', '/bands/592f7fa02615c92686b9c5aa/info')
  })

  it('should render the news section', () => {
    cy.get('.home-page__news-section')
      .find('.home-page__news-section__news-list')
      .should('have.length', 2)
  })

  it('should render loudwire news', () => {
    cy.get('.home-page__news-section')
      .find('[data-source=Loudwire]')
      .should('contain', 'Latest news from Loudwire')

    cy.get('.home-page__news-section')
      .find('[data-source=Loudwire]')
      .find('h4 a')
      .should('have.attr', 'href', 'http://loudwire.com/')

    cy.get('.home-page__news-section')
      .find('[data-source=Loudwire]')
      .find('.news-item')
      .should('have.length', 10)

    cy.get('.home-page__news-section')
      .find('[data-source=Loudwire]')
      .find('.news-item:first')
      .find('h5 a')
      .should(
        'have.attr',
        'href',
        'http://loudwire.com/corpsegrinder-fisher-cannibal-corpse-interview/',
      )
  })

  it('should render blabbermouth news', () => {
    cy.get('.home-page__news-section')
      .find('[data-source=Blabbermouth]')
      .should('contain', 'Latest news from Blabbermouth')

    cy.get('.home-page__news-section')
      .find('[data-source=Blabbermouth]')
      .find('h4 a')
      .should('have.attr', 'href', 'http://www.blabbermouth.net/')

    cy.get('.home-page__news-section')
      .find('[data-source=Blabbermouth]')
      .find('.news-item')
      .should('have.length', 10)

    cy.get('.home-page__news-section')
      .find('[data-source=Blabbermouth]')
      .find('.news-item:last')
      .find('h5 a')
      .should(
        'have.attr',
        'href',
        'http://loudwire.com/fieldy-new-bass-track-kataklysm-2018-album-more/',
      )
  })
})
