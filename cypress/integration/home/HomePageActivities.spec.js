/* eslint-disable */
/// <reference types="cypress" />

import { setupTests } from '../../util'

describe('Home Page Activities', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/', data, { loggedIn: true })
    })
  })

  it('should render the latest band ratings', () => {
    cy.get('h3').contains('Latest Band Ratings')
    cy.get('.latest-band-ratings .activity-item').should('have.length', 10)
    cy.get('.latest-band-ratings .activity-item:first').contains(
      '@Juanca rated',
    )
    cy.get('.latest-band-ratings .activity-item:first')
      .find('h3')
      .contains('Forbidden')
    cy.get('.latest-band-ratings .activity-item:first').contains(
      'with a 7.5 out of 10',
    )
    cy.get('.latest-band-ratings .activity-item:first')
      .find('.image-card .image-card__image')
      .should('have.attr', 'href', '/bands/592f7eb776c39f26827eb240/info')

    cy.get('.latest-band-ratings .activity-item:first')
      .find('.activity-item__ratings')
      .contains('Global Rating')

    cy.get('.latest-band-ratings .activity-item:first')
      .find('.activity-item__ratings')
      .contains('75%')

    cy.get('.latest-band-ratings .activity-item:first')
      .find('.activity-item__ratings')
      .contains('User Ratings')

    cy.get('.latest-band-ratings .activity-item:first')
      .find('.activity-item__ratings')
      .contains('1')
  })

  it('should navigate to the band details page when clicking on the name of a band', () => {
    cy.get('.latest-band-ratings .activity-item:first')
      .find('h3')
      .contains('Forbidden')
      .click()

    cy.url().should('include', '/bands/592f7eb776c39f26827eb240/info')

    cy.get('.band-detail-page')
  })

  it('should render the latest albums ratings', () => {
    cy.get('h3').contains('Latest Albums Ratings')
    cy.get('.latest-album-ratings .activity-item').should('have.length', 3)
    cy.get('.latest-album-ratings .activity-item:first').contains(
      '@Juanca rated album',
    )
    cy.get('.latest-album-ratings .activity-item:first')
      .find('h3')
      .contains('By Inheritance')
    cy.get('.latest-album-ratings .activity-item:first').contains(
      'with a 5.5 out of 10',
    )
    cy.get('.latest-album-ratings .activity-item:first')
      .find('.image-card .image-card__image')
      .should('have.attr', 'href', '/albums/5a595fd01740971c1637066c')

    cy.get('.latest-album-ratings .activity-item:first')
      .find('.activity-item__ratings')
      .contains('Global Rating')

    cy.get('.latest-album-ratings .activity-item:first')
      .find('.activity-item__ratings')
      .contains('55%')

    cy.get('.latest-album-ratings .activity-item:first')
      .find('.activity-item__ratings')
      .contains('User Ratings')

    cy.get('.latest-album-ratings .activity-item:first')
      .find('.activity-item__ratings')
      .contains('1')
  })

  it('should navigate to the band details page when clicking on the name of a band', () => {
    cy.get('.latest-album-ratings .activity-item:first')
      .find('h3')
      .contains('By Inheritance')
      .click()

    cy.url().should('include', '/albums/5a595fd01740971c1637066c')

    cy.get('.album-detail-page')
  })

  it('should navigate to the user profile page when cliking on the user name', () => {
    cy.get('.latest-band-ratings .activity-item:first')
      .find('h3')
      .contains('@Juanca')
      .click()

    cy.url().should('include', '/user/profile/59a9a2e84f58ec92db26b60a/rated')

    cy.get('.profile-page')
      .find('h1')
      .contains('Sandra Aguilar')
      .click()
  })
})
