import { setupTests } from '../../util'

describe('Footer', () => {
  beforeEach(() => {
    setupTests().then(data => {
      cy.visitStubbed('/', data)
    })
  })

  it('should render properly', () => {
    cy
      .get('.footer')
      .find('h5')
      .contains('2018 © MetalHeads')

    cy
      .get('.footer')
      .find('h5')
      .contains('Powered by')

    cy
      .get('.footer')
      .find('h5 a')
      .contains('The Metal Archives')
      .should('have.attr', 'href', 'https://www.metal-archives.com/')

    cy
      .get('.footer')
      .find('h5 a')
      .contains('The Metal Archives API')
      .should('have.attr', 'href', 'http://em.wemakesites.net/')
  })
})
