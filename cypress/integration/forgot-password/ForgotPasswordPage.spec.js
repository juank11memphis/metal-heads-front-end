import { setupTests } from '../../util';

describe('Forgot Password Page', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/forgot-password', data);
      });
  });

  it('should render properly', () => {
    cy.get('.forgot-password-page')
      .find('h2')
      .contains('Enter your email');

    cy.get('.forgot-password-page')
      .find('.form .field input[placeholder="Your email address"]');

    cy.get('.forgot-password-page')
      .find('button[type="submit"]')
      .contains('Request Password Reset');
  });

  it('should show proper errors if submitting without required fields filled in', () => {
    cy.get('.forgot-password-page')
      .find('button[type="submit"]')
      .contains('Request Password Reset')
      .click();

    cy.get('.forgot-password-page')
      .find('.error')
      .contains('Your email is required');
  });

  it('should show proper errors if focusing on inputs without typing any data', () => {
    cy.get('.forgot-password-page')
      .find('.form .field input[placeholder="Your email address"]')
      .click();

    cy.get('.forgot-password-page')
      .find('h2')
      .click();

    cy.get('.forgot-password-page')
      .find('.error')
      .contains('Your email is required');
  });

  it('should show proper errors if invalid data has been set on inputs', () => {
    cy.get('.forgot-password-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('invalid email');

    cy.get('.forgot-password-page')
      .find('h2')
      .click();

    cy.get('.forgot-password-page')
      .find('.error')
      .contains('Invalid email');
  });

  it('should show success message after clicking on Request Password Reset with valid data', () => {
    cy.get('.forgot-password-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('valid@email.com');

    cy.get('.forgot-password-page')
      .find('button[type="submit"]')
      .contains('Request Password Reset')
      .click();

    cy.get('.forgot-password-page')
      .find('.success-message-container')
      .contains('Check your email for further instructions');
  });

});
