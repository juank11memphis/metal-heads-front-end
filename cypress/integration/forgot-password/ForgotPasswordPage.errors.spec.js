import { setupTests } from '../../util';

describe('Forgot Password Page Errors', () => {

  beforeEach(() => {
    setupTests()
      .then((data) => {
        cy.visitStubbed('/forgot-password', data, { withErrors: true });
      });
  });

  it('should handle forgot password errors', () => {
    cy.get('.forgot-password-page')
      .find('.form .field input[placeholder="Your email address"]')
      .type('valid@email.com');

    cy.get('.forgot-password-page')
      .find('button[type="submit"]')
      .contains('Request Password Reset')
      .click();

    cy.get('.forgot-password-page')
      .find('.error-message-container');
  });

});
