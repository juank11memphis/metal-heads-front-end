import { getSearchBandsFixture } from './fixtures/search'
import { getLoadTokenUserFixture } from './fixtures/load-token-user'
import {
  getBandDetailsFixture,
  getBandSimilarArtists,
} from './fixtures/load-band-details'
import { getMostRatedBandsFixture } from './fixtures/load-most-rated-bands'
import { getNewsBySourceFixture } from './fixtures/news'
import { getSignInFixture } from './fixtures/signin'
import { getSignUpFixture } from './fixtures/signup'
import { getForgotPasswordFixture } from './fixtures/forgot-password'
import { getResetPasswordFixture } from './fixtures/reset-password'
import { getArtistDetailsFixture } from './fixtures/artist-details'
import { getAlbumDetailsFixture } from './fixtures/load-album-details'
import { getLoadAlbumUserDataFixture } from './fixtures/load-album-user-data'
import {
  getAccountDetailsFixture,
  getChangePasswordFixture,
  getSpotifyDisconnectFixture,
} from './fixtures/account'
import { getActiveQuoteFixture, getCreateQuoteFixture } from './fixtures/quotes'
import { getLoadSavedBandsFixture } from './fixtures/load-saved-bands'
import { getRemoveFromSavedBandsFixture } from './fixtures/remove-from-saved-bands'
import { getSaveBandForLaterFixture } from './fixtures/save-band-for-later'
import { getRateBandDataFixture } from './fixtures/rate-band'
import { getLoadRatedBandsFixture } from './fixtures/load-rated-bands'
import { getRateAlbumDataFixture } from './fixtures/rate-album'
import { getLoadSpotifyDevicesFixture } from './fixtures/load-spotify-devices'
import { getPlayAlbumFixture } from './fixtures/play-album'
import { getRemoveBandRatingFixture } from './fixtures/remove-band-rating'
import { getRemoveAlbumRatingFixture } from './fixtures/remove-album-rating'
import { getLatestBandRatingsFixture } from './fixtures/get-latest-band-ratings'
import { getLatestAlbumsRatingsFixture } from './fixtures/get-latest-album-ratings'
import { getLoadUserFixture } from './fixtures/load-user'
import { getLoadUserBandDataFixture } from './fixtures/load-user-band-data'

export const ALL_FIXTURES = [
  { name: 'loadConfig' },
  getSearchBandsFixture(),
  getLoadTokenUserFixture(),
  getBandDetailsFixture(),
  getBandSimilarArtists(),
  getMostRatedBandsFixture(),
  getNewsBySourceFixture(),
  getSignInFixture(),
  getSignUpFixture(),
  getForgotPasswordFixture(),
  getResetPasswordFixture(),
  getArtistDetailsFixture(),
  getAlbumDetailsFixture(),
  getLoadAlbumUserDataFixture(),
  getAccountDetailsFixture(),
  getChangePasswordFixture(),
  getSpotifyDisconnectFixture(),
  getActiveQuoteFixture(),
  getCreateQuoteFixture(),
  getLoadSavedBandsFixture(),
  getRemoveFromSavedBandsFixture(),
  getSaveBandForLaterFixture(),
  getRateBandDataFixture(),
  getLoadRatedBandsFixture(),
  getRateAlbumDataFixture(),
  getLoadSpotifyDevicesFixture(),
  getPlayAlbumFixture(),
  getRemoveBandRatingFixture(),
  getRemoveAlbumRatingFixture(),
  getLatestBandRatingsFixture(),
  getLatestAlbumsRatingsFixture(),
  getLoadUserFixture(),
  getLoadUserBandDataFixture(),
]

const loadSingleFileFixture = fixtureItem => {
  const promise = resolve => {
    const fileUrl = fixtureItem.file ? fixtureItem.file : fixtureItem.name
    cy.fixture(fileUrl).then(data => {
      const finalData = Object.assign({}, data, { fixtureItem })
      resolve({ [fixtureItem.name]: finalData })
    })
  }
  return new Promise(promise)
}

const loadMultiFileFixture = fixtureItem => {
  const promise = resolve => {
    let filesProcessedCount = 0
    const response = {}
    fixtureItem.files.forEach(file => {
      loadSingleFileFixture({ file, name: fixtureItem.name }).then(
        fixtureData => {
          const fileName = file.substring(
            file.lastIndexOf('/') + 1,
            file.length,
          )
          response[fileName] = fixtureData[fixtureItem.name]
          response.fixtureItem = fixtureItem
          filesProcessedCount += 1
          if (filesProcessedCount === fixtureItem.files.length) {
            const finalData = { [fixtureItem.name]: response }
            resolve(finalData)
          }
        },
      )
    })
  }
  return new Promise(promise)
}

const loadFixture = fixtureItem => {
  if (fixtureItem.files) {
    return loadMultiFileFixture(fixtureItem)
  }
  return loadSingleFileFixture(fixtureItem)
}

export const setupTests = (fixturesItems = ALL_FIXTURES) => {
  if (!fixturesItems || fixturesItems.length === 0) {
    return Promise.resolve({})
  }
  const promise = resolve => {
    const response = {}
    let fixturesProcessedCount = 0

    fixturesItems.forEach(fixtureItem => {
      loadFixture(fixtureItem).then(fixtureData => {
        Object.assign(response, fixtureData)
        fixturesProcessedCount += 1
        if (fixturesProcessedCount === fixturesItems.length) {
          resolve(response)
        }
      })
    })
  }
  return new Promise(promise)
}
