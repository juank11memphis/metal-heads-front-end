Cypress.Commands.add('visitStubbed', (url, fixtures = {}, context = {}) => {
  function getResponseStub(result) {
    return {
      text() {
        return Promise.resolve(JSON.stringify(result))
      },
      ok: true,
    }
  }

  function serverStub(_, req) {
    const { operationName, variables: graphReqVars } = JSON.parse(req.body)
    console.log(`Handling request stub for ${operationName}`) // eslint-disable-line no-console

    const operationFixturesData = fixtures[operationName]
    const { fixtureItem } = operationFixturesData
    if (fixtureItem.resolve) {
      return Promise.resolve(
        getResponseStub(
          fixtureItem.resolve(operationFixturesData, graphReqVars, context),
        ),
      )
    }
    if (operationFixturesData) {
      return Promise.resolve(getResponseStub(operationFixturesData))
    }
    return Promise.resolve({})
  }

  cy.visit(url, {
    onBeforeLoad: win => {
      cy.stub(win, 'fetch')
        .withArgs('/graphql')
        .callsFake(serverStub)
    },
  })
})
