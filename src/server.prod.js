/* eslint-disable */

const express = require('express')
var proxy = require('http-proxy-middleware')

const server = express()

server.use(
  '/api',
  proxy({
    target: 'http://localhost:3001',
    changeOrigin: true,
  }),
)

server.use(
  '/graphql',
  proxy({
    target: 'http://localhost:3001',
    changeOrigin: true,
  }),
)

const expressStaticGzip = require('express-static-gzip')

server.use(
  expressStaticGzip('dist', {
    enableBrotli: true,
  }),
)

const PORT = 3000
server.listen(PORT, () => {
  console.log(`Server listening on http://localhost:${PORT}`)
})
