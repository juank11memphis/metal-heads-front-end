import React from 'react'
import PropTypes from 'prop-types'

const GlobalRating = ({
  value,
  showText,
  showUserRatings,
  userRatingsValue,
}) => (
  <div className="global-rating">
    {showText ? <span>Global rating</span> : null}
    <h1 className="no-margin">{value ? `${value}%` : 'Not rated'}</h1>
    {value &&
      showUserRatings && (
        <div className="user-ratings">
          <span className="bold-text">User Ratings:</span> {userRatingsValue}
        </div>
      )}
  </div>
)

GlobalRating.defaultProps = {
  value: null,
  showText: true,
  showUserRatings: true,
  userRatingsValue: null,
}

GlobalRating.propTypes = {
  value: PropTypes.number,
  userRatingsValue: PropTypes.number,
  showText: PropTypes.bool,
  showUserRatings: PropTypes.bool,
}

export default GlobalRating
