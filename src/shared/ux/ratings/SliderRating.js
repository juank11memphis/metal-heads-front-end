import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Tooltip from 'rc-tooltip'
import Slider from 'rc-slider'
import { Icon } from 'semantic-ui-react'

import { CurrentUserData } from '../../users'

const { Handle } = Slider

const handle = props => {
  const { value, dragging, index, ...restProps } = props
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={value}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  )
}

class SliderRating extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      value: null,
      isMovingHandle: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    const { value: currentValue } = this.props
    const { value: nextValue } = nextProps
    if (nextValue !== currentValue) {
      this.setState({ value: nextValue })
    }
  }

  onMovingSlide = newValue => {
    this.setState({
      value: newValue,
      isMovingHandle: true,
    })
  }

  onAfterChange = newValue => {
    this.setState({ isMovingHandle: false })
    const { onChange } = this.props
    if (onChange) {
      onChange(newValue)
    }
  }

  getRemoveRatingButton(value) {
    const { editable } = this.props
    const { isMovingHandle } = this.state
    const { onRemove } = this.props
    if (value <= 0 || isMovingHandle || !editable) {
      return null
    }
    return (
      <div className="slider-rating__remove" data-tooltip="Remove rating">
        <Icon name="remove circle" onClick={() => onRemove && onRemove()} />
      </div>
    )
  }

  getText(value) {
    const { text } = this.props
    if (value <= 0) {
      return text || 'Add Your Rating'
    }
    return (
      text || (
        <div>
          Your Rating{' '}
          <span className="slider-rating__text__user-rating">{value}</span>
        </div>
      )
    )
  }

  renderSlider() {
    const { value: stateValue } = this.state
    const { value: propsValue, editable } = this.props
    const finalValue = !stateValue ? propsValue : stateValue
    const finalText = this.getText(finalValue)
    return (
      <div className="slider-rating">
        <div className="slider-rating__text">{finalText}</div>
        {finalValue !== undefined ? (
          <div className="hbox">
            {this.getRemoveRatingButton(finalValue)}
            <Slider
              min={0}
              max={10}
              step={0.5}
              value={finalValue}
              handle={handle}
              onChange={this.onMovingSlide}
              onAfterChange={this.onAfterChange}
              disabled={!editable}
            />
          </div>
        ) : null}
      </div>
    )
  }

  render() {
    return (
      <CurrentUserData>
        {({ isAuthenticated }) =>
          isAuthenticated ? this.renderSlider() : null
        }
      </CurrentUserData>
    )
  }
}

SliderRating.defaultProps = {
  onChange: null,
  onRemove: null,
  editable: true,
  text: null,
  value: null,
}

SliderRating.propTypes = {
  value: PropTypes.number,
  onChange: PropTypes.func,
  onRemove: PropTypes.func,
  editable: PropTypes.bool,
  text: PropTypes.string,
}

export default SliderRating
