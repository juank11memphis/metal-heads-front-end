import React from 'react'
import ReactStars from 'react-stars'
import PropTypes from 'prop-types'

const FiveStarsRating = ({ value, onChange, editable, text }) => (
  <div className="five-stars-rating">
    <span>{value > 0 ? text || 'Your Rating' : text || 'Add Your Rating'}</span>
    <ReactStars
      color1="ffffff"
      size={28}
      value={value}
      onChange={newValue => onChange && onChange(newValue)}
      edit={editable}
    />
  </div>
)

FiveStarsRating.defaultProps = {
  onChange: null,
  editable: true,
  text: null,
}

FiveStarsRating.propTypes = {
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func,
  editable: PropTypes.bool,
  text: PropTypes.string,
}

export default FiveStarsRating
