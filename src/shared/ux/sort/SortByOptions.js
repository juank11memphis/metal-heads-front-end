import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { CurrentUserData } from '../../users'
import { SelectInput } from '../form'

class SortByOptions extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = { selectedOption: '' }
  }

  getOptionsList(isAuthenticated) {
    const { showYearOption } = this.props
    let options = [
      { value: 'name', label: 'Name' },
      { value: 'globalRating', label: 'Global Rating' },
    ]
    if (showYearOption) {
      options = [{ value: 'year', label: 'Year' }, ...options]
    }
    if (isAuthenticated) {
      options = [...options, { value: 'userRating', label: 'User Rating' }]
    }
    return options
  }

  handleChange = selectedOption => {
    const { onChange } = this.props
    this.setState({ selectedOption })
    if (onChange) {
      onChange(selectedOption.value)
    }
  }

  clearValue() {
    this.setState({ selectedOption: '' })
  }

  render() {
    const { selectedOption } = this.state
    return (
      <CurrentUserData>
        {({ isAuthenticated }) => (
          <SelectInput
            className="sort-by-options"
            value={selectedOption}
            options={this.getOptionsList(isAuthenticated)}
            placeholder="Sort By"
            isClearable={false}
            onChange={this.handleChange}
          />
        )}
      </CurrentUserData>
    )
  }
}

SortByOptions.defaultProps = {
  onChange: null,
  showYearOption: true,
}

SortByOptions.propTypes = {
  onChange: PropTypes.func,
  showYearOption: PropTypes.bool,
}

export default SortByOptions
