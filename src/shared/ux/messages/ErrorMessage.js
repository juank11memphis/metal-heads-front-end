import React from 'react'
import PropTypes from 'prop-types'
import { Label } from 'semantic-ui-react'

import { GraphQLUtil } from '../../util'

const ErrorMessage = ({ error, message }) => {
  const renderSpacers = () => (
    <div>
      <br />
      <br />
    </div>
  )
  let finalError = error
  if (error && error.message && error.message.includes('Graph')) {
    finalError = GraphQLUtil.graphErrorToError(error)
  }
  return (
    <div className="error-message-container">
      <Label color="red">
        {message && message}
        {message && finalError && renderSpacers()}
        {finalError && finalError.message}
      </Label>
    </div>
  )
}

ErrorMessage.defaultProps = {
  error: null,
  message: null,
}

ErrorMessage.propTypes = {
  error: PropTypes.object,
  message: PropTypes.string,
}

export default ErrorMessage
