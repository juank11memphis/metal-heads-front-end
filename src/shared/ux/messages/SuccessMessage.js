import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Label } from 'semantic-ui-react'

class SuccessMessage extends PureComponent {
  state = {
    hideMessage: false,
  }

  componentWillReceiveProps(nextProps) {
    const { message } = nextProps
    if (message) {
      this.setState({ hideMessage: false })
    }
  }

  render() {
    const { message, autoHide, autoHideTime, size, onHide } = this.props
    const { hideMessage } = this.state
    if (hideMessage || !message) {
      return null
    }
    if (autoHide) {
      setTimeout(() => {
        this.setState({ hideMessage: true })
        if (onHide) {
          onHide()
        }
      }, autoHideTime)
    }
    return (
      <div className="success-message-container">
        <Label color="blue" size={size}>
          {message && message}
        </Label>
      </div>
    )
  }
}

SuccessMessage.defaultProps = {
  message: null,
  autoHide: true,
  autoHideTime: 3000,
  size: 'large',
  onHide: null,
}

SuccessMessage.propTypes = {
  message: PropTypes.string,
  autoHide: PropTypes.bool,
  autoHideTime: PropTypes.number,
  size: PropTypes.string,
  onHide: PropTypes.func,
}

export default SuccessMessage
