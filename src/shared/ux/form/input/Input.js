import React from 'react'
import PropTypes from 'prop-types'

const Input = ({ field, type, placeholder, disabled }) => (
  <input type={type} placeholder={placeholder} disabled={disabled} {...field} />
)

Input.defaultProps = {
  type: 'text',
  placeholder: '',
  disabled: false,
}

Input.propTypes = {
  field: PropTypes.object.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
}

export default Input
