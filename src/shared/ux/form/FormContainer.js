import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Formik, Form } from 'formik'

import { SuccessMessage, ErrorMessage } from '../messages'

class FormContainer extends PureComponent {
  state = {
    error: null,
    successMessage: null,
  }

  submitForm = async (data, formikBag) => {
    try {
      const {
        successMessage,
        submitAction,
        resetFormOnSubmit,
        initialValues,
      } = this.props
      await submitAction(data)
      if (successMessage) {
        this.setState({ error: null, successMessage })
      }
      if (resetFormOnSubmit) {
        formikBag.resetForm(initialValues)
      }
    } catch (error) {
      this.setState({ error, successMessage: null })
    }
  }

  render() {
    const {
      children,
      hideFormOnSuccess,
      successMessageSize,
      autoHideSuccessMessage,
      className,
      initialValues,
      validations,
    } = this.props
    const { error, successMessage } = this.state
    return (
      <div className={className}>
        <Formik
          initialValues={initialValues}
          validationSchema={validations}
          onSubmit={this.submitForm}
          render={() => (
            <Form className="ui form">
              {successMessage && hideFormOnSuccess ? null : children}
            </Form>
          )}
        />
        {error && <ErrorMessage error={error} />}
        {successMessage && (
          <SuccessMessage
            message={successMessage}
            size={successMessageSize}
            autoHide={autoHideSuccessMessage}
            onHide={() => this.setState({ successMessage: null })}
          />
        )}
      </div>
    )
  }
}

FormContainer.defaultProps = {
  successMessage: null,
  hideFormOnSuccess: false,
  successMessageSize: 'large',
  autoHideSuccessMessage: true,
  className: null,
  initialValues: null,
  validations: null,
  resetFormOnSubmit: false,
}

FormContainer.propTypes = {
  children: PropTypes.array.isRequired,
  submitAction: PropTypes.func.isRequired,
  successMessage: PropTypes.string,
  hideFormOnSuccess: PropTypes.bool,
  successMessageSize: PropTypes.string,
  autoHideSuccessMessage: PropTypes.bool,
  className: PropTypes.string,
  initialValues: PropTypes.object,
  validations: PropTypes.any,
  resetFormOnSubmit: PropTypes.bool,
}

export default FormContainer
