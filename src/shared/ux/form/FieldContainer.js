import * as React from 'react'
import classNames from 'classnames'
import { Field } from 'formik'
import PropTypes from 'prop-types'

const renderLabel = (htmlFor, label) => <label htmlFor={htmlFor}>{label}</label>

const renderError = error => <div className="error-container">{error}</div>

const FieldContainer = ({ name, render, label, htmlFor, ...rest }) => (
  <Field
    name={name}
    render={({ field, form }) => (
      <div
        className={classNames('field', {
          error: form.errors[name] && form.touched[name],
        })}
      >
        {label && renderLabel(htmlFor, label)}
        {render({ field, form })}
        {form.touched[name] &&
          form.errors[name] &&
          renderError(form.errors[name])}
      </div>
    )}
    {...rest}
  />
)

FieldContainer.defaultProps = {
  label: null,
  htmlFor: null,
}

FieldContainer.propTypes = {
  name: PropTypes.string.isRequired,
  render: PropTypes.func.isRequired,
  label: PropTypes.string,
  htmlFor: PropTypes.string,
}

export default FieldContainer
