import React from 'react'
import PropTypes from 'prop-types'

const DisplayLabel = props => {
  const { label, value, labelWidth } = props
  return (
    <div className="display-label-container">
      <h4
        className="display-label-container__label"
        style={{ width: `${labelWidth}px` }}
      >
        {label}
      </h4>
      <h4 className="display-label-container__value">{value}</h4>
    </div>
  )
}

DisplayLabel.defaultProps = {
  labelWidth: 120,
}

DisplayLabel.propTypes = {
  label: PropTypes.any.isRequired,
  labelWidth: PropTypes.number,
  value: PropTypes.any.isRequired,
}

export default DisplayLabel
