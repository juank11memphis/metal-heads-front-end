import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

class PasswordInput extends PureComponent {
  constructor(props, context) {
    super(props, context)
    this.state = {
      type: 'password',
    }
    this.showHide = this.showHide.bind(this)
  }

  showHide(event) {
    event.preventDefault()
    event.stopPropagation()
    const { type } = this.state
    this.setState({
      type: type === 'text' ? 'password' : 'text',
    })
  }

  render() {
    const { field, placeholder } = this.props
    const { type } = this.state
    const icon = type === 'text' ? 'hide' : 'unhide'
    return (
      <div className="password">
        <input
          {...field}
          placeholder={placeholder}
          type={type}
          className="password__input"
        />
        <Button
          icon={icon}
          size="large"
          className="password__show"
          onClick={this.showHide}
          tabIndex={-1}
          type="button"
        />
      </div>
    )
  }
}

PasswordInput.defaultProps = {
  placeholder: '',
}

PasswordInput.propTypes = {
  field: PropTypes.object.isRequired,
  placeholder: PropTypes.string,
}

export default PasswordInput
