import React from 'react'
import PropTypes from 'prop-types'
import Select from 'react-select'

const styles = {
  option: styles => ({
    ...styles,
    color: '#000000',
  }),
}

const SelectInput = ({ className, ...rest }) => (
  <Select
    {...rest}
    styles={styles}
    className={`select-input ${className}`}
    classNamePrefix="select"
  />
)

SelectInput.defaultProps = {
  className: null,
}

SelectInput.propTypes = {
  className: PropTypes.string,
}

export default SelectInput
