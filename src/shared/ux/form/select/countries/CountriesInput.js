import React from 'react'
import PropTypes from 'prop-types'

import countryOptions from './countries'
import SelectInput from '../SelectInput'

const CountriesInput = ({ field, placeholder, form }) => (
  <SelectInput
    {...field}
    options={countryOptions}
    placeholder={placeholder}
    isClearable={false}
    onChange={option => form.setFieldValue(field.name, option.value)}
    onBlur={field.onBlur}
    value={
      countryOptions
        ? countryOptions.find(option => option.value === field.value)
        : ''
    }
  />
)

CountriesInput.defaultProps = {
  placeholder: '',
}

CountriesInput.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  placeholder: PropTypes.string,
}

export default CountriesInput
