import React from 'react'
import PropTypes from 'prop-types'
import { Query } from 'react-apollo'

const QueryWrapper = ({
  children,
  query,
  errorRenderer,
  loadingRenderer,
  ...rest
}) => (
  <Query query={query} {...rest}>
    {({ data, loading, error }) => {
      if (error && errorRenderer) {
        return errorRenderer(error)
      }
      if (loading && loadingRenderer) {
        return loadingRenderer()
      }
      return children(data || {})
    }}
  </Query>
)

QueryWrapper.defaultProps = {
  fetchPolicy: null,
  variables: null,
  errorRenderer: null,
  loadingRenderer: null,
}

QueryWrapper.propTypes = {
  children: PropTypes.func.isRequired,
  query: PropTypes.object.isRequired,
  errorRenderer: PropTypes.func,
  loadingRenderer: PropTypes.func,
  fetchPolicy: PropTypes.string,
  variables: PropTypes.object,
}

export default QueryWrapper
