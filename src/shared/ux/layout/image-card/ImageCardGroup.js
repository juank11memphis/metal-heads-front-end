import React from 'react'
import PropTypes from 'prop-types'

const ImageCardGroup = props => {
  const { children, className } = props
  return <div className={`image-card-group ${className}`}>{children}</div>
}

ImageCardGroup.defaultProps = {
  className: '',
}

ImageCardGroup.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
}

export default ImageCardGroup
