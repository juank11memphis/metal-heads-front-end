import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import ImageCardGroup from './ImageCardGroup'

const defaultImage =
  'https://corriente.us/wp-content/uploads/2014/09/no-image-available.jpg'

const getImageComp = ({
  to,
  onClick,
  item,
  imageField,
  onImageOut,
  onImageOver,
}) => {
  const imageCardStyle = {
    backgroundImage: `url(${item[imageField]}), url("${defaultImage}")`,
  }
  return to ? (
    <div className="image-card__image-wrapper">
      <Link
        as="div"
        to={to}
        className="image-card__image"
        onClick={() => onClick && onClick(item)}
        onFocus={event => onImageOver && onImageOver(event)}
        onBlur={event => onImageOut && onImageOut(event)}
        onMouseOver={event => onImageOver && onImageOver(event)}
        onMouseOut={event => onImageOut && onImageOut(event)}
        style={imageCardStyle}
      />
    </div>
  ) : (
    <div className="image-card__image-wrapper">
      <div
        className="image-card__image"
        role="button"
        tabIndex={0}
        onClick={() => onClick && onClick(item)}
        onFocus={event => onImageOver && onImageOver(event)}
        onBlur={event => onImageOut && onImageOut(event)}
        onMouseOver={event => onImageOver && onImageOver(event)}
        onMouseOut={event => onImageOut && onImageOut(event)}
        style={imageCardStyle}
      />
    </div>
  )
}

const ImageCard = props => {
  const { children, contentStyle, cardStyle } = props

  return (
    <div className="image-card" style={cardStyle}>
      {getImageComp(props)}
      <div className="image-card__content" style={contentStyle}>
        {children}
      </div>
    </div>
  )
}

ImageCard.defaultProps = {
  onClick: null,
  to: null,
  cardStyle: null,
  contentStyle: null,
  onImageOver: null,
  onImageOut: null,
  children: null,
}

ImageCard.propTypes = {
  item: PropTypes.object.isRequired,
  imageField: PropTypes.string.isRequired,
  children: PropTypes.object,
  onClick: PropTypes.func,
  to: PropTypes.string,
  cardStyle: PropTypes.object,
  contentStyle: PropTypes.object,
  onImageOver: PropTypes.func,
  onImageOut: PropTypes.func,
}

ImageCard.Group = ImageCardGroup

export default ImageCard
