import React from 'react'
import { Link } from 'react-router-dom'
import { Image } from 'semantic-ui-react'

import logo from '../../../../styles/img/logo.svg'

const HeaderLogo = () => (
  <div role="button" tabIndex={0} className="header-logo hbox-raw">
    <Link to="/">
      <Image className="header-logo__image" src={logo} width={150} />
    </Link>
  </div>
)

export default HeaderLogo
