import React, { Fragment } from 'react'
import { Menu } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import { BurguerDropDown } from '../../burguer-dropdown/index'

const menuOptions = [
  { text: 'Sign Up', value: '/signup' },
  { text: 'Sign In', value: '/signin' },
]

const HeaderMenu = (props, context) => (
  <Fragment>
    <Menu className="header-menu__menu hide-on-mobile" pointing secondary>
      <Menu.Menu position="right">
        {menuOptions.map(menuItem => (
          <Menu.Item as={Link} key={menuItem.text} to={menuItem.value}>
            {menuItem.text}
          </Menu.Item>
        ))}
      </Menu.Menu>
    </Menu>
    <BurguerDropDown
      className="show-on-mobile"
      options={menuOptions}
      onItemClick={menuItem => context.router.history.push(menuItem.value)}
    />
  </Fragment>
)

HeaderMenu.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default HeaderMenu
