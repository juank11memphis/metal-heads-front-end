import React from 'react'
import { Menu, Dropdown } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { get } from 'lodash'

import { ADMIN_ROLES } from '../../../constants'
import { AuthUtil } from '../../../util'

const options = [
  { text: 'Profile', value: 'profile', path: '/user/profile/:id/rated' },
  { text: 'Account', value: 'account', path: '/user/account/details' },
  { text: 'Admin', value: 'admin', roles: ADMIN_ROLES, path: '/admin' },
  { text: 'Sign Out', value: 'signout' },
]

const getFullName = user => {
  if (!user) {
    return ''
  }
  if (!user.lastname) {
    return user.firstname
  }
  return `${user.firstname} ${user.lastname}`
}

const onChange = (data, user, context) => {
  switch (data.value) {
    case 'profile':
      // eslint-disable-next-line no-case-declarations
      const path = data.path.replace(':id', user.id)
      context.router.history.push(path)
      break
    case 'signout':
      AuthUtil.signOut()
      break
    default:
      context.router.history.push(data.path)
      break
  }
}

const renderOptions = (user, context) =>
  options.map(option => {
    const optionRoles = get(option, 'roles', undefined)
    if (optionRoles && !AuthUtil.hasSomeRole(user, option.roles)) {
      return null
    }
    return (
      <Dropdown.Item
        className="header-menu__user__menu-item"
        text={option.text}
        key={option.value}
        onClick={(event, data) => onChange(data, user, context)}
        {...option}
      />
    )
  })

const HeaderUserMenu = ({ user }, context) => (
  <Menu className="header-menu__user" compact>
    <Dropdown text={getFullName(user)} item className="hide-on-mobile">
      <Dropdown.Menu>{renderOptions(user, context)}</Dropdown.Menu>
    </Dropdown>
    <Dropdown item className="show-on-mobile">
      <Dropdown.Menu>{renderOptions(user, context)}</Dropdown.Menu>
    </Dropdown>
  </Menu>
)

HeaderUserMenu.defaultProps = {
  user: null,
}

HeaderUserMenu.propTypes = {
  user: PropTypes.object,
}

HeaderUserMenu.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default HeaderUserMenu
