import React, { Component, Fragment } from 'react'
import { Container, Icon } from 'semantic-ui-react'

import HeaderLogo from './HeaderLogo'
import HeaderMenu from './HeaderMenu'
import HeaderUserMenu from './HeaderUserMenu'
import { GlobalSearch } from '../../../search'
import { CurrentUserData } from '../../../users'

const renderMenu = () => (
  <CurrentUserData>
    {({ isAuthenticated, currentUser }) =>
      isAuthenticated ? <HeaderUserMenu user={currentUser} /> : <HeaderMenu />
    }
  </CurrentUserData>
)

class Header extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = { view: 'menu' }
  }

  onSearchClick = () => {
    this.setState({ view: 'search' })
    requestAnimationFrame(() => {
      const searchInput = this.mainContainer.querySelector('.prompt')
      searchInput.focus()
    })
  }

  onSearchBlur = () => {
    setTimeout(() => {
      this.setState({ view: 'menu' })
    }, 150)
  }

  onSearchClose = () => {
    this.setState({ view: 'menu' })
  }

  renderMenuOrSeach() {
    const { view } = this.state
    return view === 'menu' ? (
      <Fragment>
        <HeaderLogo />
        <div className="hbox-raw vertically-center-aligned full-height">
          <Icon name="search" size="big" onClick={this.onSearchClick} />
          {renderMenu()}
        </div>
      </Fragment>
    ) : (
      <GlobalSearch onClose={this.onSearchClose} onBlur={this.onSearchBlur} />
    )
  }

  render() {
    return (
      <div className="header-menu">
        <Container className="full-height">
          <div
            ref={comp => {
              this.mainContainer = comp
            }}
            className="hbox vertically-center-aligned full-height"
          >
            {this.renderMenuOrSeach()}
          </div>
        </Container>
      </div>
    )
  }
}

export default Header
