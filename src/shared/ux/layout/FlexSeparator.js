import React from 'react'

const FlexSeparator = () => <div style={{ flex: 1 }} />

export default FlexSeparator
