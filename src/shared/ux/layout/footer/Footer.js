import React from 'react'
import { Header, Segment } from 'semantic-ui-react'

const AppFooter = () => (
  <Segment className="footer" inverted vertical textAlign="center">
    <div className="hbox-raw justify-content-center">
      <Header as="h5" inverted className="small-margin">
        2018 © MetalHeads
      </Header>
      <Header as="h5" inverted className="small-margin">
        Powered by&nbsp;
        <a
          href="https://www.metal-archives.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          The Metal Archives&nbsp;
        </a>
        &nbsp;and&nbsp;
        <a
          href="http://em.wemakesites.net/"
          target="_blank"
          rel="noopener noreferrer"
        >
          The Metal Archives API
        </a>
      </Header>
    </div>
  </Segment>
)

export default AppFooter
