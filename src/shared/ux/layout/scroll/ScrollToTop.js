import { PureComponent } from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'

class ScrollToTop extends PureComponent {
  componentDidUpdate(prevProps) {
    const { location: currentLocation } = this.props
    const { location: prevLocation } = prevProps
    if (currentLocation !== prevLocation) {
      window.scrollTo(0, 0)
    }
  }

  render() {
    const { children } = this.props
    return children
  }
}

ScrollToTop.propTypes = {
  children: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

export default withRouter(ScrollToTop)
