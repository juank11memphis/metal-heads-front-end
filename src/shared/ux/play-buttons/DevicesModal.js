import React, { Component, Fragment } from 'react'
import Rodal from 'rodal'
import PropTypes from 'prop-types'
import { Query, Mutation } from 'react-apollo'
import Alert from 'react-s-alert'

import { UIUtil } from '../../util'
import { usersQueries } from '../../users'
import { SelectInput } from '../form'

const renderLoader = () => UIUtil.renderLoader('Connecting to Spotify', false)

class DevicesModal extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = { selectedDevice: '' }
  }

  handleChange = selectedDevice => {
    this.setState({ selectedDevice })
  }

  handleError = () => {
    const { onClose } = this.props
    onClose()
    const message = `
      Album not available on your region or your spotify account
      is not a Spotify Premium Account
    `
    Alert.warning(message, {
      position: 'top-right',
      effect: 'jelly',
    })
  }

  handleSuccess = () => {
    const { onClose } = this.props
    onClose()
    const message = `
      Your album should be already playing on your selected Spotify device
    `
    Alert.success(message, {
      position: 'top-right',
      effect: 'jelly',
    })
  }

  renderDevicesSelect(devices) {
    const { selectedDevice } = this.state
    const finalDevices = devices
      ? devices.map(device => ({ value: device.id, label: device.name }))
      : []
    return (
      <SelectInput
        value={selectedDevice}
        options={finalDevices}
        placeholder="Select Device"
        isClearable={false}
        onChange={this.handleChange}
      />
    )
  }

  renderActionButtons() {
    const { onClose, playOnSpotifyMutation, itemId } = this.props
    const { selectedDevice } = this.state
    const deviceId = selectedDevice.value
    return (
      <Mutation
        mutation={playOnSpotifyMutation}
        onError={this.handleError}
        onCompleted={this.handleSuccess}
      >
        {(play, { loading, called }) => {
          if (called && loading) {
            return renderLoader()
          }
          return (
            <Fragment>
              <button
                className="rodal-cancel-btn"
                onClick={onClose}
                type="button"
              >
                Cancel
              </button>
              <button
                type="button"
                className="rodal-confirm-btn"
                onClick={() =>
                  selectedDevice &&
                  play({ variables: { deviceId, id: itemId } })
                }
              >
                Play
              </button>
            </Fragment>
          )
        }}
      </Mutation>
    )
  }

  render() {
    const { open, onClose } = this.props
    return (
      <Rodal
        visible={open}
        onClose={onClose}
        className="spotify-devices-modal"
        width={350}
        height={200}
      >
        <div className="header">Spotify Active Devices</div>
        <div className="body">
          <Query
            query={usersQueries.LOAD_SPOTIFY_DEVICES}
            fetchPolicy="network-only"
          >
            {({ data, loading }) =>
              loading ? (
                renderLoader()
              ) : (
                <Fragment>
                  {this.renderDevicesSelect(data.devices)}
                  {this.renderActionButtons()}
                </Fragment>
              )
            }
          </Query>
        </div>
      </Rodal>
    )
  }
}

DevicesModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  playOnSpotifyMutation: PropTypes.object.isRequired,
  itemId: PropTypes.string.isRequired,
}

export default DevicesModal
