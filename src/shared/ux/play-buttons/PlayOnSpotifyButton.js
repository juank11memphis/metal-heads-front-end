import React from 'react'
import { Icon } from 'semantic-ui-react'
import Alert from 'react-s-alert'
import PropTypes from 'prop-types'

import { CurrentUserData } from '../../users'

const showNotConnectedToSpotifyMessage = () => {
  const message = `
    You are not connected to your Spotify accont.
    Go to your account settings page to do so
  `
  Alert.info(message, {
    position: 'top-right',
    effect: 'jelly',
  })
}

const renderButton = (isConnectedToSpotify, onPlay) => (
  <div className="vbox">
    <Icon
      className="play-buttons__icon"
      name="spotify"
      onClick={() =>
        isConnectedToSpotify ? onPlay() : showNotConnectedToSpotifyMessage()
      }
      fitted
    />
    Spotify
  </div>
)

const PlayOnSpotifyButton = ({ onPlay }) => (
  <CurrentUserData>
    {({ isAuthenticated, isConnectedToSpotify }) =>
      isAuthenticated ? renderButton(isConnectedToSpotify, onPlay) : null
    }
  </CurrentUserData>
)

PlayOnSpotifyButton.propTypes = {
  onPlay: PropTypes.func.isRequired,
}

export default PlayOnSpotifyButton
