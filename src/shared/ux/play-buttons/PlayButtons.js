import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

import DevicesModal from './DevicesModal'
import PlayOnYoutubeButton from './PlayOnYoutubeButton'
import PlayOnSpotifyButton from './PlayOnSpotifyButton'

const getContainerStyle = visible => ({
  opacity: visible ? 1 : 0,
})

class PlayButtons extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = { openDevicesModal: false }
  }

  hideSpotifyDevicesModal = () => {
    this.setState({ openDevicesModal: false })
  }

  render() {
    const {
      className,
      youtubeSearchText,
      playOnSpotifyMutation,
      createPlayAlbumActivityMutation,
      itemId,
      visible,
    } = this.props
    const { openDevicesModal } = this.state
    return (
      <Fragment>
        <div
          className={`play-buttons hbox ${className}`}
          style={getContainerStyle(visible)}
        >
          <PlayOnSpotifyButton
            onPlay={() => this.setState({ openDevicesModal: true })}
          />
          <PlayOnYoutubeButton
            itemId={itemId}
            youtubeSearchText={youtubeSearchText}
            createPlayAlbumActivityMutation={createPlayAlbumActivityMutation}
          />
        </div>
        {openDevicesModal && (
          <DevicesModal
            open
            onClose={this.hideSpotifyDevicesModal}
            playOnSpotifyMutation={playOnSpotifyMutation}
            itemId={itemId}
          />
        )}
      </Fragment>
    )
  }
}

PlayButtons.defaultProps = {
  className: '',
  visible: true,
}

PlayButtons.propTypes = {
  itemId: PropTypes.string.isRequired,
  youtubeSearchText: PropTypes.string.isRequired,
  playOnSpotifyMutation: PropTypes.object.isRequired,
  createPlayAlbumActivityMutation: PropTypes.object.isRequired,
  visible: PropTypes.bool,
  className: PropTypes.string,
}

export default PlayButtons
