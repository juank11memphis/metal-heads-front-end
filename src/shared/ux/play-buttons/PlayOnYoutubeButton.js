import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

const getYoutubeUrl = youtubeSearchText => {
  const query = youtubeSearchText
    .replace('&', '')
    .split(' ')
    .join('+')
  const youtubeUrl = `https://www.youtube.com/results?search_query=${query}`
  return youtubeUrl
}

const playOnYoutube = youtubeSearchText => {
  const win = window.open(getYoutubeUrl(youtubeSearchText), '_blank')
  win.focus()
}

const PlayOnYoutubeButton = ({
  itemId,
  youtubeSearchText,
  createPlayAlbumActivityMutation,
}) => (
  <Mutation mutation={createPlayAlbumActivityMutation}>
    {createPlayAlbumActivity => (
      <div className="vbox">
        <Icon
          className="play-buttons__icon"
          name="youtube play"
          onClick={() => {
            createPlayAlbumActivity({
              variables: { albumId: itemId, target: 'youtube' },
            })
            playOnYoutube(youtubeSearchText)
          }}
          fitted
        />
        Youtube
      </div>
    )}
  </Mutation>
)

PlayOnYoutubeButton.propTypes = {
  itemId: PropTypes.string.isRequired,
  youtubeSearchText: PropTypes.string.isRequired,
  createPlayAlbumActivityMutation: PropTypes.object.isRequired,
}

export default PlayOnYoutubeButton
