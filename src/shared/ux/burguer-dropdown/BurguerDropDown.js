import React, { PureComponent } from 'react'
import { Dropdown } from 'semantic-ui-react'
import PropTypes from 'prop-types'

class BurguerDropDown extends PureComponent {
  constructor(props, context) {
    super(props, context)
    this.state = {
      selectedItem: null,
    }
    this.optionsMap = null
    this.mobileMenu = null
  }

  getSelectedText() {
    const { options, activeItem } = this.props
    const { selectedItem } = this.state
    this.optionsMap = this.getOptionsMap()
    if (activeItem) {
      return this.optionsMap[activeItem]
    }
    if (selectedItem) {
      return selectedItem.text
    }
    if (!selectedItem && (options && options.length > 0)) {
      return options[0].text
    }
    return ''
  }

  getOptionsMap() {
    const { options } = this.props
    const map = {}
    if (!options) {
      return {}
    }
    for (let index = 0, { length } = options; index < length; index += 1) {
      const option = options[index]
      map[option.value] = option.text
    }
    return map
  }

  closeMobileMenu() {
    setTimeout(() => {
      const dropdownEl = this.mobileMenu.ref
      const dropdownMenu = dropdownEl.lastChild
      dropdownMenu.className = 'hide-drop-down-menu menu transition'
    })
  }

  render() {
    const { options, onItemClick, showSelected, className } = this.props
    const selectedText = this.getSelectedText()
    return (
      <div className={`burguer-dropdown-container hbox-raw ${className}`}>
        <Dropdown
          className="burguer-dropdown"
          icon="align justify"
          simple
          ref={comp => {
            this.mobileMenu = comp
          }}
        >
          <Dropdown.Menu>
            {options.map(menuItem => (
              <Dropdown.Item
                key={menuItem.value}
                value={menuItem.value}
                onClick={() => {
                  this.setState({ selectedItem: menuItem })
                  this.closeMobileMenu()
                  onItemClick(menuItem)
                }}
              >
                {menuItem.text}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
        {showSelected && (
          <div>
            <h3 className="no-margin">{selectedText}</h3>
          </div>
        )}
      </div>
    )
  }
}

BurguerDropDown.defaultProps = {
  showSelected: false,
  activeItem: null,
  className: '',
}

BurguerDropDown.propTypes = {
  options: PropTypes.array.isRequired,
  onItemClick: PropTypes.func.isRequired,
  showSelected: PropTypes.bool,
  activeItem: PropTypes.string,
  className: PropTypes.string,
}

export default BurguerDropDown
