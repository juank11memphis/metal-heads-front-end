import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'semantic-ui-react'

const LIGHT_GRAY = '#EEEEEE'
const LIGHT_GREEN = '#3b773b'

class SaveForLaterButton extends Component {
  constructor(props, context) {
    super(props, context)
    const { active } = props
    this.state = {
      active,
    }
  }

  componentWillReceiveProps(nextProps) {
    const { active: currentActive } = this.props
    const { active: nextActive } = nextProps
    if (currentActive !== nextActive) {
      this.setState({
        active: nextActive,
      })
    }
  }

  onClick = () => {
    const { onClick } = this.props
    const { active } = this.state
    const newActive = !active
    this.setState({
      active: newActive,
    })
    if (onClick) {
      onClick(newActive)
    }
  }

  render() {
    const { active } = this.state
    const tooltipText = active ? 'Remove from watchlist' : 'Add to watchlist'
    return (
      <div className="save-for-later-button" data-tooltip={tooltipText}>
        <div
          className="ribbon"
          style={{
            borderLeftColor: active ? LIGHT_GREEN : LIGHT_GRAY,
            borderRightColor: active ? LIGHT_GREEN : LIGHT_GRAY,
          }}
        />
        <Icon name="add" size="big" onClick={() => this.onClick()} />
      </div>
    )
  }
}

SaveForLaterButton.defaultProps = {
  onClick: null,
  active: false,
}

SaveForLaterButton.propTypes = {
  onClick: PropTypes.func,
  active: PropTypes.bool,
}

export default SaveForLaterButton
