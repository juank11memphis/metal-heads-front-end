import he from 'he'

export function decodeString(str = '') {
  return he.decode(str)
}

export function cleanAndJoin(text) {
  return text
    .trim()
    .replace(/\./g, ' ')
    .split(' ')
    .join('-')
}
