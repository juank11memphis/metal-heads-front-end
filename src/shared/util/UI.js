import React from 'react'
import { Loader } from 'semantic-ui-react'

export const renderLoader = (message, inverted = true) => (
  <Loader active indeterminate inverted={inverted} size="medium">
    {message}
  </Loader>
)
