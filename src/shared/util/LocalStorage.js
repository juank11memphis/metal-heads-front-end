export default class LocalStorageUtil {
  static storeToken(token) {
    if (!token) {
      LocalStorageUtil.removeToken()
      return
    }
    localStorage.setItem('metalHeadsToken', token)
  }

  static getToken() {
    return localStorage.getItem('metalHeadsToken')
  }

  static removeToken() {
    localStorage.removeItem('metalHeadsToken')
  }

  static storeBandSelectedData(band) {
    localStorage.setItem('metalHeadsBandSelectedId', band.id)
    localStorage.setItem('metalHeadsBandSelectedName', band.name)
  }

  static getBandSelectedData() {
    return {
      id: localStorage.getItem('metalHeadsBandSelectedId'),
      name: localStorage.getItem('metalHeadsBandSelectedName'),
    }
  }

  static removeBandSelectedData() {
    localStorage.removeItem('metalHeadsBandSelectedId')
    localStorage.removeItem('metalHeadsBandSelectedName')
  }
}
