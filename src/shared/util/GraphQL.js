export function graphErrorToError(error) {
  if (error.networkError) {
    return new Error(error.networkError.message)
  }
  const { graphQLErrors = [] } = error
  const firstError = graphQLErrors[0]
  return new Error(firstError ? firstError.message : '')
}
