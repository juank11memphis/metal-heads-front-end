import { get } from 'lodash'

const evaluateNameSort = (firstName, secondName) => {
  if (firstName < secondName) {
    return -1
  }
  if (firstName > secondName) {
    return 1
  }
  return 0
}

export const sortUserBands = (userBands, sortBy) => {
  if (!sortBy) {
    return userBands
  }
  const sorted = [...userBands]
  sorted.sort((firstUserBand, secondUserBand) => {
    let firstValue
    let secondValue
    if (sortBy === 'name') {
      firstValue = get(firstUserBand, 'band.name', '')
      secondValue = get(secondUserBand, 'band.name', '')
      return evaluateNameSort(firstValue, secondValue)
    }
    if (sortBy === 'userRating') {
      firstValue = get(firstUserBand, 'rating', 0)
      secondValue = get(secondUserBand, 'rating', 0)
    } else {
      firstValue = get(firstUserBand, `band.${sortBy}`, 0)
      secondValue = get(secondUserBand, `band.${sortBy}`, 0)
    }
    return secondValue - firstValue
  })
  return sorted
}

export const sortAlbums = (albums, sortBy) => {
  if (!sortBy || sortBy === '') {
    return albums
  }
  albums.sort((firstAlbum, secondAlbum) => {
    let firstValue = get(firstAlbum, sortBy)
    let secondValue = get(secondAlbum, sortBy)
    if (sortBy === 'name') {
      return evaluateNameSort(firstValue, secondValue)
    }
    if (sortBy === 'year') {
      return firstValue - secondValue
    }
    if (sortBy === 'userRating') {
      firstValue = get(firstAlbum, 'userData.rating', '')
      secondValue = get(secondAlbum, 'userData.rating', '')
    }
    return secondValue - firstValue
  })
  return albums
}
