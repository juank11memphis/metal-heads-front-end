import gql from 'graphql-tag'

import {
  userInfoFull,
  userBandFull,
  userBandShortWithBand,
  userAlbumFull,
} from './usersFragments'

export const LOAD_TOKEN_USER = gql`
  query loadTokenUser {
    user: loadTokenUser {
      ...UserInfoFull
    }
  }
  ${userInfoFull}
`

export const LOAD_USER = gql`
  query loadUser($userId: String) {
    user: loadUser(userId: $userId) {
      firstname
      lastname
    }
  }
`

export const LOAD_SAVED_BANDS = gql`
  query loadSavedBands($userId: String) {
    savedBands: loadSavedBands(userId: $userId) {
      ...UserBandShortWithBand
    }
  }
  ${userBandShortWithBand}
`

export const LOAD_RATED_BANDS = gql`
  query loadRatedBands($userId: String) {
    ratedBands: loadRatedBands(userId: $userId) {
      ...UserBandShortWithBand
    }
  }
  ${userBandShortWithBand}
`
export const LOAD_SPOTIFY_DEVICES = gql`
  query loadSpotifyDevices {
    devices: loadSpotifyDevices {
      id
      name
    }
  }
`

export const LOAD_USER_BAND_DATA = gql`
  query loadUserBandData($userId: String, $bandId: String) {
    userBandData: loadUserBandData(userId: $userId, bandId: $bandId) {
      ...UserBandFull
    }
  }
  ${userBandFull}
`

export const LOAD_USER_ALBUM_DATA = gql`
  query loadAlbumUserData($userId: String, $albumId: String) {
    userAlbumData: loadUserAlbumData(userId: $userId, albumId: $albumId) {
      ...UserAlbumFull
    }
  }
  ${userAlbumFull}
`
