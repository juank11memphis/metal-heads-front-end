import gql from 'graphql-tag'

export const userShort = gql`
  fragment UserShort on User {
    id
    firstname
    lastname
    email
  }
`

export const userInfoFull = gql`
  fragment UserInfoFull on User {
    id
    email
    firstname
    lastname
    country
    hasPassword
    socialAccounts {
      id
      accountType
    }
    roles {
      name
      permissions
    }
  }
`

export const userBandFull = gql`
  fragment UserBandFull on UserBand {
    id
    rating
    status
    saved
    bandId
    userId
    userAlbums {
      id
      rating
      albumId
      userId
    }
  }
`

export const userBandShortWithBand = gql`
  fragment UserBandShortWithBand on UserBand {
    id
    rating
    status
    saved
    bandId
    userId
    band {
      id
      globalRating
      ratingsCount
      name
      picture
    }
  }
`

export const userAlbumFull = gql`
  fragment UserAlbumFull on UserAlbum {
    id
    rating
    userId
    albumId
  }
`

export const userAlbumFullWithAlbum = gql`
  fragment UserAlbumFullWithAlbum on UserAlbum {
    id
    rating
    userId
    albumId
    album {
      id
      name
      picture
      ratingsCount
      globalRating
    }
  }
`
