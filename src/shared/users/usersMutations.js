import gql from 'graphql-tag'

import {
  userInfoFull,
  userBandShortWithBand,
  userAlbumFullWithAlbum,
} from './usersFragments'

export const SIGN_IN = gql`
  mutation signin($email: String, $password: String) {
    auth: signin(email: $email, password: $password) {
      token
      user {
        ...UserInfoFull
      }
    }
  }
  ${userInfoFull}
`

export const AUTH_SOCIAL = gql`
  mutation authSocial($user: UserInput) {
    auth: authSocial(user: $user) {
      token
      user {
        ...UserInfoFull
      }
    }
  }
  ${userInfoFull}
`

export const SIGN_UP = gql`
  mutation signup($user: UserInput) {
    auth: signup(user: $user) {
      token
      user {
        ...UserInfoFull
      }
    }
  }
  ${userInfoFull}
`

export const REQUEST_PASSWORD_RESET = gql`
  mutation requestPasswordReset($email: String) {
    requestPasswordReset(email: $email) {
      id
    }
  }
`

export const RESET_PASSWORD = gql`
  mutation resetPassword($password: String, $token: String) {
    auth: resetPassword(password: $password, token: $token) {
      token
      user {
        ...UserInfoFull
      }
    }
  }

  ${userInfoFull}
`

export const UPDATE_USER = gql`
  mutation updateTokenUser($user: UserInput) {
    user: updateTokenUser(user: $user) {
      ...UserInfoFull
    }
  }
  ${userInfoFull}
`
export const CHANGE_PASSWORD = gql`
  mutation changePassword($password: String, $newPassword: String) {
    user: changeTokenUserPassword(
      password: $password
      newPassword: $newPassword
    ) {
      ...UserInfoFull
    }
  }
  ${userInfoFull}
`

export const SPOTIFY_DISCONNECT = gql`
  mutation spotifyDisconnect {
    user: spotifyDisconnect {
      ...UserInfoFull
    }
  }
  ${userInfoFull}
`

export const RATE_BAND = gql`
  mutation rateBand($bandId: String, $rating: Float) {
    userBand: rateBand(bandId: $bandId, rating: $rating) {
      ...UserBandShortWithBand
    }
  }
  ${userBandShortWithBand}
`

export const REMOVE_BAND_RATING = gql`
  mutation removeBandRating($bandId: String) {
    userBand: removeBandRating(bandId: $bandId) {
      ...UserBandShortWithBand
    }
  }
  ${userBandShortWithBand}
`

export const SAVE_BAND_FOR_LATER = gql`
  mutation saveBandForLater($bandId: String) {
    userBand: saveBandForLater(bandId: $bandId) {
      ...UserBandShortWithBand
    }
  }
  ${userBandShortWithBand}
`

export const REMOVE_FROM_SAVED_BANDS = gql`
  mutation removeFromSavedBands($bandId: String) {
    userBand: removeFromSavedBands(bandId: $bandId) {
      ...UserBandShortWithBand
    }
  }
  ${userBandShortWithBand}
`

export const RATE_ALBUM = gql`
  mutation rateAlbum($albumId: String, $rating: Float) {
    userAlbum: rateAlbum(albumId: $albumId, rating: $rating) {
      ...UserAlbumFullWithAlbum
    }
  }
  ${userAlbumFullWithAlbum}
`

export const REMOVE_ALBUM_RATING = gql`
  mutation removeAlbumRating($albumId: String) {
    userAlbum: removeAlbumRating(albumId: $albumId) {
      ...UserAlbumFullWithAlbum
    }
  }
  ${userAlbumFullWithAlbum}
`
