import * as types from './usersActionTypes'
import initialState from '../../core/initialState'

export default function usersReducer(state = initialState.users, action) {
  switch (action.type) {
    case types.AUTH_SUCCESS:
      return Object.assign({}, state, {
        isAuthenticated: true,
        currentUser: action.payload,
      })
    case types.AUTH_FAILURE:
      return Object.assign({}, state, {
        isAuthenticated: false,
        currentUser: null,
      })
    case types.USER_UPDATE_SUCCESS:
      return Object.assign({}, state, {
        currentUser: action.payload,
      })
    default:
      return state
  }
}
