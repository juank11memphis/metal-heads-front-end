export const AUTH_SUCCESS = '[users]AUTH_SUCCESS'
export const AUTH_FAILURE = '[users]AUTH_FAILURE'
export const USER_UPDATE_SUCCESS = '[users]USER_UPDATE_SUCCESS'
