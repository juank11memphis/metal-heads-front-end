import React from 'react'
import { GoogleLogin } from 'react-google-login'
import { Icon } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import SpotifyButton from './SpotifyButton'
import SpotifyTokenValidator from './SpotifyTokenValidator'

const SocialAccountsButtons = props => {
  const onSignInWithSpotify = () => {
    window.location.href = '/api/auth/signin/spotify'
  }

  const onGoogleResponse = response => {
    if (response.error) {
      return
    }
    const { onSocialAuthenticated } = props
    const { profileObj } = response
    onSocialAuthenticated({
      firstname: profileObj.givenName,
      lastname: profileObj.familyName,
      email: profileObj.email,
    })
  }

  const renderSpotifyButton = () => {
    const { spotifyText } = props
    return (
      <div className="social-accounts-buttons__spotify-button">
        <SpotifyButton onClick={onSignInWithSpotify} text={spotifyText} />
      </div>
    )
  }

  const renderGoogleButton = () => {
    const { googleText, googleClientId } = props
    return (
      <div className="social-accounts-buttons__google-button">
        <GoogleLogin
          clientId={googleClientId}
          onSuccess={onGoogleResponse}
          onFailure={onGoogleResponse}
          uxMode="popup"
        >
          <Icon name="google" />
          {googleText}
        </GoogleLogin>
      </div>
    )
  }

  return (
    <div className="social-accounts-buttons">
      {renderGoogleButton()}
      {renderSpotifyButton()}
      <SpotifyTokenValidator />
    </div>
  )
}

SocialAccountsButtons.propTypes = {
  googleText: PropTypes.string.isRequired,
  spotifyText: PropTypes.string.isRequired,
  onSocialAuthenticated: PropTypes.func.isRequired,
  googleClientId: PropTypes.string.isRequired,
}

export default SocialAccountsButtons
