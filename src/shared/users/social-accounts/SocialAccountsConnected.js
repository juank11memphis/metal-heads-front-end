import React, { PureComponent } from 'react'
import { Checkbox, Segment } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import { ErrorMessage } from '../../ux/messages'
import CurrentUserData from '../CurrentUserData'
import { SPOTIFY_DISCONNECT } from '../usersMutations'
import { AuthUtil } from '../../util'

const getSpotifyButtonText = isConnectedToSpotify =>
  isConnectedToSpotify ? 'Connected to Spotify' : 'Connect to Spotify'

const doSpotifyConnect = () => {
  const token = AuthUtil.getBearerToken()
  window.location.href = `/api/users/me/connect/spotify?authorization=${token}`
}

class SocialAccountsConnected extends PureComponent {
  state = {
    spotifyButtonDisabled: false,
    error: null,
  }

  doSpotifyDisconnect = async (spotifyDisconnect, usersActions) => {
    try {
      const { data } = await spotifyDisconnect()
      usersActions.userUpdateSuccess(data.user)
      this.setState({ error: null })
    } catch (error) {
      this.setState({ error })
    }
  }

  renderConnectCheckbox(isConnectedToSpotify, usersActions) {
    const { spotifyButtonDisabled } = this.state
    return (
      <Mutation mutation={SPOTIFY_DISCONNECT}>
        {spotifyDisconnect => (
          <Checkbox
            label={getSpotifyButtonText(isConnectedToSpotify)}
            toggle
            checked={isConnectedToSpotify}
            onChange={(event, { checked }) => {
              if (checked) {
                this.setState({ spotifyButtonDisabled: true })
                doSpotifyConnect()
              } else {
                this.doSpotifyDisconnect(spotifyDisconnect, usersActions)
              }
            }}
            disabled={spotifyButtonDisabled}
          />
        )}
      </Mutation>
    )
  }

  render() {
    const { error } = this.state
    return (
      <CurrentUserData>
        {({ isConnectedToSpotify, usersActions }) => (
          <div className="account-page__social-accounts">
            <Segment compact>
              <div className="vbox">
                {this.renderConnectCheckbox(isConnectedToSpotify, usersActions)}
                <span className="account-page__social-accounts__info-text">
                  Play albums on Spotify <br />
                  with a Spotify Premium Account
                </span>
              </div>
            </Segment>
            {error && <ErrorMessage error={error} />}
          </div>
        )}
      </CurrentUserData>
    )
  }
}

export default SocialAccountsConnected
