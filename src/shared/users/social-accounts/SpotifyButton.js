import React from 'react'
import PropTypes from 'prop-types'
import { Button, Icon } from 'semantic-ui-react'

const SpotifyButton = ({ onClick, text }) => (
  <Button className="spotify-button" onClick={onClick}>
    <Icon name="spotify" /> {text}
  </Button>
)

SpotifyButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
}

export default SpotifyButton
