import React from 'react'
import PropTypes from 'prop-types'
import queryString from 'query-string'

import { LocalStorageUtil } from '../../util'
import TokenValidator from '../TokenValidator'

const SpotifyTokenValidator = (props, context) => {
  const { router } = context
  const { search } = router.route.location
  const queryStringParsed = queryString.parse(search)
  const { spotifyError, token } = queryStringParsed
  if (spotifyError) {
    return (
      <div className="ui red basic label error-container">
        Unexpected error signing in with spotify
      </div>
    )
  }
  if (token) {
    LocalStorageUtil.storeToken(token)
    return <TokenValidator forceValidation />
  }
  return null
}

SpotifyTokenValidator.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default SpotifyTokenValidator
