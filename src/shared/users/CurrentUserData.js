import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { isUserConnectedToSpotify } from './usersSelectors'
import * as usersActions from './usersActions'

const CurrentUserData = ({
  isAuthenticated,
  isConnectedToSpotify,
  currentUser,
  usersActions,
  children,
}) =>
  children({
    isAuthenticated,
    isConnectedToSpotify,
    currentUser,
    usersActions,
  })

CurrentUserData.defaultProps = {
  isAuthenticated: null,
  isConnectedToSpotify: false,
  currentUser: null,
}

CurrentUserData.propTypes = {
  children: PropTypes.func.isRequired,
  usersActions: PropTypes.shape({
    userUpdateSuccess: PropTypes.func,
    authSuccess: PropTypes.func,
  }).isRequired,
  isAuthenticated: PropTypes.bool,
  isConnectedToSpotify: PropTypes.bool,
  currentUser: PropTypes.object,
}

const mapStateToProps = reduxState => ({
  isAuthenticated: reduxState.users.isAuthenticated,
  isConnectedToSpotify: isUserConnectedToSpotify(reduxState),
  currentUser: reduxState.users.currentUser,
})

const mapDispatchToProps = dispatch => ({
  usersActions: bindActionCreators(usersActions, dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CurrentUserData)
