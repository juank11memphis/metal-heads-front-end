import * as types from './usersActionTypes'

export const authSuccess = payload => ({ type: types.AUTH_SUCCESS, payload })
export const authFailure = payload => ({ type: types.AUTH_FAILURE, payload })
export const userUpdateSuccess = payload => ({
  type: types.USER_UPDATE_SUCCESS,
  payload,
})
