import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'

import CurrentUserData from './CurrentUserData'

const AlreadyLoggedInRedirect = ({ redirectTo }) => (
  <CurrentUserData>
    {({ isAuthenticated }) =>
      isAuthenticated === true ? <Redirect to={redirectTo} /> : null
    }
  </CurrentUserData>
)

AlreadyLoggedInRedirect.propTypes = {
  redirectTo: PropTypes.string.isRequired,
}

export default AlreadyLoggedInRedirect
