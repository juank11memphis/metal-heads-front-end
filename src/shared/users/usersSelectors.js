import { createSelector } from 'reselect'

const getCurrentUser = state => state.users.currentUser

export const getCurrentUserSocialAccountsMap = createSelector(
  [getCurrentUser],
  currentUser => {
    if (!currentUser || !currentUser.socialAccounts) {
      return {}
    }
    const socialAccountsMap = currentUser.socialAccounts.reduce(
      (map, account) => ({ ...map, [account.accountType]: true }),
      {},
    )
    return socialAccountsMap
  },
)

export const isUserConnectedToSpotify = createSelector(
  getCurrentUserSocialAccountsMap,
  socialAccountsMap => socialAccountsMap.spotify,
)
