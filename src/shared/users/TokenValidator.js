import React from 'react'
import { ApolloConsumer } from 'react-apollo'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { LocalStorageUtil } from '../util/index'
import { usersActions, usersQueries } from '.'

const isRedirectFromSocialSignin = () => {
  const url = window.location.href
  if (url.indexOf('token') >= 0) {
    return true
  }
  return false
}

const validateToken = async (client, usersActions, forceValidation) => {
  if (isRedirectFromSocialSignin() && !forceValidation) {
    return
  }
  try {
    const { data } = await client.query({
      query: usersQueries.LOAD_TOKEN_USER,
    })
    usersActions.authSuccess(data.user)
  } catch (error) {
    LocalStorageUtil.removeToken()
    usersActions.authFailure(error)
  }
}

const TokenValidator = ({ usersActions, forceValidation }) => (
  <ApolloConsumer>
    {client => {
      validateToken(client, usersActions, forceValidation)
      return null
    }}
  </ApolloConsumer>
)

TokenValidator.defaultProps = {
  forceValidation: false,
}

TokenValidator.propTypes = {
  usersActions: PropTypes.shape({
    authSuccess: PropTypes.func,
    authFailure: PropTypes.func,
  }).isRequired,
  forceValidation: PropTypes.bool,
}

function mapDispatchToProps(dispatch) {
  return {
    usersActions: bindActionCreators(usersActions, dispatch),
  }
}

export default connect(
  null,
  mapDispatchToProps,
)(TokenValidator)
