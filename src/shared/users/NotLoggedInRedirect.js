import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'

import CurrentUserData from './CurrentUserData'

const NotLoggedInRedirect = ({ redirectTo }) => (
  <CurrentUserData>
    {({ isAuthenticated }) =>
      isAuthenticated === false ? <Redirect to={redirectTo} /> : null
    }
  </CurrentUserData>
)

NotLoggedInRedirect.propTypes = {
  redirectTo: PropTypes.string.isRequired,
}

export default NotLoggedInRedirect
