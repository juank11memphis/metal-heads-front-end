import gql from 'graphql-tag'
import { bandsFragments } from '../../components/bands'
import { albumsFragments } from '../../components/albums'
import { artistsFragments } from '../../components/artists'

export const GLOBAL_SEARCH = gql`
  query search($searchText: String) {
    search(searchText: $searchText) {
      bands {
        ...BandSearchInfo
      }
      albums {
        ...AlbumSearchInfo
      }
      artists {
        ...ArtistSearchInfo
      }
    }
  }
  ${bandsFragments.bandSearchInfo}
  ${albumsFragments.albumSearchInfo}
  ${artistsFragments.artistSearchInfo}
`
