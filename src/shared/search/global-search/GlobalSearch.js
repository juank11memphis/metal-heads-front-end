import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'semantic-ui-react'
import { ApolloConsumer } from 'react-apollo'

import SearchDropdown from '../dropdown/SearchDropdown'
import { GLOBAL_SEARCH } from '../searchQueries'

class GlobalSearch extends PureComponent {
  static resultRenderer(item) {
    const typeProp = '__typename'
    if (item[typeProp] === 'BandSearch') {
      return (
        <div>
          <div className="title">{item.title}</div>
          <div className="description">{item.country}</div>
          <div className="description">{item.genre}</div>
        </div>
      )
    }
    if (item[typeProp] === 'AlbumSearch') {
      return (
        <div>
          <div className="title">{item.title}</div>
          <div className="description">{item.year}</div>
          <div className="description">{item.type}</div>
        </div>
      )
    }
    return (
      <div>
        <div className="title">{item.title}</div>
        <div className="description">{item.gender}</div>
      </div>
    )
  }

  static formatSearchResultsForRendering(searchResults) {
    const categoriesOrder = ['bands', 'albums', 'artists']
    const finalResult = {}
    categoriesOrder.forEach(category => {
      const items = searchResults[category]
      if (!items || items.length === 0) {
        return
      }
      const typenameProp = '__typename'
      finalResult[category] = {
        name: category,
        results: items.map(item => ({
          childKey: item.id,
          title: item.name,
          country: item.country,
          genre: item.genre,
          year: item.year,
          type: item.type,
          gender: item.gender,
          id: item.id,
          [typenameProp]: item[typenameProp],
        })),
      }
    })
    return finalResult
  }

  componentWillMount() {
    this.resetComponent()
  }

  onItemSelected = item => {
    const sufixMap = {
      bands: '/info',
      artists: '',
      albums: '',
    }
    const typenameProp = '__typename'
    const itemType = item[typenameProp].replace('Search', '')
    const pathType = `${itemType.toLowerCase()}s`
    const path = `/${pathType}/${item.id}${sufixMap[pathType]}`
    const { router } = this.context
    const { history } = router
    history.push(path)
  }

  handleSearchChange = async (client, searchText, doneTyping) => {
    try {
      this.setState({ value: searchText })
      if (doneTyping) {
        const { data } = await client.query({
          query: GLOBAL_SEARCH,
          variables: { searchText },
        })
        this.setState({
          results: GlobalSearch.formatSearchResultsForRendering(data.search),
        })
      }
    } catch (error) {
      this.setState({ results: {} })
    }
  }

  resetComponent() {
    this.setState({
      results: {},
      value: '',
    })
  }

  render() {
    const { onClose, onBlur } = this.props
    const { value, results } = this.state

    return (
      <div className="global-search hbox-raw vertically-center-aligned full-height">
        <ApolloConsumer>
          {client => (
            <SearchDropdown
              className="flex-child-full"
              category
              resultRenderer={GlobalSearch.resultRenderer}
              results={results}
              value={value}
              onSearchChange={(searchText, doneTyping) =>
                this.handleSearchChange(client, searchText, doneTyping)
              }
              onResultSelect={(event, { result }) =>
                this.onItemSelected(result)
              }
              onBlur={() => onBlur && onBlur()}
            />
          )}
        </ApolloConsumer>
        <Icon name="close" size="big" onClick={() => onClose && onClose()} />
      </div>
    )
  }
}

GlobalSearch.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

GlobalSearch.defaultProps = {
  onBlur: null,
  onClose: null,
}

GlobalSearch.propTypes = {
  onClose: PropTypes.func,
  onBlur: PropTypes.func,
}

export default GlobalSearch
