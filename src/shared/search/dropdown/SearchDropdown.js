import { Search } from 'semantic-ui-react'
import PropTypes from 'prop-types'

class SearchDropdown extends Search {
  constructor(props, context) {
    super(props, context)
    this.doneTyping = this.doneTyping.bind(this)
    this.typingTimer = undefined
  }

  /* eslint-disable no-invalid-this */
  handleSearchChange = event => {
    // prevent propagating to this.props.onChange()
    const { onSearchChange } = this.props
    const newQuery = event.target.value
    event.stopPropagation()
    clearTimeout(this.typingTimer)
    this.typingTimer = setTimeout(() => this.doneTyping(newQuery), 300)
    if (onSearchChange) onSearchChange(newQuery, false)
  }

  doneTyping(newQuery) {
    const { minCharacters, onSearchChange } = this.props
    const { open } = this.state

    // open search dropdown on search query
    if (newQuery.length < minCharacters) {
      this.close()
    } else if (!open) {
      this.tryOpen(newQuery)
    }

    this.setValue(newQuery)

    if (onSearchChange) {
      onSearchChange(newQuery, true)
    }
  }
}

SearchDropdown.propTypes = {
  defaultOpen: PropTypes.bool,
  open: PropTypes.bool,
  defaultValue: PropTypes.string,
  value: PropTypes.string,
}

export default SearchDropdown
