import gql from 'graphql-tag'

export const LOAD_CONFIG = gql`
  query loadConfig {
    config: loadConfig {
      googleClientId
    }
  }
`
