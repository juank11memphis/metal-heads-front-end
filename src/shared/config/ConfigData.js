import PropTypes from 'prop-types'
import { connect } from 'react-redux'

const ConfigData = ({ googleClientId, children }) =>
  googleClientId
    ? children({
        googleClientId,
      })
    : null

ConfigData.defaultProps = {
  googleClientId: null,
}

ConfigData.propTypes = {
  children: PropTypes.func.isRequired,
  googleClientId: PropTypes.string,
}

const mapStateToProps = reduxState => ({
  googleClientId: reduxState.config.googleClientId,
})

export default connect(
  mapStateToProps,
  null,
)(ConfigData)
