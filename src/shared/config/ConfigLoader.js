import React from 'react'
import { ApolloConsumer } from 'react-apollo'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { configActions } from '.'
import { LOAD_CONFIG } from './configQueries'

const ConfigLoader = props => {
  const { configActions } = props
  const loadConfig = async client => {
    try {
      const { data } = await client.query({ query: LOAD_CONFIG })
      configActions.loadConfigSuccess(data.config)
    } catch (error) {
      configActions.loadConfigSuccess({})
    }
  }

  return (
    <ApolloConsumer>
      {client => {
        loadConfig(client)
        return null
      }}
    </ApolloConsumer>
  )
}

ConfigLoader.propTypes = {
  configActions: PropTypes.shape({
    loadConfigSuccess: PropTypes.func,
  }).isRequired,
}

function mapDispatchToProps(dispatch) {
  return {
    configActions: bindActionCreators(configActions, dispatch),
  }
}

export default connect(
  null,
  mapDispatchToProps,
)(ConfigLoader)
