import { LOAD_CONFIG_SUCCESS } from './configActions'
import initialState from '../../core/initialState'

export default function configReducer(state = initialState.config, action) {
  switch (action.type) {
    case LOAD_CONFIG_SUCCESS:
      return action.payload
    default:
      return state
  }
}
