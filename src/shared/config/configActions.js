export const LOAD_CONFIG_SUCCESS = '[config]LOAD_CONFIG_SUCCESS'
export const loadConfigSuccess = payload => ({
  type: LOAD_CONFIG_SUCCESS,
  payload,
})
