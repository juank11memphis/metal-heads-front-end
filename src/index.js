import React from 'react'
import ReactDom from 'react-dom'
import { Provider } from 'react-redux'

import 'css-reset-and-normalize-sass/css/flavored-reset-and-normalize.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'semantic-ui-css/semantic.min.css'
import 'rc-slider/assets/index.css'
import 'react-s-alert/dist/s-alert-default.css'
import 'react-s-alert/dist/s-alert-css-effects/jelly.css'
import 'rodal/lib/rodal.css'

import App from './components/App'
import configureStore from './core/configureStore'
import configureAjax from './core/configureAjax'

import './styles/main.scss'

const store = configureStore()

configureAjax()

ReactDom.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app'),
)
