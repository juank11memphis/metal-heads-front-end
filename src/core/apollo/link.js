import { setContext } from 'apollo-link-context'
import { createHttpLink } from 'apollo-link-http'
import { ApolloLink } from 'apollo-link'

import { AuthUtil, LocalStorageUtil } from '../../shared/util'

const httpLink = createHttpLink({ uri: '/graphql' })

const contextLink = setContext(() => ({
  headers: {
    authorization: AuthUtil.getBearerToken(),
  },
}))

const handleAuthTokenLink = new ApolloLink((operation, forward) =>
  forward(operation).map(response => {
    const {
      data: { auth },
    } = response
    if (auth && auth.token) {
      LocalStorageUtil.storeToken(auth.token)
    }
    return response
  }),
)

const link = ApolloLink.from([contextLink, handleAuthTokenLink, httpLink])

export default link
