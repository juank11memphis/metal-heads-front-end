import { IntrospectionFragmentMatcher } from 'apollo-cache-inmemory/lib'

export default new IntrospectionFragmentMatcher({
  introspectionQueryResultData: {
    __schema: {
      types: [
        {
          kind: 'INTERFACE',
          name: 'Band',
          possibleTypes: [{ name: 'ArtistBand' }, { name: 'Band' }],
        },
      ],
    },
  },
})
