import axios from 'axios'
import { AuthUtil } from '../shared/util'

export default function configureAjax() {
  axios.defaults.headers.common.authorization = AuthUtil.getBearerToken()
}
