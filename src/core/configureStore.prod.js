import { createStore } from 'redux'

import rootReducer from './rootReducer'

export default function configureStore(apolloClient) {
  return createStore(rootReducer(apolloClient))
}
