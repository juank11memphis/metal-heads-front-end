import { createStore, applyMiddleware, compose } from 'redux'
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'

import rootReducer from './rootReducer'

export default function configureStore() {
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  return createStore(
    rootReducer(),
    composeEnhancers(applyMiddleware(reduxImmutableStateInvariant())),
  )
  /* eslint-enable */
}
