import { combineReducers } from 'redux'

import { configReducer } from '../shared/config'
import { usersReducer } from '../shared/users'

export default function() {
  const rootReducer = combineReducers({
    config: configReducer,
    users: usersReducer,
  })
  return rootReducer
}
