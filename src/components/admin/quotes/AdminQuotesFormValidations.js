import { object, string } from 'yup'

const validations = object().shape({
  text: string()
    .min(6, 'Quote text must have at least 6 characters')
    .required('Quote text is required'),
})

export default validations
