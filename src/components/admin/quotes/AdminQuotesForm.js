import React from 'react'
import { Button } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import { FormContainer, FieldContainer, Input } from '../../../shared/ux/form'
import validations from './AdminQuotesFormValidations'
import { quotesMutations } from '../../quotes'

const AdminQuotesForm = () => (
  <Mutation mutation={quotesMutations.CREATE_QUOTE}>
    {createQuote => {
      const submitAction = async quote => {
        const data = await createQuote({ variables: { quote } })
        return data.quote
      }
      return (
        <FormContainer
          className="account-page__details"
          successMessage="Quote added successfully"
          initialValues={{ text: '', author: '' }}
          validations={validations}
          resetFormOnSubmit
          submitAction={submitAction}
        >
          <FieldContainer
            name="text"
            render={({ field }) => (
              <Input placeholder="Quote text" field={field} />
            )}
          />
          <FieldContainer
            name="author"
            render={({ field }) => (
              <Input placeholder="Quote author" field={field} />
            )}
          />
          <Button secondary type="submit">
            Add quote
          </Button>
        </FormContainer>
      )
    }}
  </Mutation>
)

export default AdminQuotesForm
