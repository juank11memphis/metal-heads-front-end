import React, { PureComponent } from 'react'
import { Tab } from 'semantic-ui-react'
import { get } from 'lodash'
import { Redirect } from 'react-router-dom'

import { AuthUtil } from '../../shared/util'
import AdminQuotesForm from './quotes/AdminQuotesForm'
import { CurrentUserData } from '../../shared/users'

export default class AdminPage extends PureComponent {
  getTabPanes(currentUser) {
    this.panes = [
      {
        menuItem: 'Quotes',
        permissions: ['create:quote'],
        render: () => <AdminQuotesForm />,
      },
      {
        menuItem: 'Bands',
        permissions: ['update:band'],
        render: () => <div>bands</div>,
      },
    ]
    return this.panes.filter(paneItem => {
      const paneItemPermissions = get(paneItem, 'permissions', undefined)
      if (!paneItemPermissions) {
        return true
      }
      return AuthUtil.hasSomePermission(currentUser, paneItemPermissions)
    })
  }

  render() {
    return (
      <CurrentUserData>
        {({ isAuthenticated, currentUser }) =>
          isAuthenticated === false ||
          (currentUser && !AuthUtil.isAdmin(currentUser)) ? (
            <Redirect to="/signin" />
          ) : (
            <div className="default-detail-page admin-page">
              <Tab panes={this.getTabPanes(currentUser)} />
            </div>
          )
        }
      </CurrentUserData>
    )
  }
}
