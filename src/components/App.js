import React from 'react'
import { Container } from 'semantic-ui-react'
import Alert from 'react-s-alert'
import { BrowserRouter } from 'react-router-dom'
import { hot } from 'react-hot-loader'
import { ApolloProvider } from 'react-apollo'

import { Header, Footer } from '../shared/ux/layout'
import { ConfigLoader } from '../shared/config'
import { TokenValidator } from '../shared/users'
import AppRoutes from './AppRoutes'
import configureApolloClient from '../core/apollo/configureApolloClient'

const apolloClient = configureApolloClient()

const App = () => (
  <ApolloProvider client={apolloClient}>
    <BrowserRouter>
      <div className="main-container">
        <ConfigLoader />
        <TokenValidator />
        <Header />
        <Container>
          <AppRoutes />
        </Container>
        <Footer />
        <Alert stack={{ limit: 3 }} />
      </div>
    </BrowserRouter>
  </ApolloProvider>
)

export default hot(module)(App)
