import gql from 'graphql-tag'

export const LOAD_NEWS_BY_SOURCE = gql`
  query loadNewsBySource($source: String) {
    news: loadNewsBySource(source: $source) {
      id
      link
      title
    }
  }
`
