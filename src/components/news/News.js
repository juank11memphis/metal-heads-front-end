import React from 'react'
import { Grid } from 'semantic-ui-react'
import { Query } from 'react-apollo'

import NewsList from './NewsList'
import { LOAD_NEWS_BY_SOURCE } from './newsQueries'

const News = () => (
  <div className="home-page__news-section">
    <Grid centered>
      <Grid.Column computer={8} tablet={8} mobile={16}>
        <Query query={LOAD_NEWS_BY_SOURCE} variables={{ source: 'loudwire' }}>
          {({ data, error, loading }) => (
            <NewsList
              title="Loudwire"
              link="http://loudwire.com/"
              className="home-page__news-section__news-list default-paddding"
              data={data}
              error={error}
              loading={loading}
            />
          )}
        </Query>
      </Grid.Column>
      <Grid.Column computer={8} tablet={8} mobile={16}>
        <Query
          query={LOAD_NEWS_BY_SOURCE}
          variables={{ source: 'blabbermouth' }}
        >
          {({ data, error, loading }) => (
            <NewsList
              title="Blabbermouth"
              link="http://www.blabbermouth.net/"
              className="home-page__news-section__news-list default-paddding"
              data={data}
              error={error}
              loading={loading}
            />
          )}
        </Query>
      </Grid.Column>
    </Grid>
  </div>
)

export default News
