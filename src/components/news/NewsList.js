import React from 'react'
import { Divider } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import { ErrorMessage } from '../../shared/ux/messages'
import { UIUtil } from '../../shared/util'

const NewsList = props => {
  const { data, error, loading, title, link, className } = props
  const renderNewsItems = () => {
    const { news = [] } = data
    if (error) {
      return (
        <ErrorMessage
          error={error}
          message="An unexpected error has occurred loading news"
        />
      )
    }
    return news.map(newsItem => (
      <div key={newsItem.id} className="news-item">
        <h5>
          <a href={newsItem.link} target="_blank" rel="noopener noreferrer">
            {newsItem.title}
          </a>
        </h5>
        <Divider />
      </div>
    ))
  }

  return (
    <div className={className} data-source={title}>
      <h4>
        Latest news from{' '}
        <a href={link} target="_blank" rel="noopener noreferrer">
          {title}
        </a>
      </h4>
      <Divider />
      {loading ? UIUtil.renderLoader('Loading News...') : renderNewsItems()}
    </div>
  )
}

NewsList.defaultProps = {
  className: '',
  error: null,
  data: {},
}

NewsList.propTypes = {
  data: PropTypes.shape({
    news: PropTypes.array,
  }),
  title: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  className: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.object,
}

export default NewsList
