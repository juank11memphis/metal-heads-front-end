import News from './News'
import * as newsQueries from './newsQueries'

export { News, newsQueries }
