import React from 'react'
import { Segment } from 'semantic-ui-react'
import PropTypes from 'prop-types'

const HomePageHeader = () => (
  <Segment
    className="home-page__header vbox"
    inverted
    vertical
    textAlign="center"
  >
    <div className="home-page__header__slogan">
      <h2>Share your love for metal with the world</h2>
    </div>
  </Segment>
)

HomePageHeader.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default HomePageHeader
