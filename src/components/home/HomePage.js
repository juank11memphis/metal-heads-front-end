import React from 'react'

import { LocalStorageUtil } from '../../shared/util'
import HomePageHeader from './HomePageHeader'
import { MostRatedBands } from '../bands'
import { News } from '../news'
import { LatestBandRatings, LatestAlbumRatings } from '../activities'

const HomePage = () => {
  LocalStorageUtil.removeBandSelectedData()

  return (
    <div className="home-page">
      <HomePageHeader />
      <MostRatedBands />
      <LatestBandRatings />
      <LatestAlbumRatings />
      <News />
    </div>
  )
}

export default HomePage
