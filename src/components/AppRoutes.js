import React from 'react'
import { Route, Switch } from 'react-router-dom'

import { HomePage } from './home'
import { BandDetailPage } from './bands'
import { ArtistDetailPage } from './artists'
import { AlbumDetailPage } from './albums'
import {
  SignUpPage,
  SignInPage,
  ForgotPasswordPage,
  ResetPasswordPage,
} from './auth'
import { AccountPage } from './account'
import { AdminPage } from './admin'
import { ProfilePage } from './profile'

const AppRoutes = () => (
  <Switch>
    <Route exact path="/" render={() => <HomePage />} />
    <Route
      path="/bands/:id"
      render={({
        match: {
          params: { id },
        },
      }) => <BandDetailPage bandId={id} />}
    />
    <Route
      path="/artists/:id"
      render={({
        match: {
          params: { id },
        },
      }) => <ArtistDetailPage artistId={id} />}
    />
    <Route
      path="/albums/:id"
      render={({
        match: {
          params: { id },
        },
      }) => <AlbumDetailPage albumId={id} />}
    />
    <Route path="/signin" render={() => <SignInPage />} />
    <Route path="/signup" render={() => <SignUpPage />} />
    <Route path="/forgot-password" render={() => <ForgotPasswordPage />} />
    <Route path="/reset-password" render={() => <ResetPasswordPage />} />
    <Route path="/user/account" render={() => <AccountPage />} />
    <Route path="/admin" render={() => <AdminPage />} />
    <Route
      path="/user/profile/:id"
      render={({
        match: {
          params: { id },
        },
      }) => <ProfilePage userId={id} />}
    />
  </Switch>
)

export default AppRoutes
