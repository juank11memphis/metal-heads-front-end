import BandDetailPage from './detail/BandDetailPage'
import MostRatedBands from './most-rated/MostRatedBands'
import * as bandsQueries from './bandsQueries'
import * as bandsFragments from './bandsFragments'

export { BandDetailPage, MostRatedBands, bandsQueries, bandsFragments }
