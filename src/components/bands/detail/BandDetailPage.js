import React from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'

import { LOAD_BAND_DETAILS } from '../bandsQueries'
import BandDetailHeader from './header/BandDetailHeader'
import BandDetailGeneralInfo from './generalinfo/BandDetailGeneralInfo'
import BandDetailDiscography from './discography/BandDetailDiscography'
import BandDetailSimilarArtists from './similarartists/BandDetailSimilarArtists'
import { UIUtil, LocalStorageUtil } from '../../../shared/util'
import { ErrorMessage } from '../../../shared/ux/messages'
import { QueryWrapper } from '../../../shared/ux/graphql'
import { usersQueries, CurrentUserData } from '../../../shared/users'
import BandDetailImage from './BandDetailImage'

export const BandDetailContext = React.createContext()

const renderError = error => (
  <ErrorMessage
    error={error}
    message="An unexpected error has occurred loading band details"
  />
)

const renderDetail = () => (
  <div>
    <BandDetailImage />
    <div className="default-paddding band-detail-page__content">
      <BandDetailHeader />
      <Switch>
        <Route
          path="/bands/:id/info"
          render={() => <BandDetailGeneralInfo />}
        />
        <Route
          path="/bands/:id/albums"
          render={() => <BandDetailDiscography />}
        />
        <Route
          path="/bands/:id/similar"
          render={() => <BandDetailSimilarArtists />}
        />
      </Switch>
    </div>
  </div>
)

const BandDetailPage = ({ bandId }) => (
  <div className="default-detail-page band-detail-page">
    <CurrentUserData>
      {({ currentUser }) => (
        <QueryWrapper
          query={LOAD_BAND_DETAILS}
          variables={{ bandId }}
          errorRenderer={renderError}
          loadingRenderer={() => UIUtil.renderLoader('Loading Band Details...')}
        >
          {({ band = {} }) => (
            <QueryWrapper
              query={usersQueries.LOAD_USER_BAND_DATA}
              fetchPolicy="network-only"
              variables={{ userId: currentUser && currentUser.id, bandId }}
            >
              {({ userBandData }) => {
                LocalStorageUtil.storeBandSelectedData(band)
                return (
                  <BandDetailContext.Provider value={{ band, userBandData }}>
                    {renderDetail()}
                  </BandDetailContext.Provider>
                )
              }}
            </QueryWrapper>
          )}
        </QueryWrapper>
      )}
    </CurrentUserData>
  </div>
)

BandDetailPage.propTypes = {
  bandId: PropTypes.string.isRequired,
}

export default BandDetailPage
