import React from 'react'
import { Grid } from 'semantic-ui-react'

import { BandDetailContext } from '../BandDetailPage'
import { DisplayLabel } from '../../../../shared/ux/layout'
import BandDetailMembers from './BandDetailMembers'

const BandDetailGeneralInfo = () => (
  <BandDetailContext.Consumer>
    {({ band }) => (
      <div className="band-detail-page__general-info">
        <Grid>
          <Grid.Column width={8}>
            <DisplayLabel label="Genre: " value={band.genre} />
            <DisplayLabel label="Country: " value={band.country} />
            <DisplayLabel label="Location: " value={band.location} />
            <DisplayLabel label="Formed In: " value={band.formedIn} />
          </Grid.Column>
          <Grid.Column width={8}>
            <DisplayLabel label="Years Active: " value={band.yearsActive} />
            <DisplayLabel label="Status: " value={band.status} />
            <DisplayLabel label="Lyrical Themes: " value={band.lyricalThemes} />
          </Grid.Column>
        </Grid>
        <BandDetailMembers band={band} />
      </div>
    )}
  </BandDetailContext.Consumer>
)

export default BandDetailGeneralInfo
