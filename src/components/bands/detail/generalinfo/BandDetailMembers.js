import React from 'react'
import PropTypes from 'prop-types'
import { Divider } from 'semantic-ui-react'

import { StringUtil } from '../../../../shared/util'
import { ImageCard } from '../../../../shared/ux/layout'

const BandDetailMembers = props => {
  const { band } = props

  const getRoles = ({ roles }) => {
    const roleItem = roles.find(role => role.bandId === band.id)
    return roleItem ? roleItem.role : ''
  }

  const renderMembersCards = () => {
    const { currentMembers = [] } = band
    if (currentMembers.length === 0) {
      return <h4>None</h4>
    }
    return currentMembers.map(artist => (
      <ImageCard
        key={artist.id}
        item={artist}
        imageField="picture"
        to={`/artists/${artist.id}`}
      >
        <div>
          <h3 className="no-margin">{StringUtil.decodeString(artist.name)}</h3>
          <h4 className="no-margin">
            {StringUtil.decodeString(getRoles(artist))}
          </h4>
        </div>
      </ImageCard>
    ))
  }

  return (
    <div className="band-detail-page__band-members">
      <h2>Current Lineup</h2>
      <Divider />
      <ImageCard.Group>{renderMembersCards()}</ImageCard.Group>
    </div>
  )
}

BandDetailMembers.propTypes = {
  band: PropTypes.object.isRequired,
}

export default BandDetailMembers
