import React, { PureComponent } from 'react'
import { get } from 'lodash'

import { BandDetailContext } from '../BandDetailPage'
import BandDetailDiscographyMenu from './BandDetailDiscographyMenu'
import { AlbumsCards, AlbumsList } from '../../../albums'
import { SortUtil } from '../../../../shared/util'

const mergeBandAlbumsWithUserAlbumsData = (userBandData, albums) => {
  const userAlbums = get(userBandData, 'userAlbums', null)
  if (!userAlbums) {
    return [...albums]
  }
  const mergedAlbums = albums.map(album => ({
    ...album,
    userData: {
      id: `${userBandData.userId}-${album.id}`,
      rating: null,
      userId: userBandData.userId,
      albumId: album.id,
    },
  }))
  return mergedAlbums.map(album => {
    const userAlbumFound = userAlbums.find(
      userAlbum => userAlbum.albumId === album.id,
    )
    return {
      ...album,
      userData: userAlbumFound || album.userData,
    }
  })
}

class BandDetailDiscography extends PureComponent {
  constructor(props, context) {
    super(props, context)
    this.state = {
      selectedCategory: 'full',
      selectedSortBy: null,
    }
    this.onCategorySelected = this.onCategorySelected.bind(this)
  }

  onCategorySelected(category) {
    this.setState({
      selectedCategory: category,
    })
  }

  onSortChange = sortBy => {
    this.setState({ selectedSortBy: sortBy })
  }

  renderAlbums(band, userBandData) {
    const { selectedCategory } = this.state
    const albums = mergeBandAlbumsWithUserAlbumsData(
      userBandData,
      band.albums ? band.albums[selectedCategory] : [],
    )
    const { selectedSortBy } = this.state
    const sortedAlbums = SortUtil.sortAlbums(albums, selectedSortBy)
    if (selectedCategory === 'full') {
      return <AlbumsCards albums={sortedAlbums} />
    }
    return <AlbumsList albums={sortedAlbums} />
  }

  render() {
    return (
      <BandDetailContext.Consumer>
        {({ band, userBandData }) => (
          <div className="band-detail-page__discography">
            <BandDetailDiscographyMenu
              onCategorySelected={this.onCategorySelected}
              onSortBy={this.onSortChange}
            />
            {this.renderAlbums(band, userBandData)}
          </div>
        )}
      </BandDetailContext.Consumer>
    )
  }
}

export default BandDetailDiscography
