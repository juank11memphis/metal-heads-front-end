import React, { PureComponent } from 'react'
import { Menu } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import { BurguerDropDown } from '../../../../shared/ux/burguer-dropdown'
import { SortByOptions } from '../../../../shared/ux/sort'

class BandDetailDiscographyMenu extends PureComponent {
  constructor(props, context) {
    super(props, context)
    this.state = { activeItem: 'full' }
    this.menuOptions = [
      { value: 'full', text: 'Full-length' },
      { value: 'live', text: 'Live' },
      { value: 'video', text: 'Video' },
      { value: 'compilations', text: 'Compilations' },
      { value: 'singles', text: 'Singles' },
      { value: 'other', text: 'Other' },
    ]
    this.handleItemClick = this.handleItemClick.bind(this)
  }

  handleItemClick(event, { name }) {
    const { onCategorySelected } = this.props
    this.setState({ activeItem: name })
    onCategorySelected(name)
  }

  renderMobileMenu() {
    const { onSortBy } = this.props
    const { activeItem } = this.state
    return (
      <div className="hbox show-on-mobile">
        <BurguerDropDown
          showSelected
          options={this.menuOptions}
          activeItem={activeItem}
          onItemClick={menuItem =>
            this.handleItemClick(null, { name: menuItem.value })
          }
        />
        <SortByOptions onChange={onSortBy} />
      </div>
    )
  }

  renderDesktopMenu() {
    const { onSortBy } = this.props
    const { activeItem } = this.state
    return (
      <div className="hbox hide-on-mobile">
        <Menu inverted compact>
          {this.menuOptions.map(menuItem => (
            <Menu.Item
              key={menuItem.value}
              name={menuItem.value}
              active={activeItem === menuItem.value}
              onClick={this.handleItemClick}
            >
              {menuItem.text}
            </Menu.Item>
          ))}
        </Menu>
        <SortByOptions onChange={onSortBy} />
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.renderDesktopMenu()}
        {this.renderMobileMenu()}
      </div>
    )
  }
}

BandDetailDiscographyMenu.propTypes = {
  onCategorySelected: PropTypes.func.isRequired,
  onSortBy: PropTypes.func.isRequired,
}

export default BandDetailDiscographyMenu
