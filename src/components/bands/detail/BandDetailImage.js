import React from 'react'

import { BandDetailContext } from './BandDetailPage'

const BandDetailImage = () => (
  <BandDetailContext.Consumer>
    {({ band }) => (
      <div
        style={{ backgroundImage: `url("${band.picture}")` }}
        className="band-detail-page__band-image"
      />
    )}
  </BandDetailContext.Consumer>
)

export default BandDetailImage
