import React from 'react'
import { Mutation } from 'react-apollo'

import { CurrentUserData, usersMutations } from '../../../../shared/users'
import { SaveForLaterButton } from '../../../../shared/ux/save-for-later-button'
import { BandDetailContext } from '../BandDetailPage'

const shouldShowSaveForLaterButton = (isAuthenticated, userBandData) => {
  if (isAuthenticated && userBandData && !userBandData.rating) {
    return true
  }
  return false
}

const renderButton = (band, userBandData) => (
  <Mutation mutation={usersMutations.SAVE_BAND_FOR_LATER}>
    {saveBandForLater => (
      <Mutation mutation={usersMutations.REMOVE_FROM_SAVED_BANDS}>
        {removeFromSavedBands => (
          <SaveForLaterButton
            active={userBandData ? userBandData.saved : false}
            onClick={active =>
              active
                ? saveBandForLater({ variables: { bandId: band.id } })
                : removeFromSavedBands({ variables: { bandId: band.id } })
            }
          />
        )}
      </Mutation>
    )}
  </Mutation>
)

const SaveBandForLaterButton = () => (
  <CurrentUserData>
    {({ isAuthenticated }) => (
      <BandDetailContext.Consumer>
        {({ band, userBandData }) =>
          band && shouldShowSaveForLaterButton(isAuthenticated, userBandData)
            ? renderButton(band, userBandData)
            : null
        }
      </BandDetailContext.Consumer>
    )}
  </CurrentUserData>
)
export default SaveBandForLaterButton
