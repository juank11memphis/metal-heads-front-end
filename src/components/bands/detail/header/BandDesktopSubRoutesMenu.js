import React from 'react'
import { Menu } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import { BandDetailContext } from '../BandDetailPage'

const getActiveSubRouteFromUrl = context => {
  const { router } = context
  const { route } = router
  const { location } = route
  const routeUrl = location.pathname
  return routeUrl.substring(routeUrl.lastIndexOf('/') + 1, routeUrl.length)
}

const doHistoryPush = (context, path) => {
  const { router } = context
  const { history } = router
  history.push(path)
}

const BandDesktopSubRoutesMenu = ({ menuOptions }, context) => (
  <BandDetailContext.Consumer>
    {({ band }) => {
      const activeItem = getActiveSubRouteFromUrl(context)
      return (
        <Menu inverted compact className="hide-on-mobile">
          {menuOptions.map(menuItem => (
            <Menu.Item
              key={menuItem.value}
              name={menuItem.value}
              active={activeItem === menuItem.value}
              onClick={(_, { name }) =>
                doHistoryPush(context, `/bands/${band.id}/${name}`)
              }
            >
              {menuItem.text}
            </Menu.Item>
          ))}
        </Menu>
      )
    }}
  </BandDetailContext.Consumer>
)

BandDesktopSubRoutesMenu.propTypes = {
  menuOptions: PropTypes.array.isRequired,
}

BandDesktopSubRoutesMenu.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default BandDesktopSubRoutesMenu
