import React from 'react'
import PropTypes from 'prop-types'

import { BandDetailContext } from '../BandDetailPage'
import { BurguerDropDown } from '../../../../shared/ux/burguer-dropdown'

const BandMobileSubRoutesMenu = ({ menuOptions }, context) => (
  <BandDetailContext.Consumer>
    {({ band }) => (
      <BurguerDropDown
        className="show-on-mobile"
        options={menuOptions}
        onItemClick={menuItem =>
          context.router.history.push(`/bands/${band.id}/${menuItem.value}`)
        }
      />
    )}
  </BandDetailContext.Consumer>
)

BandMobileSubRoutesMenu.propTypes = {
  menuOptions: PropTypes.array.isRequired,
}

BandMobileSubRoutesMenu.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default BandMobileSubRoutesMenu
