import React from 'react'

import SaveBandForLaterButton from './SaveBandForLaterButton'
import BandSliderRating from './BandSliderRating'
import BandGlobalRating from './BandGlobalRating'

const BandDetailActionButtons = () => (
  <div className="band-detail-page__header__action-buttons">
    <SaveBandForLaterButton />
    <BandSliderRating />
    <BandGlobalRating />
  </div>
)

export default BandDetailActionButtons
