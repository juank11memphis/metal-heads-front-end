import React from 'react'

import { GlobalRating } from '../../../../shared/ux/ratings'
import { BandDetailContext } from '../BandDetailPage'

const BandGlobalRating = () => (
  <BandDetailContext.Consumer>
    {({ band }) => (
      <GlobalRating
        value={band.globalRating}
        userRatingsValue={band.ratingsCount}
      />
    )}
  </BandDetailContext.Consumer>
)

export default BandGlobalRating
