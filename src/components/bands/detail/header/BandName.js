import React from 'react'
import PropTypes from 'prop-types'

import { BandDetailContext } from '../BandDetailPage'

const BandName = (_, context) => (
  <BandDetailContext.Consumer>
    {({ band }) => (
      <div
        className="band-detail-page__header__name"
        role="button"
        tabIndex={0}
        onClick={() => context.router.history.push(`/bands/${band.id}/info`)}
      >
        <h1>{band.name}</h1>
      </div>
    )}
  </BandDetailContext.Consumer>
)

BandName.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default BandName
