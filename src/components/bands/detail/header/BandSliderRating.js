import React from 'react'
import { Mutation } from 'react-apollo'

import { SliderRating } from '../../../../shared/ux/ratings'
import { BandDetailContext } from '../BandDetailPage'
import { usersMutations } from '../../../../shared/users'

const getUserRating = userBandData =>
  userBandData && userBandData.rating ? userBandData.rating : 0

const renderSlider = (band, userBandData) => (
  <Mutation mutation={usersMutations.RATE_BAND}>
    {rateBand => (
      <Mutation mutation={usersMutations.REMOVE_BAND_RATING}>
        {removeBandRating => (
          <SliderRating
            value={getUserRating(userBandData)}
            onChange={rating =>
              rateBand({ variables: { bandId: band.id, rating } })
            }
            onRemove={() =>
              removeBandRating({ variables: { bandId: band.id } })
            }
          />
        )}
      </Mutation>
    )}
  </Mutation>
)

const BandSliderRating = () => (
  <BandDetailContext.Consumer>
    {({ band, userBandData }) => renderSlider(band, userBandData)}
  </BandDetailContext.Consumer>
)

export default BandSliderRating
