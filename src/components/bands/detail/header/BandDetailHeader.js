import React from 'react'
import { Divider } from 'semantic-ui-react'

import BandName from './BandName'
import BandDesktopSubRoutesMenu from './BandDesktopSubRoutesMenu'
import BandMobileSubRoutesMenu from './BandMobileSubRoutesMenu'
import BandDetailActionButtons from './BandDetailActionButtons'

const menuOptions = [
  { value: 'info', text: 'General Info' },
  { value: 'albums', text: 'Discography' },
  { value: 'similar', text: 'Similar Artists' },
]

const BandDetailHeader = () => (
  <div className="band-detail-page__header">
    <div className="hbox">
      <BandName />
      <BandDesktopSubRoutesMenu menuOptions={menuOptions} />
      <BandMobileSubRoutesMenu menuOptions={menuOptions} />
    </div>
    <Divider />
    <BandDetailActionButtons />
    <Divider />
  </div>
)

export default BandDetailHeader
