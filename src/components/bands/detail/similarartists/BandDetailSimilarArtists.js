import React from 'react'
import { Query } from 'react-apollo'

import { BandDetailContext } from '../BandDetailPage'
import { ErrorMessage } from '../../../../shared/ux/messages'
import { ImageCard } from '../../../../shared/ux/layout'
import { LOAD_SIMILAR_BANDS } from '../../bandsQueries'
import { StringUtil } from '../../../../shared/util'

const renderError = error => (
  <ErrorMessage
    error={error}
    message="An unexpected error has occurred loading similar bands"
  />
)

const renderBandsCards = similarBands =>
  similarBands.map(band => (
    <ImageCard
      key={band.id}
      item={band}
      imageField="picture"
      to={`/bands/${band.id}/info`}
    >
      <div className="hbox">
        <h4 className="no-margin">{StringUtil.decodeString(band.name)}</h4>
        <h4 className="global-rating no-margin">
          {band.globalRating ? `${band.globalRating}%` : 'Not Rated'}
        </h4>
      </div>
    </ImageCard>
  ))

const renderSimilarArtists = ({ similarBands = [] }) => (
  <div className="band-detail-page__similar-bands">
    <ImageCard.Group>{renderBandsCards(similarBands)}</ImageCard.Group>
  </div>
)

const BandDetailSimilarArtists = () => (
  <BandDetailContext.Consumer>
    {({ band }) => (
      <Query query={LOAD_SIMILAR_BANDS} variables={{ bandId: band.id }}>
        {({ data, error }) =>
          error ? renderError(error) : renderSimilarArtists(data)
        }
      </Query>
    )}
  </BandDetailContext.Consumer>
)

export default BandDetailSimilarArtists
