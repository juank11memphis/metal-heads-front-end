import gql from 'graphql-tag'
import * as bandsFragments from './bandsFragments'

export const LOAD_MOST_RATED_BANDS = gql`
  query loadMostRatedBands {
    bands: loadMostRatedBands {
      ...BandInfoShort
    }
  }
  ${bandsFragments.bandInfoShort}
`

export const LOAD_SIMILAR_BANDS = gql`
  query loadSimilarBands($bandId: String) {
    similarBands: loadSimilarBands(bandId: $bandId) {
      ...BandInfoShort
    }
  }
  ${bandsFragments.bandInfoShort}
`

export const LOAD_BAND_DETAILS = gql`
  query loadBandDetails($bandId: String) {
    band: loadBandDetails(bandId: $bandId) {
      ...BandInfoFull
    }
  }
  ${bandsFragments.bandInfoFull}
`
