import gql from 'graphql-tag'

import { albumInfoShort } from '../albums/albumsFragments'
import { artistInfoShort } from '../artists/artistsFragments'

export const bandSearchInfo = gql`
  fragment BandSearchInfo on BandSearch {
    id
    name
    genre
    country
  }
`

export const bandInfoShort = gql`
  fragment BandInfoShort on Band {
    id
    name
    picture
    country
    genre
    globalRating
    ratingsCount
    status
    lyricalThemes
    location
    formedIn
    yearsActive
  }
`

export const bandAlbums = gql`
  fragment BandAlbums on Band {
    albums {
      compilations {
        ...AlbumInfoShort
      }
      full {
        ...AlbumInfoShort
      }
      live {
        ...AlbumInfoShort
      }
      other {
        ...AlbumInfoShort
      }
      singles {
        ...AlbumInfoShort
      }
      video {
        ...AlbumInfoShort
      }
    }
  }
  ${albumInfoShort}
`

export const bandCurrentMembers = gql`
  fragment BandCurrentMembers on Band {
    currentMembers {
      ...ArtistInfoShort
    }
  }
  ${artistInfoShort}
`

export const bandInfoFull = gql`
  fragment BandInfoFull on Band {
    ...BandInfoShort
    ...BandAlbums
    ...BandCurrentMembers
  }
  ${bandInfoShort}
  ${bandAlbums}
  ${bandCurrentMembers}
`
