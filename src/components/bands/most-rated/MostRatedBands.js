import React, { Component } from 'react'
import Slider from 'react-slick'
import { Grid, Divider } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { get } from 'lodash'
import { Query } from 'react-apollo'

import { LOAD_MOST_RATED_BANDS } from '../bandsQueries'
import { ErrorMessage } from '../../../shared/ux/messages'

export default class MostRatedBands extends Component {
  shouldComponentUpdate(nextProps) {
    const currentBands = get(this.props, 'data.bands', [])
    const nextBands = get(nextProps, 'data.bands', [])
    return nextBands !== currentBands
  }

  static renderBandCarouselSlide(index, band) {
    const slideStyle = {
      backgroundImage: `url(${band.picture})`,
    }
    return (
      <Link key={index} to={`/bands/${band.id}/info`}>
        <div
          className="home-page__most-rated-bands__slider__slide"
          style={slideStyle}
        >
          <Grid
            className="home-page__most-rated-bands__slider__slide__info"
            verticalAlign="bottom"
            textAlign="center"
          >
            <Grid.Column className="no-padding">
              <h2 className="no-margin">
                {index + 1}.{` ${band.name}`}
              </h2>
              <h1 className="no-margin home-page__most-rated-bands__slider__slide__info__global-rating">
                {band.globalRating}%
              </h1>
              <div className="user-ratings">
                <span className="bold-text">User Ratings:</span>{' '}
                {band.ratingsCount}
              </div>
            </Grid.Column>
          </Grid>
        </div>
      </Link>
    )
  }

  static renderCarousel({ bands }) {
    if (!bands || bands.length === 0) {
      return null
    }
    const carouselOptions = {
      className: 'home-page__most-rated-bands__slider',
      dots: false,
      infinite: true,
      adaptiveHeight: false,
      draggable: false,
      swipe: false,
      speed: 500,
      autoplay: false,
      autoplaySpeed: 4000,
    }
    return (
      <Grid className="home-page__most-rated-bands__slider-grid">
        <Grid.Row only="computer">
          <Slider {...carouselOptions} slidesToShow={2} slidesToScroll={2}>
            {bands.map((band, index) =>
              MostRatedBands.renderBandCarouselSlide(index, band),
            )}
          </Slider>
        </Grid.Row>
        <Grid.Row only="tablet mobile">
          <Slider {...carouselOptions} slidesToShow={1} slidesToScroll={1}>
            {bands.map((band, index) =>
              MostRatedBands.renderBandCarouselSlide(index, band),
            )}
          </Slider>
        </Grid.Row>
      </Grid>
    )
  }

  renderError() {
    const { error } = this.props
    return (
      <ErrorMessage
        error={error}
        message="An unexpected error has occurred loading most rated bands"
      />
    )
  }

  render() {
    return (
      <div>
        <h3>Most Rated Bands</h3>
        <Divider />
        <div className="home-page__most-rated-bands">
          <Query query={LOAD_MOST_RATED_BANDS} fetchPolicy="network-only">
            {({ data, error }) =>
              error
                ? this.renderError(error)
                : MostRatedBands.renderCarousel(data)
            }
          </Query>
        </div>
      </div>
    )
  }
}
