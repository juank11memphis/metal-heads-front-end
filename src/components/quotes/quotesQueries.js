import gql from 'graphql-tag'
import { graphql } from 'react-apollo'

const LOAD_ACTIVE_QUOTE = gql`
  query loadActiveQuote {
    activeQuote: loadActiveQuote {
      text
      author
    }
  }
`
export const withActiveQuote = graphql(LOAD_ACTIVE_QUOTE)
