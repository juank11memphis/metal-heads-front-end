import Quote from './Quote'
import * as quotesMutations from './quotesMutations'

export { Quote, quotesMutations }
