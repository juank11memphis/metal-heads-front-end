import gql from 'graphql-tag'

export const CREATE_QUOTE = gql`
  mutation createQuote($quote: QuoteInput) {
    quote: createQuote(quote: $quote) {
      text
      author
    }
  }
`
