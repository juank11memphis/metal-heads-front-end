import React from 'react'
import PropTypes from 'prop-types'

const Quote = ({ className, data: { activeQuote, error } }) => {
  if (!activeQuote || error) {
    return null
  }
  return (
    <div className={className}>
      <h2 style={{ textAlign: 'right' }}>{activeQuote.text}</h2>
      <h4 style={{ textAlign: 'right' }}>-{activeQuote.author}</h4>
    </div>
  )
}

Quote.defaultProps = {
  className: null,
}

Quote.propTypes = {
  data: PropTypes.shape({
    activeQuote: PropTypes.shape({
      text: PropTypes.string,
      author: PropTypes.string,
    }),
    error: PropTypes.object,
  }).isRequired,
  className: PropTypes.string,
}

export default Quote
