import React from 'react'

import { AlbumDetailContext } from './AlbumDetailPage'

const AlbumDetailImage = () => (
  <AlbumDetailContext.Consumer>
    {({ album }) => (
      <div
        style={{ backgroundImage: `url("${album.picture}")` }}
        className="album-detail-page__image"
      />
    )}
  </AlbumDetailContext.Consumer>
)

export default AlbumDetailImage
