import React from 'react'

import AlbumSliderRating from '../ux/AlbumSliderRating'
import AlbumGlobalRating from './AlbumGlobalRating'
import { AlbumDetailContext } from '../AlbumDetailPage'

const AlbumDetailRatings = () => (
  <AlbumDetailContext.Consumer>
    {({ userAlbumData }) => (
      <div className="hbox-space-around">
        <AlbumSliderRating userAlbumData={userAlbumData} />
        <AlbumGlobalRating />
      </div>
    )}
  </AlbumDetailContext.Consumer>
)

export default AlbumDetailRatings
