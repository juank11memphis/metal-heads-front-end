import React from 'react'

import { GlobalRating } from '../../../../shared/ux/ratings'
import { AlbumDetailContext } from '../AlbumDetailPage'

const AlbumGlobalRating = () => (
  <AlbumDetailContext.Consumer>
    {({ album }) => (
      <GlobalRating
        value={album.globalRating}
        userRatingsValue={album.ratingsCount}
      />
    )}
  </AlbumDetailContext.Consumer>
)

export default AlbumGlobalRating
