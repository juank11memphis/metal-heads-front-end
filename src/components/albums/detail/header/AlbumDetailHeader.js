import React from 'react'
import { Link } from 'react-router-dom'
import { Divider } from 'semantic-ui-react'

import { AlbumDetailContext } from '../AlbumDetailPage'
import { PlayButtons } from '../../../../shared/ux/play-buttons'
import { PLAY_ALBUM, CREATE_PLAY_ALBUM_ACTIVITY } from '../../albumsMutations'
import AlbumDetailRatings from './AlbumDetailRatings'

const renderPlayButtons = (album, className = '') => (
  <PlayButtons
    className={className}
    itemId={album.id}
    youtubeSearchText={`${album.band.name} ${album.name}+full+album`}
    playOnSpotifyMutation={PLAY_ALBUM}
    createPlayAlbumActivityMutation={CREATE_PLAY_ALBUM_ACTIVITY}
  />
)

const AlbumDetailHeader = () => (
  <AlbumDetailContext.Consumer>
    {({ album }) => (
      <div className="album-detail-page__header">
        <div className="hbox">
          <div className="album-detail-page__header__info">
            <Link to={`/bands/${album.band.id}/albums`}>
              <h2 className="no-margin">{album.band.name}</h2>
            </Link>
            <h3>&gt;</h3>
            <Link to={`/albums/${album.id}`}>
              <h2 className="no-margin">{album.name}</h2>
            </Link>
          </div>
          {renderPlayButtons(album, 'hide-on-mobile')}
        </div>
        {renderPlayButtons(album, 'show-on-mobile')}
        <Divider />
        <AlbumDetailRatings />
        <Divider />
      </div>
    )}
  </AlbumDetailContext.Consumer>
)

export default AlbumDetailHeader
