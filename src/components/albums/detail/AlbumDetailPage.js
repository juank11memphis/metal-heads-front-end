import React, { PureComponent } from 'react'
import { Menu } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import AlbumDetailGeneralInfo from './general-info/AlbumDetailGeneralInfo'
import AlbumDetailSongs from './songs/AlbumDetailSongs'
import AlbumDetailLineUp from './lineup/AlbumDetailLineUp'
import AlbumDetailHeader from './header/AlbumDetailHeader'
import AlbumDetailImage from './AlbumDetailImage'
import { UIUtil } from '../../../shared/util'
import { ErrorMessage } from '../../../shared/ux/messages'
import { LOAD_ALBUM_DETAILS } from '../albumsQueries'
import { QueryWrapper } from '../../../shared/ux/graphql'
import { usersQueries, CurrentUserData } from '../../../shared/users'

export const AlbumDetailContext = React.createContext()

const renderError = error => (
  <ErrorMessage
    error={error}
    message="An unexpected error has occurred loading album details"
  />
)

class AlbumDetailPage extends PureComponent {
  state = {
    selectedMenuOption: 'songs',
  }

  onMenuOptionSelected = (event, { name }) => {
    this.setState({ selectedMenuOption: name })
  }

  renderMenu() {
    const { selectedMenuOption } = this.state
    return (
      <Menu inverted compact>
        <Menu.Item
          name="songs"
          active={selectedMenuOption === 'songs'}
          onClick={this.onMenuOptionSelected}
        >
          Songs
        </Menu.Item>
        <Menu.Item
          name="lineup"
          active={selectedMenuOption === 'lineup'}
          onClick={this.onMenuOptionSelected}
        >
          Line Up
        </Menu.Item>
      </Menu>
    )
  }

  renderDetail() {
    const { selectedMenuOption } = this.state
    return (
      <div>
        <AlbumDetailImage />
        <AlbumDetailHeader />
        <AlbumDetailGeneralInfo />
        {this.renderMenu()}
        <div className="default-margin-top-bottom">
          {selectedMenuOption === 'songs' && <AlbumDetailSongs />}
          {selectedMenuOption === 'lineup' && <AlbumDetailLineUp />}
        </div>
      </div>
    )
  }

  render() {
    const { albumId } = this.props
    return (
      <div className="default-detail-page album-detail-page">
        <CurrentUserData>
          {({ currentUser }) => (
            <QueryWrapper
              query={LOAD_ALBUM_DETAILS}
              variables={{ albumId }}
              errorRenderer={renderError}
              loadingRenderer={() =>
                UIUtil.renderLoader('Loading Album Details...')
              }
            >
              {({ album }) => (
                <QueryWrapper
                  fetchPolicy="network-only"
                  query={usersQueries.LOAD_USER_ALBUM_DATA}
                  variables={{ userId: currentUser && currentUser.id, albumId }}
                >
                  {({ userAlbumData }) => (
                    <AlbumDetailContext.Provider
                      value={{ album, userAlbumData }}
                    >
                      {this.renderDetail()}
                    </AlbumDetailContext.Provider>
                  )}
                </QueryWrapper>
              )}
            </QueryWrapper>
          )}
        </CurrentUserData>
      </div>
    )
  }
}

AlbumDetailPage.propTypes = {
  albumId: PropTypes.string.isRequired,
}

export default AlbumDetailPage
