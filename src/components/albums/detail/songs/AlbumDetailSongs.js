import React from 'react'
import { Grid, Image } from 'semantic-ui-react'

import musixMatchLogo from '../../../../styles/img/musixmatch.png'

import { AlbumDetailContext } from '../AlbumDetailPage'
import { StringUtil } from '../../../../shared/util'

const onOpenLyricsClick = (band, song) => {
  const bandName = StringUtil.cleanAndJoin(band.name)
  const songName = StringUtil.cleanAndJoin(song.name)
  const musixMatchUrl = `https://www.musixmatch.com/lyrics/${bandName}/${songName}`
  const win = window.open(musixMatchUrl, '_blank')
  win.focus()
}

const renderSongRow = (band, song, index) => (
  <Grid.Row key={index} className="no-padding">
    <Grid.Column width={8}>
      {index + 1}.{` ${song.name}`}
    </Grid.Column>
    <Grid.Column textAlign="right">
      <div className="hbox-raw vertically-center-aligned justify-content-end">
        <Image
          className="icon"
          src={musixMatchLogo}
          width="30"
          height="30"
          onClick={() => onOpenLyricsClick(band, song)}
          data-id="musixmatch-icon"
        />
        <span className="default-margin-left">{song.length}</span>
      </div>
    </Grid.Column>
  </Grid.Row>
)

const AlbumDetailSongs = () => (
  <AlbumDetailContext.Consumer>
    {({ album }) => (
      <Grid
        className="no-margin album-detail-page__songs-grid"
        columns="equal"
        divided="vertically"
      >
        {album.songs.map((song, index) =>
          renderSongRow(album.band, song, index),
        )}
      </Grid>
    )}
  </AlbumDetailContext.Consumer>
)

export default AlbumDetailSongs
