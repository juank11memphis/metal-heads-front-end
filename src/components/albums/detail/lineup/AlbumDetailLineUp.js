import React from 'react'
import { Link } from 'react-router-dom'
import { Divider } from 'semantic-ui-react'

import { AlbumDetailContext } from '../AlbumDetailPage'
import { StringUtil } from '../../../../shared/util'

const renderArtistRow = (artist, index) => {
  const path = `/artists/${artist.id}`

  return (
    <div key={index} className="album-detail-page__line-up__item">
      <Link to={path}>
        <h4>{StringUtil.decodeString(artist.name)}</h4>
      </Link>
      {StringUtil.decodeString(artist.roles)}
      <Divider />
    </div>
  )
}

const AlbumDetailLineUp = () => (
  <AlbumDetailContext.Consumer>
    {({ album }) => (
      <div className="album-detail-page__line-up">
        {album.lineUp.map((artist, index) => renderArtistRow(artist, index))}
      </div>
    )}
  </AlbumDetailContext.Consumer>
)

export default AlbumDetailLineUp
