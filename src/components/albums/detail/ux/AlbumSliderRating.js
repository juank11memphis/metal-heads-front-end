import React from 'react'
import { Mutation } from 'react-apollo'
import PropTypes from 'prop-types'

import { SliderRating } from '../../../../shared/ux/ratings'
import { usersMutations } from '../../../../shared/users'

const getUserRating = userAlbumData =>
  userAlbumData && userAlbumData.rating ? userAlbumData.rating : 0

const AlbumSliderRating = ({ userAlbumData, refetchQueries }) =>
  userAlbumData ? (
    <Mutation
      mutation={usersMutations.RATE_ALBUM}
      refetchQueries={refetchQueries}
    >
      {rateAlbum => (
        <Mutation mutation={usersMutations.REMOVE_ALBUM_RATING}>
          {removeAlbumRating => (
            <SliderRating
              value={getUserRating(userAlbumData)}
              onChange={rating =>
                rateAlbum({
                  variables: { albumId: userAlbumData.albumId, rating },
                })
              }
              onRemove={() =>
                removeAlbumRating({
                  variables: { albumId: userAlbumData.albumId },
                })
              }
            />
          )}
        </Mutation>
      )}
    </Mutation>
  ) : null

AlbumSliderRating.defaultProps = {
  userAlbumData: null,
  refetchQueries: null,
}

AlbumSliderRating.propTypes = {
  userAlbumData: PropTypes.object,
  refetchQueries: PropTypes.array,
}

export default AlbumSliderRating
