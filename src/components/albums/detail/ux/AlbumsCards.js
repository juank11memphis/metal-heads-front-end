import React from 'react'
import PropTypes from 'prop-types'

import { ImageCard } from '../../../../shared/ux/layout'
import AlbumCard from './AlbumCard'

const AlbumsCards = ({ albums }) => (
  <ImageCard.Group>
    {albums.map(album => (
      <AlbumCard key={album.id} album={album} />
    ))}
  </ImageCard.Group>
)

AlbumsCards.propTypes = {
  albums: PropTypes.array.isRequired,
}

export default AlbumsCards
