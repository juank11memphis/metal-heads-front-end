import React, { Component } from 'react'
import { Divider } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { get } from 'lodash'

import { PLAY_ALBUM, CREATE_PLAY_ALBUM_ACTIVITY } from '../../albumsMutations'
import { PlayButtons } from '../../../../shared/ux/play-buttons'
import { ImageCard } from '../../../../shared/ux/layout'
import { CurrentUserData, usersQueries } from '../../../../shared/users'
import AlbumSliderRating from './AlbumSliderRating'

const shouldRenderAlbumType = type => {
  const types = [
    'Video',
    'Split video',
    'Compilation',
    'Boxed set',
    'Demo',
    'EP',
    'Split',
  ]
  return types.includes(type)
}

class AlbumCard extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = { playButtonsVisible: false }
  }

  onImageCardMouseOver = () => {
    this.setState({ playButtonsVisible: true })
  }

  onImageCardMouseOut = ({ relatedTarget }) => {
    const classList = get(relatedTarget, 'classList', {})
    const isOnSpotifyButton = classList.contains
      ? classList.contains('spotify')
      : false
    const isOnYoutubeButton = classList.contains
      ? classList.contains('youtube')
      : false
    if (isOnSpotifyButton || isOnYoutubeButton) {
      return
    }
    this.setState({ playButtonsVisible: false })
  }

  static getCardStyle(isAuthenticated) {
    return isAuthenticated ? { height: 390 } : { height: 330 }
  }

  static getRefetchQueries(album) {
    const { band, userData = {} } = album
    return [
      {
        query: usersQueries.LOAD_USER_BAND_DATA,
        variables: { userId: userData.userId, bandId: band.id },
      },
    ]
  }

  renderAlbumDetail() {
    const { album } = this.props
    return (
      <div className="album-card__detail-container">
        <div className="hbox">
          <h4 className="no-margin">{album.name}</h4>
          <h4 className="no-margin global-rating">
            {album.globalRating ? `${album.globalRating}%` : 'Not rated'}
          </h4>
        </div>
        <h4 className="no-margin">{album.year}</h4>
        {shouldRenderAlbumType(album.type) && (
          <h5 className="no-margin">{album.type}</h5>
        )}
      </div>
    )
  }

  render() {
    const { album } = this.props
    const { playButtonsVisible } = this.state
    const youtubeSearchText = `${album.band.name} ${album.name}+full+album`
    return (
      <CurrentUserData>
        {({ isAuthenticated }) => (
          <ImageCard
            item={album}
            imageField="picture"
            to={`/albums/${album.id}`}
            cardStyle={AlbumCard.getCardStyle(isAuthenticated)}
            onImageOver={this.onImageCardMouseOver}
            onImageOut={this.onImageCardMouseOut}
          >
            <div className="album-card">
              <PlayButtons
                itemId={album.id}
                visible={playButtonsVisible}
                youtubeSearchText={youtubeSearchText}
                playOnSpotifyMutation={PLAY_ALBUM}
                createPlayAlbumActivityMutation={CREATE_PLAY_ALBUM_ACTIVITY}
              />
              <div className="vbox" style={{ flex: 1 }}>
                {this.renderAlbumDetail()}
                <div style={{ flex: 1 }} />
                <div className="text-align-center">
                  <AlbumSliderRating
                    userAlbumData={album.userData}
                    refetchQueries={AlbumCard.getRefetchQueries(album)}
                  />
                </div>
                <Divider className="no-margin" />
              </div>
            </div>
          </ImageCard>
        )}
      </CurrentUserData>
    )
  }
}

AlbumCard.propTypes = {
  album: PropTypes.object.isRequired,
}

export default AlbumCard
