import React from 'react'
import PropTypes from 'prop-types'
import { Divider } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

import { PLAY_ALBUM, CREATE_PLAY_ALBUM_ACTIVITY } from '../../albumsMutations'
import { PlayButtons } from '../../../../shared/ux/play-buttons'
import AlbumSliderRating from './AlbumSliderRating'
import { usersQueries } from '../../../../shared/users'

const shouldRenderAlbumType = type => {
  const types = [
    'Video',
    'Split video',
    'Compilation',
    'Boxed set',
    'Demo',
    'EP',
    'Split',
  ]
  return types.includes(type)
}

const renderPlayButtons = (album, props, className = '') => (
  <PlayButtons
    className={className}
    itemId={album.id}
    youtubeSearchText={`${album.band.name} ${album.name}+full+album`}
    playOnSpotifyMutation={PLAY_ALBUM}
    createPlayAlbumActivityMutation={CREATE_PLAY_ALBUM_ACTIVITY}
  />
)

const getRefetchQueries = album => {
  const { band, userData = {} } = album
  return [
    {
      query: usersQueries.LOAD_USER_BAND_DATA,
      variables: { userId: userData.userId, bandId: band.id },
    },
  ]
}

const AlbumsList = props => {
  const { albums } = props
  return (
    <div className="albums-list">
      {albums.map(album => (
        <div className="albums-list__item" key={album.id}>
          <div className="hbox">
            <div className="albums-list__item__info">
              <Link to={`/albums/${album.id}`}>
                <h4>{album.name}</h4>
              </Link>
              <p className="no-margin">{album.year}</p>
              <p className="no-margin">
                {shouldRenderAlbumType(album.type) && album.type}
              </p>
            </div>
            {renderPlayButtons(album, props, 'hide-on-mobile')}
            <div className="text-align-center">
              <AlbumSliderRating
                userAlbumData={album.userData}
                refetchQueries={getRefetchQueries(album)}
              />
            </div>
            <h3 className="no-margin global-rating">
              {album.globalRating ? `${album.globalRating}%` : 'Not rated'}
            </h3>
          </div>
          {renderPlayButtons(album, props, 'show-on-mobile')}
          <Divider />
        </div>
      ))}
    </div>
  )
}

AlbumsList.propTypes = {
  albums: PropTypes.array.isRequired,
}

export default AlbumsList
