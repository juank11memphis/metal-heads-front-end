import React from 'react'
import { Grid } from 'semantic-ui-react'

import { AlbumDetailContext } from '../AlbumDetailPage'
import { DisplayLabel } from '../../../../shared/ux/layout'

const AlbumDetailGeneralInfo = () => (
  <AlbumDetailContext.Consumer>
    {({ album }) => (
      <div>
        <Grid className="no-margin album-detail-page__general-info">
          <Grid.Column width={8} className="no-left-right">
            <DisplayLabel label="Type: " value={album.type} />
            <DisplayLabel label="Release Date: " value={album.releaseDate} />
            <DisplayLabel label="Catalog ID: " value={album.catalogId} />
          </Grid.Column>
          <Grid.Column width={8} className="no-left-right">
            <DisplayLabel label="Label: " value={album.label} />
            <DisplayLabel label="Format: " value={album.format} />
          </Grid.Column>
        </Grid>
      </div>
    )}
  </AlbumDetailContext.Consumer>
)

export default AlbumDetailGeneralInfo
