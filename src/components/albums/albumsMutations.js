import gql from 'graphql-tag'

export const PLAY_ALBUM = gql`
  mutation playAlbum($deviceId: String, $id: String) {
    success: playAlbum(deviceId: $deviceId, albumId: $id)
  }
`

export const CREATE_PLAY_ALBUM_ACTIVITY = gql`
  mutation createPlayAlbumActivity($albumId: String, $target: String) {
    sucess: createPlayAlbumActivity(albumId: $albumId, target: $target)
  }
`
