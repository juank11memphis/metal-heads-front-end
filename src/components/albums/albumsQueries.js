import gql from 'graphql-tag'

import * as albumsFragments from './albumsFragments'

export const LOAD_ALBUM_DETAILS = gql`
  query loadAlbumDetails($albumId: String) {
    album: loadAlbumDetails(albumId: $albumId) {
      ...AlbumInfoFull
    }
  }
  ${albumsFragments.albumInfoFull}
`
