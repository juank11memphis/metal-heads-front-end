import gql from 'graphql-tag'

export const albumSearchInfo = gql`
  fragment AlbumSearchInfo on AlbumSearch {
    id
    name
    year
    type
  }
`

export const albumInfoShort = gql`
  fragment AlbumInfoShort on Album {
    id
    name
    picture
    type
    year
    globalRating
    ratingsCount
    band {
      id
      name
    }
  }
`

export const albumUserData = gql`
  fragment AlbumUserData on Album {
    userData {
      id
      rating
    }
  }
`

export const albumInfoFull = gql`
  fragment AlbumInfoFull on Album {
    ...AlbumInfoShort
    label
    catalogId
    format
    releaseDate
    songs {
      name
      length
    }
    lineUp {
      id
      name
      roles
    }
  }
  ${albumInfoShort}
`
