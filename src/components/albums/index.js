import AlbumDetailPage from './detail/AlbumDetailPage'
import * as albumsFragments from './albumsFragments'
import * as albumsQueries from './albumsQueries'
import * as albumsMutations from './albumsMutations'
import AlbumsList from './detail/ux/AlbumsList'
import AlbumsCards from './detail/ux/AlbumsCards'

export {
  AlbumDetailPage,
  albumsFragments,
  albumsMutations,
  albumsQueries,
  AlbumsList,
  AlbumsCards,
}
