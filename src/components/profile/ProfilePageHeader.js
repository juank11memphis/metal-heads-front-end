import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Menu, Divider } from 'semantic-ui-react'

import { SortByOptions } from '../../shared/ux/sort'

class ProfilePageHeader extends Component {
  constructor(props, context) {
    super(props, context)
    this.menuOptions = [
      { value: 'rated', text: 'Rated Bands' },
      { value: 'saved', text: 'Watchlist' },
    ]
  }

  getActiveItemFromUrl() {
    const { router } = this.context
    const { route } = router
    const { location } = route
    const routeUrl = location.pathname
    return routeUrl.substring(routeUrl.lastIndexOf('/') + 1, routeUrl.length)
  }

  handleItemClick = (event, { name }) => {
    const { userId } = this.props
    const path = `/user/profile/${userId}/${name}`
    const { router } = this.context
    const { history } = router
    history.push(path)
  }

  renderMenu() {
    const activeItem = this.getActiveItemFromUrl()
    return (
      <Menu inverted compact>
        {this.menuOptions.map(menuItem => (
          <Menu.Item
            key={menuItem.value}
            name={menuItem.value}
            active={activeItem === menuItem.value}
            onClick={this.handleItemClick}
          >
            {menuItem.text}
          </Menu.Item>
        ))}
      </Menu>
    )
  }

  render() {
    const { onSortChange } = this.props
    return (
      <div>
        <div className="profile-page__header hbox">
          {this.renderMenu()}
          <SortByOptions onChange={onSortChange} showYearOption={false} />
        </div>
        <Divider />
      </div>
    )
  }
}

ProfilePageHeader.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

ProfilePageHeader.propTypes = {
  onSortChange: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
}

export default ProfilePageHeader
