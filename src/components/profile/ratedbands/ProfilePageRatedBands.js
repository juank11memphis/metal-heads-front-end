import React from 'react'
import PropTypes from 'prop-types'
import { Divider } from 'semantic-ui-react'

import { QueryWrapper } from '../../../shared/ux/graphql'
import { ImageCard } from '../../../shared/ux/layout'
import { StringUtil, SortUtil, UIUtil } from '../../../shared/util'
import { ErrorMessage } from '../../../shared/ux/messages'
import { SliderRating } from '../../../shared/ux/ratings'
import { usersQueries } from '../../../shared/users'

const renderBandCard = userBand => {
  const { band } = userBand
  return (
    <ImageCard
      key={userBand.id}
      item={band}
      imageField="picture"
      to={`/bands/${band.id}/info`}
    >
      <div>
        <div className="hbox">
          <h3 className="no-margin">{StringUtil.decodeString(band.name)}</h3>
          <h3 className="no-margin global-rating">{band.globalRating}%</h3>
        </div>
        <div className="profile-page-rated-bands__user-rating">
          <SliderRating value={userBand.rating} editable={false} />
        </div>
        <Divider />
      </div>
    </ImageCard>
  )
}

const renderError = error => (
  <ErrorMessage
    error={error}
    message="An unexpected error has occurred loading rated bands"
  />
)

const ProfilePageRatedBands = ({ sortBy, userId }) => (
  <QueryWrapper
    query={usersQueries.LOAD_RATED_BANDS}
    variables={{ userId }}
    fetchPolicy="network-only"
    loadingRenderer={() => UIUtil.renderLoader('Loading your bands...')}
    errorRenderer={renderError}
  >
    {({ ratedBands = [] }) => {
      if (ratedBands.length === 0) {
        return <div>You have not rate any bands yet</div>
      }
      const sortedBands = SortUtil.sortUserBands(ratedBands, sortBy)
      return (
        <ImageCard.Group className="profile-page-rated-bands">
          {sortedBands.map(userBand => renderBandCard(userBand))}
        </ImageCard.Group>
      )
    }}
  </QueryWrapper>
)

ProfilePageRatedBands.defaultProps = {
  sortBy: null,
}

ProfilePageRatedBands.propTypes = {
  userId: PropTypes.string.isRequired,
  sortBy: PropTypes.string,
}

export default ProfilePageRatedBands
