import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'
import { Query } from 'react-apollo'

import ProfilePageHeader from './ProfilePageHeader'
import ProfilePageSavedBands from './savedBands/ProfilePageSavedBands'
import ProfilePageRatedBands from './ratedbands/ProfilePageRatedBands'
import { usersQueries } from '../../shared/users'

export default class ProfilePage extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = { currentSort: null }
  }

  onSortChange = newSort => {
    this.setState({ currentSort: newSort })
  }

  render() {
    const { currentSort } = this.state
    const { userId } = this.props
    return (
      <Query query={usersQueries.LOAD_USER} variables={{ userId }}>
        {({ data: { user } }) =>
          user ? (
            <div className="default-detail-page profile-page">
              <h1>{`${user.firstname} ${user.lastname}`}</h1>
              <ProfilePageHeader
                onSortChange={this.onSortChange}
                userId={userId}
              />
              <Switch>
                <Route
                  path="/user/profile/:id/saved"
                  render={() => (
                    <ProfilePageSavedBands
                      sortBy={currentSort}
                      userId={userId}
                    />
                  )}
                />
                <Route
                  path="/user/profile/:id/rated"
                  render={() => (
                    <ProfilePageRatedBands
                      sortBy={currentSort}
                      userId={userId}
                    />
                  )}
                />
              </Switch>
            </div>
          ) : null
        }
      </Query>
    )
  }
}

ProfilePage.propTypes = {
  userId: PropTypes.string.isRequired,
}
