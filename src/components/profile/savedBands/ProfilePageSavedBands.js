import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import { ImageCard } from '../../../shared/ux/layout'
import { StringUtil, SortUtil, UIUtil } from '../../../shared/util'
import { ErrorMessage } from '../../../shared/ux/messages'
import { usersQueries, usersMutations } from '../../../shared/users'
import { QueryWrapper } from '../../../shared/ux/graphql'

const renderBandCard = (band, removeFromSavedBands) => (
  <ImageCard
    key={band.id}
    item={band}
    imageField="picture"
    to={`/bands/${band.id}/info`}
  >
    <div>
      <div className="band-actions">
        <Icon
          name="star"
          onClick={() =>
            removeFromSavedBands({ variables: { bandId: band.id } })
          }
        />
      </div>
      <div className="hbox">
        <h3 className="no-margin">{StringUtil.decodeString(band.name)}</h3>
        <h3 className="no-margin global-rating">
          {band.globalRating ? `${band.globalRating}%` : 'Not Rated'}
        </h3>
      </div>
    </div>
  </ImageCard>
)

const renderError = error => (
  <ErrorMessage
    error={error}
    message="An unexpected error has occurred loading watchlist"
  />
)

const getRefetchQueries = userId => [
  {
    query: usersQueries.LOAD_SAVED_BANDS,
    variables: { userId },
  },
]

const ProfilePageSavedBands = ({ sortBy, userId }) => (
  <QueryWrapper
    query={usersQueries.LOAD_SAVED_BANDS}
    variables={{ userId }}
    fetchPolicy="network-only"
    loadingRenderer={() => UIUtil.renderLoader('Loading your watchlist...')}
    errorRenderer={renderError}
  >
    {({ savedBands = [] }) => {
      if (savedBands.length === 0) {
        return <div>You have not added any bands to your watchlist</div>
      }
      const sortedBands = SortUtil.sortUserBands(savedBands, sortBy)
      return (
        <Mutation
          mutation={usersMutations.REMOVE_FROM_SAVED_BANDS}
          refetchQueries={getRefetchQueries(userId)}
        >
          {removeFromSavedBands => (
            <ImageCard.Group>
              {sortedBands.map(userBand =>
                renderBandCard(userBand.band, removeFromSavedBands),
              )}
            </ImageCard.Group>
          )}
        </Mutation>
      )
    }}
  </QueryWrapper>
)

ProfilePageSavedBands.defaultProps = {
  sortBy: null,
}

ProfilePageSavedBands.propTypes = {
  userId: PropTypes.string.isRequired,
  sortBy: PropTypes.string,
}

export default ProfilePageSavedBands
