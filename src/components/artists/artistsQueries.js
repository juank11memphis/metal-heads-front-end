import gql from 'graphql-tag'

import * as artistsFragments from './artistsFragments'

export const LOAD_ARTIST_DETAILS = gql`
  query loadArtistDetails($artistId: String) {
    artist: loadArtistDetails(artistId: $artistId) {
      ...ArtistInfoFull
    }
  }
  ${artistsFragments.artistInfoFull}
`
