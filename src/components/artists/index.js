import ArtistDetailPage from './detail/ArtistDetailPage'
import * as artistsFragments from './artistsFragments'

export { ArtistDetailPage, artistsFragments }
