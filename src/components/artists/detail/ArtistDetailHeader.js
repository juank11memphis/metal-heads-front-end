import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const ArtistDetailHeader = ({ band, artist }) => (
  <div className="artist-detail-page__header">
    {band ? (
      <Link to={`/bands/${band.id}/info`}>
        <h2 className="no-margin">{band.name}</h2>
      </Link>
    ) : null}
    {band ? <h3>&gt;</h3> : null}
    <Link to={`/artists/${artist.id}`}>
      <h2 className="no-margin">{artist.name}</h2>
    </Link>
  </div>
)

ArtistDetailHeader.defaultProps = {
  band: null,
}

ArtistDetailHeader.propTypes = {
  band: PropTypes.object,
  artist: PropTypes.object.isRequired,
}

export default ArtistDetailHeader
