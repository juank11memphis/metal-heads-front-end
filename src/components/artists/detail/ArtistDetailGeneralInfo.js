import React from 'react'
import PropTypes from 'prop-types'
import { Grid } from 'semantic-ui-react'
import { DisplayLabel } from '../../../shared/ux/layout'
import { StringUtil } from '../../../shared/util'

const ArtistDetailGeneralInfo = props => {
  const { artist } = props
  const imageStyle = {
    backgroundImage: `url(${artist.picture})`,
  }

  return (
    <div>
      <div className="artist-detail-page__image" style={imageStyle} />
      <Grid className="artist-detail-page__general-info">
        <Grid.Column width={8}>
          <DisplayLabel
            label="Real Name: "
            value={StringUtil.decodeString(artist.realName)}
          />
          <DisplayLabel
            label="Place of Origin: "
            value={StringUtil.decodeString(artist.placeOfOrigin)}
          />
        </Grid.Column>
        <Grid.Column width={8}>
          <DisplayLabel
            label="Age: "
            value={StringUtil.decodeString(artist.age)}
          />
          <DisplayLabel
            label="Gender: "
            value={StringUtil.decodeString(artist.gender)}
          />
        </Grid.Column>
      </Grid>
    </div>
  )
}

ArtistDetailGeneralInfo.propTypes = {
  artist: PropTypes.object.isRequired,
}

export default ArtistDetailGeneralInfo
