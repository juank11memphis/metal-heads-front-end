import React from 'react'
import PropTypes from 'prop-types'
import { Query } from 'react-apollo'

import ArtistDetailHeader from './ArtistDetailHeader'
import ArtistDetailGeneralInfo from './ArtistDetailGeneralInfo'
import ArtistDetailBands from './ArtistDetailBands'
import { LOAD_ARTIST_DETAILS } from '../artistsQueries'
import { LocalStorageUtil, UIUtil } from '../../../shared/util'
import { ErrorMessage } from '../../../shared/ux/messages'

const renderError = error => (
  <ErrorMessage
    error={error}
    message="An unexpected error has occurred loading artist details"
  />
)

const renderDetail = ({ artist }) => {
  const selectedBandData = LocalStorageUtil.getBandSelectedData()
  return (
    <div>
      <ArtistDetailHeader band={selectedBandData} artist={artist} />
      <ArtistDetailGeneralInfo artist={artist} />
      <ArtistDetailBands artist={artist} />
    </div>
  )
}

const ArtistDetailPage = ({ artistId }) => (
  <div className="default-detail-page artist-detail-page">
    <Query query={LOAD_ARTIST_DETAILS} variables={{ artistId }}>
      {({ data, error, loading }) => {
        if (loading) {
          return UIUtil.renderLoader('Loading Artist Details...')
        }
        if (error) {
          return renderError(error)
        }
        return renderDetail(data)
      }}
    </Query>
  </div>
)

ArtistDetailPage.propTypes = {
  artistId: PropTypes.string.isRequired,
}

export default ArtistDetailPage
