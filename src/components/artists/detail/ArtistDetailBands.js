import React, { PureComponent } from 'react'
import { Menu, Divider } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { StringUtil } from '../../../shared/util'

class ArtistDetailBands extends PureComponent {
  constructor(props, context) {
    super(props, context)
    this.state = {
      activeItem: 'activeBands',
    }
    this.handleItemClick = this.handleItemClick.bind(this)
  }

  handleItemClick(event, { name }) {
    this.setState({ activeItem: name })
  }

  static renderBandRow(band, index) {
    const path = `/bands/${band.id}/info`

    return (
      <div key={index} className="artist-detail-page__bands__item">
        {band.id && (
          <Link to={path}>
            <h4>{StringUtil.decodeString(band.name)}</h4>
          </Link>
        )}
        {!band.id && <h4>{StringUtil.decodeString(band.name)}</h4>}
        {StringUtil.decodeString(band.role)}
        <Divider />
      </div>
    )
  }

  renderBands() {
    const { activeItem } = this.state
    const { artist } = this.props
    const bands = artist[activeItem] || []
    return (
      <div className="artist-detail-page__bands">
        {bands.map((band, index) =>
          ArtistDetailBands.renderBandRow(band, index),
        )}
      </div>
    )
  }

  render() {
    const { activeItem } = this.state

    return (
      <div>
        <Menu className="artist-detail-page__bands-menu" inverted compact>
          <Menu.Item
            name="activeBands"
            active={activeItem === 'activeBands'}
            onClick={this.handleItemClick}
          >
            Active Bands
          </Menu.Item>
          <Menu.Item
            name="pastBands"
            active={activeItem === 'pastBands'}
            onClick={this.handleItemClick}
          >
            Past Bands
          </Menu.Item>
        </Menu>
        {this.renderBands()}
      </div>
    )
  }
}

ArtistDetailBands.propTypes = {
  artist: PropTypes.object.isRequired,
}

export default ArtistDetailBands
