import gql from 'graphql-tag'

export const artistSearchInfo = gql`
  fragment ArtistSearchInfo on ArtistSearch {
    id
    name
    gender
  }
`

export const artistInfoShort = gql`
  fragment ArtistInfoShort on Artist {
    id
    name
    picture
    gender
    roles {
      bandId
      role
    }
  }
`

export const artistBandInfo = gql`
  fragment ArtistBandInfo on ArtistBand {
    id
    name
    role
  }
`

export const artistInfoFull = gql`
  fragment ArtistInfoFull on Artist {
    ...ArtistInfoShort
    realName
    placeOfOrigin
    age
    activeBands {
      ...ArtistBandInfo
    }
    pastBands {
      ...ArtistBandInfo
    }
  }
  ${artistInfoShort}
  ${artistBandInfo}
`
