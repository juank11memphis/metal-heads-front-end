import React from 'react'
import { Link } from 'react-router-dom'
import { Divider } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import SignInForm from './form/SignInForm'
import {
  SocialAccountsButtons,
  AlreadyLoggedInRedirect,
  CurrentUserData,
  usersMutations,
} from '../../../shared/users'
import { ConfigData } from '../../../shared/config'

const renderSocialAccountsButtons = usersActions => (
  <ConfigData>
    {({ googleClientId }) => (
      <Mutation mutation={usersMutations.AUTH_SOCIAL}>
        {authSocial => (
          <SocialAccountsButtons
            googleClientId={googleClientId}
            googleText="Sign in with Google"
            spotifyText="Sign in with Spotify"
            onSocialAuthenticated={async user => {
              const { data } = await authSocial({ variables: { user } })
              const { auth } = data
              usersActions.authSuccess(auth.user)
            }}
          />
        )}
      </Mutation>
    )}
  </ConfigData>
)

const SignInPage = () => (
  <CurrentUserData>
    {({ usersActions }) => (
      <div className="signin-page">
        <AlreadyLoggedInRedirect redirectTo="/" />
        <h2>Sign In</h2>
        {renderSocialAccountsButtons(usersActions)}
        <Divider className="signin-page__divider" horizontal>
          Or use your email
        </Divider>
        <SignInForm />
        <Link to="forgot-password">
          <h4 className="signin-page__to-forgot-password">
            I have forgotten my password
          </h4>
        </Link>
        <div className="signin-page__to-signup">
          <span>
            New to MetalHeads?
            <Link to="/signup">
              <h3>Sign up</h3>
            </Link>
          </span>
        </div>
      </div>
    )}
  </CurrentUserData>
)

export default SignInPage
