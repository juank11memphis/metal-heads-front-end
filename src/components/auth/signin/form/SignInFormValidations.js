import { object, string } from 'yup'

const validations = object().shape({
  email: string()
    .email('Invalid email')
    .required('Your email is required'),
  password: string()
    .min(6, 'Password must have at least 6 characters')
    .required('Your password is required'),
})

export default validations
