import React from 'react'
import { Button } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import {
  FormContainer,
  FieldContainer,
  Input,
  PasswordInput,
} from '../../../../shared/ux/form'
import { usersMutations, CurrentUserData } from '../../../../shared/users'
import validations from './SignInFormValidations'

const initialValues = {
  email: '',
  password: '',
}

const SignInForm = () => (
  <CurrentUserData>
    {({ usersActions }) => (
      <Mutation mutation={usersMutations.SIGN_IN}>
        {signIn => {
          const submitAction = async ({ email, password }) => {
            const { data } = await signIn({
              variables: { email, password },
            })
            const { auth } = data
            usersActions.authSuccess(auth.user)
          }
          return (
            <FormContainer
              initialValues={initialValues}
              validations={validations}
              submitAction={submitAction}
            >
              <FieldContainer
                name="email"
                render={({ field }) => (
                  <Input placeholder="Your email address" field={field} />
                )}
              />
              <FieldContainer
                name="password"
                render={({ field }) => (
                  <PasswordInput placeholder="Your password" field={field} />
                )}
              />
              <Button secondary type="submit">
                Sign In
              </Button>
            </FormContainer>
          )
        }}
      </Mutation>
    )}
  </CurrentUserData>
)

export default SignInForm
