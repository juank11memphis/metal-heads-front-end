import React from 'react'
import { Button } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import validations from './signUpFormValidations'
import {
  FormContainer,
  Input,
  FieldContainer,
  PasswordInput,
  CountriesInput,
} from '../../../../shared/ux/form'
import { usersMutations, CurrentUserData } from '../../../../shared/users'

const initialValues = {
  firstname: '',
  lastname: '',
  email: '',
  password: '',
  country: '',
}

const SignUpForm = () => (
  <CurrentUserData>
    {({ usersActions }) => (
      <Mutation mutation={usersMutations.SIGN_UP}>
        {signUp => {
          const submitAction = async user => {
            const { data } = await signUp({
              variables: { user },
            })
            const { auth } = data
            usersActions.authSuccess(auth.user)
          }
          return (
            <FormContainer
              initialValues={initialValues}
              validations={validations}
              submitAction={submitAction}
            >
              <FieldContainer
                name="firstname"
                render={({ field }) => (
                  <Input placeholder="Your first name" field={field} />
                )}
              />
              <FieldContainer
                name="lastname"
                render={({ field }) => (
                  <Input placeholder="Your last name" field={field} />
                )}
              />
              <FieldContainer
                name="email"
                render={({ field }) => (
                  <Input placeholder="Your email address" field={field} />
                )}
              />
              <FieldContainer
                name="password"
                render={({ field }) => (
                  <PasswordInput placeholder="Your password" field={field} />
                )}
              />
              <FieldContainer
                name="country"
                render={({ field, form }) => (
                  <CountriesInput
                    placeholder="Your country"
                    field={field}
                    form={form}
                  />
                )}
              />
              <Button secondary type="submit">
                Sign Up
              </Button>
            </FormContainer>
          )
        }}
      </Mutation>
    )}
  </CurrentUserData>
)

export default SignUpForm
