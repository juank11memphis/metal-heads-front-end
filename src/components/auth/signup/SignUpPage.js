import React from 'react'
import { Divider } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { Mutation } from 'react-apollo'

import SignUpForm from './form/SignUpForm'
import {
  SocialAccountsButtons,
  usersMutations,
  CurrentUserData,
  AlreadyLoggedInRedirect,
} from '../../../shared/users'
import { ConfigData } from '../../../shared/config'

const renderSocialAccountButtons = usersActions => (
  <ConfigData>
    {({ googleClientId }) => (
      <Mutation mutation={usersMutations.AUTH_SOCIAL}>
        {authSocial => (
          <SocialAccountsButtons
            googleText="Sign up with Google"
            spotifyText="Sign up with Spotify"
            googleClientId={googleClientId}
            onSocialAuthenticated={async user => {
              const { data } = await authSocial({ variables: { user } })
              const { auth } = data
              usersActions.authSuccess(auth.user)
            }}
          />
        )}
      </Mutation>
    )}
  </ConfigData>
)

const SignUpPage = () => (
  <CurrentUserData>
    {({ usersActions }) => (
      <div className="signup-page">
        <AlreadyLoggedInRedirect redirectTo="/" />
        <h2>Become a MetalHead!</h2>
        {renderSocialAccountButtons(usersActions)}
        <Divider className="signup-page__divider" horizontal>
          Or use your email
        </Divider>
        <SignUpForm />
        <div className="signup-page__to-signin">
          Already a MetalHead?
          <Link to="/signin">
            <h3>Sign In</h3>
          </Link>
        </div>
      </div>
    )}
  </CurrentUserData>
)

export default SignUpPage
