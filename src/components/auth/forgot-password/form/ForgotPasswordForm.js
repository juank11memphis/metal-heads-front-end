import React from 'react'
import { Button } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import validations from './ForgotPasswordValidations'
import {
  FormContainer,
  Input,
  FieldContainer,
} from '../../../../shared/ux/form'
import { usersMutations } from '../../../../shared/users'

const ForgotPasswordForm = () => (
  <Mutation mutation={usersMutations.REQUEST_PASSWORD_RESET}>
    {requestPasswordReset => {
      const submitAction = async ({ email }) => {
        await requestPasswordReset({
          variables: { email },
        })
      }
      return (
        <FormContainer
          successMessage="Check your email for further instructions"
          successMessageSize="massive"
          hideFormOnSuccess
          autoHideSuccessMessage={false}
          initialValues={{ email: '' }}
          validations={validations}
          submitAction={submitAction}
        >
          <h2>Enter your email</h2>
          <FieldContainer
            name="email"
            render={({ field }) => (
              <Input placeholder="Your email address" field={field} />
            )}
          />
          <Button secondary type="submit">
            Request Password Reset
          </Button>
        </FormContainer>
      )
    }}
  </Mutation>
)

export default ForgotPasswordForm
