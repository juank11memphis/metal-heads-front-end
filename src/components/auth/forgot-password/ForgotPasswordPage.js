import React from 'react'

import ForgotPasswordForm from './form/ForgotPasswordForm'
import { AlreadyLoggedInRedirect } from '../../../shared/users'

const ForgotPasswordPage = () => (
  <div className="forgot-password-page">
    <AlreadyLoggedInRedirect redirectTo="/" />
    <ForgotPasswordForm />
  </div>
)

export default ForgotPasswordPage
