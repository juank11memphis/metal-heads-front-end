import React, { PureComponent } from 'react'
import queryString from 'query-string'
import PropTypes from 'prop-types'

import ResetPasswordForm from './form/ResetPasswordForm'
import { AlreadyLoggedInRedirect } from '../../../shared/users'

class ResetPasswordPage extends PureComponent {
  componentWillMount() {
    this.extractResetPasswordTokenFromUrl()
  }

  extractResetPasswordTokenFromUrl() {
    const { router } = this.context
    const { search } = router.route.location
    const queryStringParsed = queryString.parse(search)
    if (queryStringParsed.token) {
      this.resetPasswordToken = queryStringParsed.token
    }
  }

  render() {
    return (
      <div className="reset-password-page">
        <AlreadyLoggedInRedirect redirectTo="/" />
        <ResetPasswordForm resetToken={this.resetPasswordToken} />
      </div>
    )
  }
}

ResetPasswordPage.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default ResetPasswordPage
