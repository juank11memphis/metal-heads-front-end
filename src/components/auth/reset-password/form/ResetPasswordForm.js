import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import validations from './ResetPasswordValidations'
import {
  FormContainer,
  PasswordInput,
  FieldContainer,
} from '../../../../shared/ux/form'
import { CurrentUserData, usersMutations } from '../../../../shared/users'

const ResetPasswordForm = ({ resetToken }) => (
  <CurrentUserData>
    {({ usersActions }) => (
      <Mutation mutation={usersMutations.RESET_PASSWORD}>
        {resetPassword => {
          const submitAction = async ({ password, resetToken }) => {
            const { data } = await resetPassword({
              variables: { password, token: resetToken },
            })
            const { auth } = data
            usersActions.authSuccess(auth.user)
          }
          return (
            <FormContainer
              initialValues={{ password: '' }}
              validations={validations}
              submitAction={({ password }) =>
                submitAction({ password, resetToken })
              }
            >
              <h2>Enter your new password</h2>
              <FieldContainer
                name="password"
                render={({ field }) => (
                  <PasswordInput
                    placeholder="Your new password"
                    field={field}
                  />
                )}
              />
              <Button secondary type="submit">
                Reset Password
              </Button>
            </FormContainer>
          )
        }}
      </Mutation>
    )}
  </CurrentUserData>
)

ResetPasswordForm.defaultProps = {
  resetToken: null,
}

ResetPasswordForm.propTypes = {
  resetToken: PropTypes.string,
}

export default ResetPasswordForm
