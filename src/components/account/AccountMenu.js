import React, { Component } from 'react'
import { Menu, Divider } from 'semantic-ui-react'
import PropTypes from 'prop-types'

class AccountMenu extends Component {
  constructor(props, context) {
    super(props, context)
    this.handleItemClick = this.handleItemClick.bind(this)
    this.menuOptions = [
      { value: 'details', text: 'Details' },
      { value: 'password', text: 'Password' },
      { value: 'social-accounts', text: 'Social Accounts' },
    ]
  }

  getActiveItemFromUrl() {
    const { router } = this.context
    const { route } = router
    const { location } = route
    const routeUrl = location.pathname
    return routeUrl.substring(routeUrl.lastIndexOf('/') + 1, routeUrl.length)
  }

  handleItemClick(event, { name }) {
    const path = `/user/account/${name}`
    const { router } = this.context
    const { history } = router
    history.push(path)
  }

  render() {
    const activeItem = this.getActiveItemFromUrl()
    return (
      <div>
        <Menu className="account-page__menu" pointing secondary>
          {this.menuOptions.map(menuItem => (
            <Menu.Item
              key={menuItem.value}
              name={menuItem.value}
              active={activeItem === menuItem.value}
              onClick={this.handleItemClick}
            >
              {menuItem.text}
            </Menu.Item>
          ))}
        </Menu>
        <Divider className="account-page__divider" />
      </div>
    )
  }
}

AccountMenu.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  }),
}

export default AccountMenu
