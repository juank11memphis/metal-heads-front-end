import React from 'react'
import { Route, Switch } from 'react-router-dom'

import AccountMenu from './AccountMenu'
import AccountDetailsForm from './details/AccountDetailsForm'
import AccountChangePasswordForm from './change-password/AccountChangePasswordForm'
import {
  SocialAccountsConnected,
  NotLoggedInRedirect,
} from '../../shared/users'

const AccountPage = () => (
  <div className="default-detail-page account-page">
    <NotLoggedInRedirect redirectTo="/signin" />
    <h1 className="no-margin text-align-center">Account Settings</h1>
    <AccountMenu />
    <Switch>
      <Route
        path="/user/account/details"
        render={() => <AccountDetailsForm />}
      />
      <Route
        path="/user/account/password"
        render={() => <AccountChangePasswordForm />}
      />
      <Route
        path="/user/account/social-accounts"
        render={() => <SocialAccountsConnected />}
      />
    </Switch>
  </div>
)

export default AccountPage
