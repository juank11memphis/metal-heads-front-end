import { object, string } from 'yup'

export const allValidations = object().shape({
  password: string()
    .min(6, 'Your password must have at least 6 characters')
    .required('Your password is required'),
  newPassword: string()
    .min(6, 'Your new password must have at least 6 characters')
    .required('Your new password is required'),
})

export const onlyNewPasswordValidations = object().shape({
  newPassword: string()
    .min(6, 'Your new password must have at least 6 characters')
    .required('Your new password is required'),
})
