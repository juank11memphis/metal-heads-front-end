import React from 'react'
import { Button } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import {
  allValidations,
  onlyNewPasswordValidations,
} from './accountChangePasswordFormValidations'
import {
  FormContainer,
  PasswordInput,
  FieldContainer,
} from '../../../shared/ux/form'
import { CurrentUserData, usersMutations } from '../../../shared/users'

const initialValues = {
  password: '',
  newPassword: '',
}

const getValidations = showCurrentPassword => {
  if (!showCurrentPassword) {
    return onlyNewPasswordValidations
  }
  return allValidations
}

const renderForm = currentUser => (
  <Mutation mutation={usersMutations.CHANGE_PASSWORD}>
    {changePassword => {
      const submitAction = async ({ password, newPassword }) => {
        const { data } = await changePassword({
          variables: { password, newPassword },
        })
        return data.user
      }
      return (
        <FormContainer
          className="account-page__passwords"
          successMessage="Password changed successfully"
          initialValues={initialValues}
          validations={() => getValidations(currentUser.hasPassword)}
          submitAction={submitAction}
        >
          {currentUser.hasPassword ? (
            <FieldContainer
              name="password"
              render={({ field }) => (
                <PasswordInput placeholder="Current Password" field={field} />
              )}
            />
          ) : null}
          <FieldContainer
            name="newPassword"
            render={({ field }) => (
              <PasswordInput placeholder="New Password" field={field} />
            )}
          />
          <Button secondary type="submit">
            Change Password
          </Button>
        </FormContainer>
      )
    }}
  </Mutation>
)

const AccountChangePasswordForm = () => (
  <CurrentUserData>
    {({ currentUser }) => currentUser && renderForm(currentUser)}
  </CurrentUserData>
)

export default AccountChangePasswordForm
