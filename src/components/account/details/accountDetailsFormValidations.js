import { object, string } from 'yup'

const validations = object().shape({
  firstname: string()
    .min(2, 'Your first name must have at least 2 characters')
    .required('Your first name is required'),
  lastname: string()
    .min(2, 'Your last name must have at least 2 characters')
    .required('Your last name is required'),
  country: string().required('Your country is required'),
})

export default validations
