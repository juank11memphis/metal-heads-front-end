import React from 'react'
import { Button } from 'semantic-ui-react'
import { Mutation } from 'react-apollo'

import validations from './accountDetailsFormValidations'
import {
  FormContainer,
  Input,
  FieldContainer,
  CountriesInput,
} from '../../../shared/ux/form'
import { CurrentUserData, usersMutations } from '../../../shared/users'

const renderForm = (currentUser, usersActions) => (
  <Mutation mutation={usersMutations.UPDATE_USER}>
    {updateTokenUser => {
      const submitAction = async user => {
        const { data } = await updateTokenUser({ variables: { user } })
        usersActions.userUpdateSuccess(data.user)
        return data.user
      }
      return (
        <FormContainer
          className="account-page__details"
          successMessage="User updated successfully"
          initialValues={currentUser}
          validations={validations}
          submitAction={({ firstname, lastname, country }) =>
            submitAction({ firstname, lastname, country })
          }
        >
          <FieldContainer
            name="firstname"
            render={({ field }) => (
              <Input placeholder="Your first name" field={field} />
            )}
          />
          <FieldContainer
            name="lastname"
            render={({ field }) => (
              <Input placeholder="Your last name" field={field} />
            )}
          />
          <FieldContainer
            name="email"
            render={({ field }) => (
              <Input placeholder="Your email address" field={field} disabled />
            )}
          />
          <FieldContainer
            name="country"
            render={({ field, form }) => (
              <CountriesInput
                placeholder="Your country"
                field={field}
                form={form}
              />
            )}
          />
          <Button secondary type="submit">
            Save
          </Button>
        </FormContainer>
      )
    }}
  </Mutation>
)

const AccountDetailsForm = () => (
  <CurrentUserData>
    {({ currentUser, usersActions }) =>
      currentUser && renderForm(currentUser, usersActions)
    }
  </CurrentUserData>
)

export default AccountDetailsForm
