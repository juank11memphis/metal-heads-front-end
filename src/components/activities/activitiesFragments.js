import gql from 'graphql-tag'

import { usersFragments } from '../../shared/users'

export const bandActivityFull = gql`
  fragment BandActivityFull on Activity {
    id
    actor {
      ...UserShort
    }
    verb
    target
    band {
      id
      name
      picture
      globalRating
      ratingsCount
    }
    metadata {
      rating
    }
  }
  ${usersFragments.userShort}
`

export const albumActivityFull = gql`
  fragment AlbumActivityFull on Activity {
    id
    actor {
      ...UserShort
    }
    verb
    target
    album {
      id
      name
      picture
      globalRating
      ratingsCount
    }
    metadata {
      rating
    }
  }
  ${usersFragments.userShort}
`
