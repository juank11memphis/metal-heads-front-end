import React from 'react'
import { Divider } from 'semantic-ui-react'
import { Query } from 'react-apollo'

import { ErrorMessage } from '../../shared/ux/messages'
import { GET_LATEST_ALBUM_RATINGS } from './activitiesQueries'
import RatingActivity from './RatingActivity'

const renderError = error => (
  <ErrorMessage
    error={error}
    message="An unexpected error has occurred loading latest album ratings"
  />
)

const renderActivities = ({ albumRatings = [] }) => (
  <div className="activities-container">
    {albumRatings.map(activity => (
      <RatingActivity key={activity.id} activity={activity} type="album" />
    ))}
  </div>
)

const LatestAlbumRatings = () => (
  <div className="latest-album-ratings">
    <h3>Latest Albums Ratings</h3>
    <Divider />
    <Query
      query={GET_LATEST_ALBUM_RATINGS}
      variables={{ limit: 10 }}
      fetchPolicy="network-only"
    >
      {({ data, error }) =>
        error ? renderError(error) : renderActivities(data)
      }
    </Query>
  </div>
)

export default LatestAlbumRatings
