import React from 'react'
import { Divider } from 'semantic-ui-react'
import { Query } from 'react-apollo'

import { ErrorMessage } from '../../shared/ux/messages'
import { GET_LATEST_BAND_RATINGS } from './activitiesQueries'
import RatingActivity from './RatingActivity'

const renderError = error => (
  <ErrorMessage
    error={error}
    message="An unexpected error has occurred loading latest band ratings"
  />
)

const renderActivities = ({ bandRatings = [] }) => (
  <div className="activities-container">
    {bandRatings.map(activity => (
      <RatingActivity key={activity.id} activity={activity} type="band" />
    ))}
  </div>
)

const LatestBandRatings = () => (
  <div className="latest-band-ratings">
    <h3>Latest Band Ratings</h3>
    <Divider />
    <Query
      query={GET_LATEST_BAND_RATINGS}
      variables={{ limit: 10 }}
      fetchPolicy="network-only"
    >
      {({ data, error }) =>
        error ? renderError(error) : renderActivities(data)
      }
    </Query>
  </div>
)

export default LatestBandRatings
