import gql from 'graphql-tag'

import * as activitiesFragments from './activitiesFragments'

export const GET_LATEST_BAND_RATINGS = gql`
  query getLatestBandRatings($limit: Float) {
    bandRatings: getLatestBandRatings(limit: $limit) {
      ...BandActivityFull
    }
  }
  ${activitiesFragments.bandActivityFull}
`

export const GET_LATEST_ALBUM_RATINGS = gql`
  query getLatestAlbumRatings($limit: Float) {
    albumRatings: getLatestAlbumRatings(limit: $limit) {
      ...AlbumActivityFull
    }
  }
  ${activitiesFragments.albumActivityFull}
`
