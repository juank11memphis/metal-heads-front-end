import React from 'react'
import PropTypes from 'prop-types'
import { get } from 'lodash'
import { Link } from 'react-router-dom'
import { Divider } from 'semantic-ui-react'

import { ImageCard } from '../../shared/ux/layout'

const RatingActivity = ({ activity, type }) => {
  const actorId = get(activity, 'actor.id')
  const actorName = get(activity, 'actor.firstname')
  const item = get(activity, type)
  const itemId = get(activity, `${type}.id`)
  const itemName = get(activity, `${type}.name`)
  const itemGlobalRating = get(activity, `${type}.globalRating`)
  const itemRatingsCount = get(activity, `${type}.ratingsCount`)
  const rating = get(activity, 'metadata.rating')
  const userLink = `user/profile/${actorId}/rated`
  const bandLink =
    type === 'band' ? `/bands/${itemId}/info` : `/albums/${itemId}`
  return (
    <div className="activity-item">
      <div className="text-align-center small-margin-top-bottom">
        <h3 className="no-margin display-inline-block">
          <Link to={userLink}>@{actorName}</Link>
        </h3>{' '}
        rated
        {` ${type} `}
        <h3 className="no-margin display-inline-block">
          <Link to={bandLink}> {itemName} </Link>
        </h3>{' '}
        with a{' '}
        <h3 className="no-margin display-inline-block yellow-text">{rating}</h3>{' '}
        out of 10
      </div>
      <div className="hbox-raw" style={{ flex: 1 }}>
        <ImageCard item={item} imageField="picture" to={bandLink} />
        <div className="activity-item__ratings vbox flex-child-full text-align-center justify-content-center">
          <div>Global Rating</div>
          <h1 className="global-rating no-margin small-margin-top-bottom">
            {itemGlobalRating}%
          </h1>
          <div>User Ratings</div>
          <h3 className="no-margin small-margin-top-bottom">
            {itemRatingsCount}
          </h3>
        </div>
      </div>
      <Divider className="no-margin" />
    </div>
  )
}

RatingActivity.propTypes = {
  activity: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
}

export default RatingActivity
