import LatestBandRatings from './LatestBandRatings'
import LatestAlbumRatings from './LatestAlbumRatings'

export { LatestBandRatings, LatestAlbumRatings }
