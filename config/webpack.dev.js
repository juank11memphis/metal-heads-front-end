/* eslint-disable */

const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const HTMLWebpackPlugin = require('html-webpack-plugin')

const common = require('./webpack.common')

module.exports = merge(common, {
  entry: {
    app: ['./src/index.js'],
  },
  mode: 'development',
  devServer: {
    compress: true,
    contentBase: 'dist',
    overlay: true,
    port: 3000,
    historyApiFallback: true,
    hot: true,
    proxy: {
      '/graphql': 'http://localhost:3001',
      '/api': 'http://localhost:3001',
      '/static': 'http://localhost:3001',
    },
    stats: {
      colors: true,
    },
  },
  devtool: 'inline-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
})
