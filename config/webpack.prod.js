/* eslint-disable */

const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const optimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const uglifyJSPlugin = require('uglifyjs-webpack-plugin')
const compressionPlugin = require('compression-webpack-plugin')
const brotliPlugin = require('brotli-webpack-plugin')
const miniCssExtractPlugin = require('mini-css-extract-plugin')

const common = require('./webpack.common')

module.exports = merge(common, {
  entry: {
    app: ['./src/index.js'],
  },
  output: {
    filename: '[name]-bundle-[hash:8].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/',
  },
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new optimizeCssAssetsPlugin(),
    new miniCssExtractPlugin({
      filename: '[name]-[contenthash].css',
    }),
    new uglifyJSPlugin(),
    new compressionPlugin({
      algorithm: 'gzip',
    }),
    new brotliPlugin(),
  ],
})
